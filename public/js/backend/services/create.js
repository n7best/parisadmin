$(function() {
    $(document).ready(function(){
        var metaCounter = 0;
        var box = $('#metaBox');

        $('#addMeta').click(function(e){
            e.preventDefault();
            //meta select
            var el = newFormGroup('metasType['+metaCounter+']', 'Meta'+metaCounter+' Type')
                .append(addSelect(metaCounter));
            //meta name
            var el2 = newFormGroup('metasName['+metaCounter+']', 'Meta'+metaCounter+' Name')
                .append(addInput('metasName['+metaCounter+']', 'Meta'+metaCounter+' Name'));
            //meta price
            var el3 = newFormGroup('metasPrice['+metaCounter+']', 'Meta'+metaCounter+' Price')
                .append(addInput('metasPrice['+metaCounter+']', 'Meta'+metaCounter+' Price'));
            //options
            var el4 = newFormGroup('metasOptions['+metaCounter+']', 'Meta'+metaCounter+' Options')
                .append(addInput('metasOptions['+metaCounter+']', 'Meta'+metaCounter+' Options'));

            box.append(el, el2, el3, el4);
            metaCounter++;
        });

        function newFormGroup(name,label){
            return $('<div></div>')
                .addClass('form-group')
                .html(newLabel(name,label));
        }

        function newLabel(name,label){
            return '<label for="'+name+'" class="col-lg-2 control-label">'+label+'</label>';
        }

        function addInput(name,label){
            return $('<div></div>').addClass('col-lg-10')
                .html('<input class="form-control" placeholder="'+ label +'" name="'+name+'" type="text">');
        }

        function addSelect(counter){
            return $('<div></div>').addClass('col-lg-10')
                .html('<select name="metasType['+counter+']" class="form-control">'+
                    '<option value="text">Text</option>' +
                    '<option value="integer">Integer</option>' +
                    '<option value="select">Select</option>' +
                 '</select>');
        }
    });
});