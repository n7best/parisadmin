<?php

return [
  'order'=>[
    ['type'=>'created', 'role'=>'Sales,Sales LA'],
    ['type'=>'progress', 'role'=>'Sales,Sales LA'],
    ['type'=>'complete', 'role'=>'Sales,Sales LA'],
    ['type'=>'cancel', 'role'=>''],
    ['type'=>'reschedule', 'role'=>'Sales,Sales LA'],
  ],
  'tasks'=>[
    //photo
    [
      'services' => [
        'pre-photoshot',
        'post-photoshot',
        'wd-photographer',
        'wd-photographer-lim',
        'wd-videographer',
        'wd-videographer-lim',
        'wd-videographer-teamwork'
      ],
      'status' => [
        ['type'=>'created', 'role'=>'Sales LA,Sales'],
        ['type'=>'schedule', 'role'=>'Sales LA,Sales'],
        ['type'=>'reschedule', 'role'=>'Sales LA,Sales'],
        ['type'=>'complete', 'role'=>'Sales LA,Sales'],
        ['type'=>'approved', 'role'=>'Sales LA,Sales'],
        ['type'=>'submited', 'role'=>'Sales LA,Sales,Makeup Artist,Photographer'],
        ['type'=>'pending', 'role'=>'Sales LA,Sales'],
        ['type'=>'post-production', 'role'=>'Sales LA,Sales'],
      ]
    ],
    //item
    [
      'services' => [
        'albums',
        'slideshow',
        'invitation-cards',
        'photo-cd',
        'ablums-case',
        'printout-4x6',
        'poster',
        'print-out',
        'frame'
      ],
      'status' => [
        ['type'=>'created', 'role'=>'Sales LA,Sales'],
        ['type'=>'pending pickup', 'role'=>'Sales LA,Sales'],
        ['type'=>'complete', 'role'=>'Sales LA,Sales'],
      ]
    ],
    //dress
    [
      'services' => [
        'pre-dress',
        'baby-outfit',
        'wd-dress'
      ],
      'status' => [
        ['type'=>'created', 'role'=>'Sales LA,Sales'],
        ['type'=>'schedule', 'role'=>'Sales LA,Sales'],
        ['type'=>'selected', 'role'=>'Sales LA,Sales'],
        ['type'=>'reschedule', 'role'=>'Sales LA,Sales'],
        ['type'=>'complete', 'role'=>'Sales LA,Sales']
      ]
    ],
    //wd-services
    [
      'services' => [
        'car-service-addon',
        'hummer-20',
        'limousine-10-weekday',
        'limousine-10-weekend',
        'makeup-hair',
        'night-view-addon',
        'ot-makeup-morning',
        'ot-makeup-night',
        'ot-makeup-outdoor',
        'video-lovestory-addon',
        'video-prewedding-addon',
        'video-weddingday-addon',
        'wd-flower',
        'wd-makeup',
        'wd-makeup-try'
      ],
      'status' => [
        ['type'=>'created', 'role'=>'Sales LA,Sales'],
        ['type'=>'assigned', 'role'=>'Sales LA,Sales'],
        ['type'=>'complete', 'role'=>'Sales LA,Sales']
      ]
    ],
  ]
];