<?php

return [
    /*
    |----------------------------------------------------------------------------
    | Google application name
    |----------------------------------------------------------------------------
    */
    'application_name' => 'parisapi',

    /*
    |----------------------------------------------------------------------------
    | Google OAuth 2.0 access
    |----------------------------------------------------------------------------
    |
    | Keys for OAuth 2.0 access, see the API console at
    | https://developers.google.com/console
    |
    */
    'client_id'       => '755165891507-raqkimbsf8qdh2g5n7scnl3irb7fvr9e.apps.googleusercontent.com',
    'client_secret'   => 'WqkqDtJjtmO9Wj7fiiAREk9I',
    'redirect_uri'    => '',
    'scopes'          => [],
    'access_type'     => 'online',
    'approval_prompt' => 'auto',

    /*
    |----------------------------------------------------------------------------
    | Google developer key
    |----------------------------------------------------------------------------
    |
    | Simple API access key, also from the API console. Ensure you get
    | a Server key, and not a Browser key.
    |
    */
    'developer_key' => 'AIzaSyDzwd2ApMMBsMESBF8RjU-zGtCDTvtzDtI',

    /*
    |----------------------------------------------------------------------------
    | Google service account
    |----------------------------------------------------------------------------
    |
    | Enable and set the information below to use assert credentials
    | Enable and leave blank to use app engine or compute engine.
    |
    */
    'service' => [
        /*
        | Enable service account auth or not.
        */
        'enable' => true,

        /*
        | Example xxx@developer.gserviceaccount.com
        */
        'account' => 'apiadmin@parisapi-1269.iam.gserviceaccount.com',

        /*
        | Example ['https://www.googleapis.com/auth/cloud-platform']
        */
        'scopes' => ['https://www.googleapis.com/auth/drive'],

        /*
        | Path to key file
        | Example storage_path().'/key/google.p12'
        */
        'key' => storage_path().'/google/parisapi-114d191b4fd2.p12',
    ],
];
