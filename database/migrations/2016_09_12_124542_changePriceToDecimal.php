<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangePriceToDecimal extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function ($table) {
            $table->decimal('price', 10, 2)->change();
            $table->decimal('tax', 10, 2)->change();
            $table->decimal('discounts', 10, 2)->change();
            $table->decimal('adjustment', 10, 2)->change();
            $table->decimal('balance', 10, 2)->change();
            $table->decimal('price_services', 10, 2)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
