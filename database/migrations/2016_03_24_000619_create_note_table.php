<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNoteTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('note', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('notes_id')->unsigned();
            $table->foreign('notes_id')->references('id')->on('notes')->onDelete('cascade');
            $table->string('name');
            $table->text('message');
            $table->boolean('interal');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('chats');
    }
}
