<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateZipFileTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('zips', function(Blueprint $table) {
            $table->increments('id');

            $table->integer('file_id')->nullable()->unsigned();
            $table->foreign('file_id')->references('id')->on('files')->onDelete('cascade');

            $table->string('from');
            $table->integer('from_id');

            $table->string('status');

            $table->string('hash');

            $table->text('details');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('zips');
    }
}
