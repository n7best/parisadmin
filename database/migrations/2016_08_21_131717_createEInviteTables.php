<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEInviteTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('einvites', function (Blueprint $table) {
            $table->increments('id');
            $table->text('options');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('einvitelogs', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('einvite_id')->unsigned();
            $table->foreign('einvite_id')->references('id')->on('einvites')->onDelete('cascade');

            $table->text('message');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('einvitedonations', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('einvite_id')->unsigned();
            $table->foreign('einvite_id')->references('id')->on('einvites')->onDelete('cascade');

            $table->string('name');
            $table->string('type');
            $table->float('amount');
            $table->string('token')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('einvites');
        Schema::drop('einvitelogs');
        Schema::drop('einvitedonations');
    }
}
