<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SetupServicesTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('services', function ($table) {
            $table->increments('id')->unsigned();
            $table->string('name');
            $table->string('display_name');
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default('0000-00-00 00:00');
            /**
             * Add Foreign/Unique/Index
             */
            $table->unique('name');
        });

        Schema::create('services_metas', function ($table) {
            $table->increments('id')->unsigned();
            $table->integer('services_id')->unsigned();
            $table->string('name');
            $table->string('type');
            $table->string('price_factor');
            $table->string('options');
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default('0000-00-00 00:00');
            /**
             * Add Foreign/Unique/Index
             */
            $table->foreign('services_id')
                ->references('id')
                ->on('services')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('services', function (Blueprint $table) {
            $table->dropUnique('services' . '_name_unique');
        });

        Schema::table('services_metas', function (Blueprint $table) {
            $table->dropForeign('services_metas' . '_services_id_foreign');
        });

        Schema::drop('services_metas');
        Schema::drop('services');
    }
}
