<table class="table table-responsive table-striped table-bordered table-hover">
    <thead>
    <th>Id</th>
			<th>Name</th>
			<th>Logo</th>
			<th>Phones</th>
			<th>Address</th>
			<th>Email</th>
    <th width="50px">Action</th>
    </thead>
    <tbody>
    @foreach($companies as $company)
        <tr>
            <td>{!! $company->id !!}</td>
			<td>{!! $company->name !!}</td>
			<td>{!! $company->logo !!}</td>
			<td>{!! $company->phones !!}</td>
			<td>{!! $company->address !!}</td>
			<td>{!! $company->email !!}</td>
            <td>
                <a href="{!! route('admin.companies.edit', [$company->id]) !!}"><i class="glyphicon glyphicon-edit"></i></a>
                <a href="{!! route('admin.companies.delete', [$company->id]) !!}" onclick="return confirm('Are you sure wants to delete this Company?')">
                    <i class="glyphicon glyphicon-trash"></i>
                </a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>