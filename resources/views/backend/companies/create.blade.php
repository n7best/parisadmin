@extends ('backend.layouts.master')

@section('content')
<div class="container">

    <div class="row">
        <div class="col-sm-12">
            <h1 class="pull-left">Create New Company</h1>
        </div>
    </div>

    @include('core-templates::common.errors')

    <div class="row">
        {!! Form::open(['route' => 'admin.companies.store']) !!}

            @include('backend.companies.fields')

        {!! Form::close() !!}
    </div>
</div>
@endsection