@extends ('backend.layouts.master')

@section ('title', 'Companies')

@section('content')
    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">Companies</h3>

            <div class="box-tools pull-right">
               <a class="btn btn-primary btn-sm" href="{!! route('admin.companies.create') !!}">Add New</a>
            </div>
        </div><!-- /.box-header -->

        <div class="box-body">
        @include('flash::message')
        <div class="clearfix"></div>
        @if($companies->isEmpty())
            <div class="well text-center">No Companies found.</div>
        @else
            @include('backend.companies.table')
        @endif
        </div>
    </div>
@endsection