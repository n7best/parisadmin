@extends('backend.layouts.master')

@section('page-header')
    <h1>
        Activity Log
    </h1>
@endsection

@section('content')
    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">Logs</h3>
        </div><!-- /.box-header -->
        <div class="box-body">
            <table class="table table-responsive table-striped table-bordered table-hover" id="logs-table">
                   <thead>
                       <tr>
                           <th>Created At</th>
                           <th>Details</th>
                           <th>Action</th>
                           <th>Description</th>
                           <th>Updated At</th>
                       </tr>
                   </thead>
               </table>
        </div><!-- /.box-body -->
    </div><!--box box-success-->
@endsection

@section('after-styles-end')
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.11/css/dataTables.bootstrap.min.css">
@endsection

@section('before-scripts-end')
    <script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
    <script src="//cdn.datatables.net/1.10.11/js/dataTables.bootstrap.min.js"></script>
@endsection

@section('after-scripts-end')
    <script>
    $(function() {
        $('#logs-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{!! route('activity-logs.data') !!}',
            order: [[ 1, "desc" ]],
            columns: [
                { data: 'created_at', name: 'created_at' },
                { data: 'details', name: 'details' },
                { data: 'action', name: 'action' },
                { data: 'description', name: 'description' },
                { data: 'updated_at', name: 'updated_at' }
            ]
        });
    });
    </script>
@endsection