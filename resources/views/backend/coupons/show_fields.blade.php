<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $coupon->id !!}</p>
</div>

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('type', 'Type:') !!}
    <p>{!! $coupon->type !!}</p>
</div>

<!-- Display Name Field -->
<div class="form-group">
    {!! Form::label('value', 'Value:') !!}
    <p>{!! $coupon->value !!}</p>
</div>

<!-- Services Field -->
<div class="form-group">
    {!! Form::label('requirements', 'Requirements:') !!}
    <p>{!! $coupon->requirements !!}</p>
</div>

<!-- Price Field -->
<div class="form-group">
    {!! Form::label('code', 'Code:') !!}
    <p>{!! $coupon->code !!}</p>
</div>
