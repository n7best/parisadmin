@extends ('backend.layouts.master')

@section('content')

    <div class="container">

        <h1 class="pull-left">coupons</h1>
        <a class="btn btn-primary pull-right" style="margin-top: 25px" href="{!! route('admin.coupons.create') !!}">Add New</a>

        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>

        @if($coupons->isEmpty())
            <div class="well text-center">No coupons found.</div>
        @else
            @include('backend.coupons.table')
        @endif
        
    </div>
@endsection