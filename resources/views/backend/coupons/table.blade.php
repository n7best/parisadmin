<table class="table table-responsive">
    <thead>
      <th>Id</th>
      <th>Code</th>
      <th>Type</th>
      <th>Value</th>
      <th>Order</th>
    <th width="50px">Action</th>
    </thead>
    <tbody>
    @foreach($coupons as $coupon)
        <tr>
            <td>{!! $coupon->id !!}</td>
      <td>{!! $coupon->code !!}</td>
      <td>{!! $coupon->type !!}</td>
      <td>{!! $coupon->value !!}</td>
      <td>{!! $coupon->order_id !!}</td>
            <td>
                <a href="{!! route('admin.coupons.edit', [$coupon->id]) !!}"><i class="glyphicon glyphicon-edit"></i></a>
                <a href="{!! route('admin.coupons.delete', [$coupon->id]) !!}" onclick="return confirm('Are you sure wants to delete this coupon?')">
                    <i class="glyphicon glyphicon-trash"></i>
                </a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>