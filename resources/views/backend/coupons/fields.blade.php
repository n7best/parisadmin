<!--- Name Field --->
<div class="form-group col-sm-6">
    {!! Form::label('type', 'Type:') !!}
    {!! Form::text('type', null, ['class' => 'form-control']) !!}
</div>

<!--- Display Name Field --->
<div class="form-group col-sm-6">
    {!! Form::label('value', 'Value:') !!}
    {!! Form::text('value', null, ['class' => 'form-control']) !!}
</div>


<!--- Display Name Field --->
<div class="form-group col-sm-6">
    {!! Form::label('requirements', 'Requirements:') !!}
    {!! Form::text('requirements', null, ['class' => 'form-control']) !!}
</div>


<!--- Display Name Field --->
<div class="form-group col-sm-6">
    {!! Form::label('code', 'Code:') !!}
    {!! Form::text('code', null, ['class' => 'form-control']) !!}
</div>

<!--- Submit Field --->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('admin.coupons.index') !!}" class="btn btn-default">Cancel</a>
</div>
