@extends ('backend.layouts.master')

@section('content')
<div class="container">

    <div class="row">
        <div class="col-sm-12">
            <h1 class="pull-left">Create New coupon</h1>
        </div>
    </div>

    @include('core-templates::common.errors')

    <div class="row">
        {!! Form::open(['route' => 'admin.coupons.store']) !!}

            @include('backend.coupons.fields')

        {!! Form::close() !!}
    </div>
</div>
@endsection

@section('after-scripts-end')

@endsection