@extends ('backend.layouts.master')

@section('content')
    <div class="container">

        <div class="row">
            <div class="col-sm-12">
                <h1 class="pull-left">Create New Coupon</h1>
            </div>
        </div>

        @include('core-templates::common.errors')

        <div class="row">
            {!! Form::model($coupon, ['route' => ['admin.coupons.update', $coupon->id], 'method' => 'patch']) !!}

            @include('backend.coupons.fields')

            {!! Form::close() !!}
        </div>
    </div>
@endsection

@section('after-scripts-end')

@endsection