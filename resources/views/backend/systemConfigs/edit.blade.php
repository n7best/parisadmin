@extends('backend.layouts.master')

@section('content')
    <div class="container">

        <div class="row">
            <div class="col-sm-12">
                <h1 class="pull-left">Create New SystemConfigs</h1>
            </div>
        </div>

        @include('core-templates::common.errors')

        <div class="row">
            {!! Form::model($systemConfigs, ['route' => ['admin.systemConfigs.update', $systemConfigs->id], 'method' => 'patch']) !!}

            @include('backend.systemConfigs.fields')

            {!! Form::close() !!}
        </div>
    </div>
@endsection