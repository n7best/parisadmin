<table class="table table-responsive table-striped table-bordered table-hover">
    <thead>
    <th>Id</th>
			<th>Type</th>
			<th>Value</th>
    <th width="50px">Action</th>
    </thead>
    <tbody>
    @foreach($systemConfigs as $systemConfigs)
        <tr>
            <td>{!! $systemConfigs->id !!}</td>
			<td>{!! $systemConfigs->type !!}</td>
			<td>{!! $systemConfigs->value !!}</td>
            <td>
                <a href="{!! route('admin.systemConfigs.edit', [$systemConfigs->id]) !!}"><i class="glyphicon glyphicon-edit"></i></a>
                <a href="{!! route('admin.systemConfigs.delete', [$systemConfigs->id]) !!}" onclick="return confirm('Are you sure wants to delete this SystemConfigs?')">
                    <i class="glyphicon glyphicon-trash"></i>
                </a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>