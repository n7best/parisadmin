@extends('backend.layouts.master')

@section('content')
<div class="container">

    <div class="row">
        <div class="col-sm-12">
            <h1 class="pull-left">Create New SystemConfigs</h1>
        </div>
    </div>

    @include('core-templates::common.errors')

    <div class="row">
        {!! Form::open(['route' => 'admin.systemConfigs.store']) !!}

            @include('backend.systemConfigs.fields')

        {!! Form::close() !!}
    </div>
</div>
@endsection