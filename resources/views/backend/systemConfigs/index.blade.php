@extends('backend.layouts.master')

@section('content')

    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">System Configs</h3>

            <div class="box-tools pull-right">
               <a class="btn btn-primary btn-sm" href="{!! route('admin.systemConfigs.create') !!}">Add New</a>
            </div>
        </div><!-- /.box-header -->

        <div class="box-body">

        @include('flash::message')

        <div class="clearfix"></div>

        @if($systemConfigs->isEmpty())
            <div class="well text-center">No SystemConfigs found.</div>
        @else
            @include('backend.systemConfigs.table')
        @endif

        </div>

    </div>

    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">Templates</h3>
        </div><!-- /.box-header -->

        <div class="box-body">
            <table class="table table-responsive table-striped table-bordered table-hover">
                <thead>
                <th>Id</th>
                <th>Slog</th>
                <th width="50px">Action</th>
                </thead>
                <tbody>
                @foreach($templates as $template)
                    <tr>
                        <td>{!! $template->id !!}</td>
                        <td>{!! $template->slog !!}</td>
                        <td>
                            <a href="{!! route('admin.templates.edit', [$template->id]) !!}"><i class="glyphicon glyphicon-edit"></i></a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection