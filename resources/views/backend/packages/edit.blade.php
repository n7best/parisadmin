@extends ('backend.layouts.master')

@section('content')
    <div class="container">

        <div class="row">
            <div class="col-sm-12">
                <h1 class="pull-left">Create New package</h1>
            </div>
        </div>

        @include('core-templates::common.errors')

        <div class="row">
            {!! Form::model($package, ['route' => ['admin.packages.update', $package->id], 'method' => 'patch']) !!}

            @include('backend.packages.fields')

            {!! Form::close() !!}
        </div>
    </div>
@endsection

@section('after-scripts-end')
<script>
    var allServices = {!! $services !!}
    var allOptions = {}
    var allNames = {}
    allServices.forEach(function(service){
        allOptions[service.id] = service.metas
        allNames[service.id] = service.display_name
    })


    var services = $('#services').val().split(',');
    var servicesSelect = [];
    var selectedOptions = $('#options').val() ? JSON.parse($('#options').val()) : {};
    var serviceQty = {};

    services.forEach(function(el,i){
        var serProperty = el.split(':');
        var id = serProperty[0];
        servicesSelect.push(id);
        var cb = $('input:checkbox[value='+id+']').attr('checked',true);

        if(serProperty.length > 1){
            var qty = serProperty[1];
            serviceQty[id] = qty;
            $('input[data-id='+id+']').val(parseInt(qty));
        }
    });

    updateOptions(true)

    $('.service').click(function(){
        var cb = $(this);
        var id = cb.val();

        var qty = $('#cqty'+id).val();

        var _index = servicesSelect.indexOf(id);

        if( _index == -1) {
            servicesSelect.push(id);
            serviceQty[id] = qty;
            cb.attr('checked',true);
        }else{
            servicesSelect.splice(_index,1);
            delete serviceQty[id];
            cb.attr('checked',false);
        }

        updateServices();
    });

    $('.quantity').change(function(){
        var el = $(this);
        var qty = el.val();
        var id = el.data('id');
        serviceQty[id] = qty;

        updateServices();
    });

    function updateOptions(initial = false) {
        //if(initial && Object.keys(selectedOptions).length == 0) return;
        var divSelected = $('#divSelected');
        divSelected.html('')
        servicesSelect.forEach(function(el,id){
            //console.log(el, serviceQty[el])
            if(!initial || !selectedOptions[el]) selectedOptions[el] = {}
            for(var i = 0; i < serviceQty[el]; i++){
                //console.log('-', i, '/', serviceQty[el])
                if(!initial || !selectedOptions[el][i]) selectedOptions[el][i] = {}
                var ulOptions = $('<ul class="list-group"></ul>');
                allOptions[el].forEach(function(option){
                    var elOption = $('<li class="list-group-item"></li>')
                    var elLabel = $(`<span class="prefix">${option.name}:</span>`)
                    elOption.append(elLabel)
                    var elInput = null
                    switch(option.type){
                        case 'integer':
                        elInput = $('<input type="integer" />')
                        break
                        case 'select':
                        elInput = $('<select/>')
                        option.options.forEach(function(_option){
                            elInput.append(`<option value="${_option}">${_option}</option>`)
                        })
                        break
                        case 'text':
                        elInput = $('<input type="text" />')
                        break
                    }

                    if(initial) $(elInput).val(selectedOptions[el][i][option.name])
                    if(!initial) selectedOptions[el][i][option.name] = $(elInput).val()

                    elInput.change(function(el, i){
                        selectedOptions[el][i][option.name] = $(elInput).val()

                         $('#options').val(JSON.stringify(selectedOptions))
                    }.bind(this, el, i))

                    elOption.append(elInput)
                    ulOptions.append(elOption)
                })

                var elOptions = `<div class="panel panel-default">
                  <div class="panel-heading" role="tab" id="${el}-${i}">
                    <h4 class="panel-title">
                      <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse${el}-${i}" aria-expanded="true" aria-controls="collapseOne">
                        ${allNames[el]}
                      </a>
                    </h4>
                  </div>
                  <div id="collapse${el}-${i}" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="${el}-${i}">
                    <div class="panel-body" id="options${el}-${i}">

                    </div>
                  </div>
                </div>`
                divSelected.append(elOptions)
                $(`#options${el}-${i}`).append(ulOptions)
            }
        })
    }

    function updateServices() {
        var result = [];
        
        servicesSelect.forEach(function(el,id){
            result.push(el + ':' + serviceQty[el]);
        })

        updateOptions()
        $('#services').val(result.join(','));
    }
</script>
@endsection