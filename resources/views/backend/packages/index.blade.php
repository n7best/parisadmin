@extends ('backend.layouts.master')

@section('content')

    <div class="container">

        <h1 class="pull-left">packages</h1>
        <a class="btn btn-primary pull-right" style="margin-top: 25px" href="{!! route('admin.packages.create') !!}">Add New</a>

        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>

        @if($packages->isEmpty())
            <div class="well text-center">No packages found.</div>
        @else
            @include('backend.packages.table')
        @endif
        
    </div>
@endsection