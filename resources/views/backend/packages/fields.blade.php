
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Name:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>


<div class="form-group col-sm-6">
    {!! Form::label('display_name', 'Display Name:') !!}
    {!! Form::text('display_name', null, ['class' => 'form-control']) !!}
</div>

<div class="row">
  <div class="form-group col-sm-6">
      {!! Form::label('services', 'Services:') !!}
      <br/>
      {!! Form::hidden('services', null, ['id' => 'services']) !!}
      {!! Form::hidden('options', null, ['id' => 'options']) !!}
      @foreach($services as $service)
          <div class="row">
            <div class="col-xs-12">
              <div class="input-group">
                <span class="input-group-addon">
                  <input type="checkbox" class="service" id="cbox{!! $service->id !!}" value="{!! $service->id !!}">
                </span>
                <span class="input-group-addon"> <input class="quantity" data-id="{!! $service->id !!}" id="cqty{!! $service->id !!}" type="number" style="width: 40px;" value="1"></span>
                <span class="form-control" >{!!$service->display_name !!}</span>

              </div><!-- /input-group -->
            </div><!-- /.col-lg-6 -->
          </div><!-- /.row -->
      @endforeach
  </div>
  <div class="col-sm-6">
    <label>Selected</label>
      <div class="panel-group" id="divSelected" role="tablist" aria-multiselectable="true">

      </div>
  </div>
</div>



<div class="form-group col-sm-6">
    {!! Form::label('price', 'Price:') !!}
    {!! Form::text('price', null, ['class' => 'form-control']) !!}
</div>


<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('admin.packages.index') !!}" class="btn btn-default">Cancel</a>
</div>
