<table class="table table-responsive">
    <thead>
    <th>Id</th>
			<th>Name</th>
			<th>Display Name</th>
			<th>Services</th>
			<th>Price</th>
    <th width="50px">Action</th>
    </thead>
    <tbody>
    @foreach($packages as $package)
        <tr>
            <td>{!! $package->id !!}</td>
			<td>{!! $package->name !!}</td>
			<td>{!! $package->display_name !!}</td>
			<td>{!! $package->services !!}</td>
			<td>{!! $package->price !!}</td>
            <td>
                <a href="{!! route('admin.packages.edit', [$package->id]) !!}"><i class="glyphicon glyphicon-edit"></i></a>
                <a href="{!! route('admin.packages.delete', [$package->id]) !!}" onclick="return confirm('Are you sure wants to delete this package?')">
                    <i class="glyphicon glyphicon-trash"></i>
                </a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>