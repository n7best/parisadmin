@extends ('backend.layouts.master')

@section ('title', 'Service')

@section('page-header')
    <h1>
        Service Manage
        <small>hahah</small>
    </h1>
@endsection

@section('content')
    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">Services</h3>

            <div class="box-tools pull-right">
                @include('backend.services.includes.header-buttons')
            </div>
        </div><!-- /.box-header -->

        <div class="box-body">
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Display Name</th>
                        <th>{{ trans('labels.general.actions') }}</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach ($services as $service)
                            <tr>
                                <td>{!! $service->id !!}</td>
                                <td>{!! $service->name !!}</td>
                                <td>{!! $service->display_name !!}</td>
                                <td>{!! $service->action_buttons !!}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>

            <div class="pull-left">
                {!! $services->count() !!} services total
            </div>
<!-- 
            <div class="pull-right">
                haha
            </div>
 -->
            <div class="clearfix"></div>
        </div><!-- /.box-body -->
    </div><!--box-->
@stop
