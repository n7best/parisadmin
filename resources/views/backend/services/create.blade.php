@extends ('backend.layouts.master')

@section ('title', 'Services Management' . ' | ' . 'Create')

@section('page-header')
    <h1>
        Service Management
        <small>Create Service</small>
    </h1>
@endsection

@section('content')
    {!! Form::open(['route' => 'admin.services.store', 'class' => 'form-horizontal', 'role' => 'form']) !!}

        <div class="box box-success">
            <div class="box-header with-border">
                <h3 class="box-title">Create Service</h3>

                <div class="box-tools pull-right">
                    @include('backend.services.includes.header-buttons')
                </div>
            </div><!-- /.box-header -->

            <div class="box-body" id="serviceForm">
                <div class="form-group">
                    {!! Form::label('name', 'Service Name', ['class' => 'col-lg-2 control-label']) !!}
                    <div class="col-lg-10">
                        {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Service Name']) !!}
                    </div>
                </div><!--form control-->

                <div class="form-group">
                    {!! Form::label('display_name', 'Display Name', ['class' => 'col-lg-2 control-label']) !!}
                    <div class="col-lg-10">
                        {!! Form::text('display_name', null, ['class' => 'form-control', 'placeholder' => 'Display Name']) !!}
                    </div>
                </div><!--form control-->

                <div class="form-group">
                    {!! Form::label('catalog', 'Catalog', ['class' => 'col-lg-2 control-label']) !!}
                    <div class="col-lg-10">
                        {!! Form::text('catalog', null, ['class' => 'form-control', 'placeholder' => 'Catalog']) !!}
                    </div>
                </div><!--form control-->
            </div><!-- /.box-body -->
        </div><!--box-->

        <div class="box box-success">
            <div class="box-header with-border">
                <h3 class="box-title">Metas</h3>

                <div class="box-tools pull-right">
                    <a href="javascript:;" class="btn btn-success btn-xs" id="addMeta">Add Meta</a>
                </div>
            </div><!-- /.box-header -->
            <div class="box-body" id="metaBox"></div><!-- /.box-body -->
        </div><!--box-->

        <div class="box box-success">
            <div class="box-body">
                <div class="pull-left">
                    <a href="{!! route('admin.access.roles.permissions.index') !!}" class="btn btn-danger btn-xs">{{ trans('buttons.general.cancel') }}</a>
                </div>

                <div class="pull-right">
                    <input type="submit" class="btn btn-success btn-xs" value="{{ trans('buttons.general.crud.create') }}" />
                </div>
                <div class="clearfix"></div>
            </div><!-- /.box-body -->
        </div><!--box-->

    {!! Form::close() !!}
@stop

@section('after-scripts-end')
    {!! Html::script('js/backend/services/create.js') !!}
@stop