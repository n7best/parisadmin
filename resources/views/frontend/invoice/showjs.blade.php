$('document').ready(function(){
  $.fn.select2.defaults.set( "theme", "bootstrap" );
  $(".user-select-ajax").select2({
    theme: "bootstrap",
    ajax: {
      url: "{!! route('api.selects') !!}?d=user",
      dataType: 'json',
      delay: 250,
      cache: true,
      processResults: function (data) {
        return {
          results: data
        };
      }
    },
    minimumInputLength: 1,
  });

  //initialize all ediatable field
  var fields = ['orderPrice'];

  fields.forEach(function(field){
    $('#'+field).editable();
    $('#'+field).on('save',function(e,params){
      $('#formOrderPrice').val(params.newValue);
    });
  });

  //status select xedit
  $('.orderStatus').editable({
      value: '{!! $order->status !!}',
      source: option2source({!! $order->avaible_options!!})
  }).on('save',function(e, params){
        console.log(params.newValue);
        $('#formOrderStatus').val(params.newValue);
  });

  //select2
  $('.optselect').select2()


  //print options

  var prtFields = ['chkPrintInvoice','chkPrintPolicy','chkPrintTips'];
  prtFields.forEach(function(field){
    $('#'+field).bootstrapSwitch();
  });

  $('#btnPrintGenerate').click(function(){
    var url = "{{route('frontend.pdf', $order->id)}}?"
    if($('#chkPrintInvoice').bootstrapSwitch('state')) url += '&invoice=1'
    if($('#chkPrintPolicy').bootstrapSwitch('state')) url += '&terms=1'
    if($('#chkPrintTips').bootstrapSwitch('state')) url += '&tips=1'
    window.open(url);
  })

  tasks.forEach(function(task){
    //status
    $('.'+task.class).editable({
        value: task.status,
        source: option2source(task.options)
    }).on('save',function(e, params){
        console.log(params.newValue);
        $('#'+task.id).val(params.newValue);
    });

    //gallery
    if(task.gallery){
      $('#'+task.galleryel).magnificPopup({
          delegate: 'a',
          gallery: {
            enabled: true
          },
          type: 'image' // this is default type
      })

      $('#'+task.btnDlAll).click(function(){
        var btn = $(this)
        var ogText = btn.html()
        btn.button('loading')
        console.log('dl all')
        var Quene = []
        var finish = 0
        var zip = new JSZip();
        $('#'+task.galleryel+ ' > a').each(function(){
          var url = $(this).attr('href')
          console.log($(this).attr('href'))
          var p = new Promise((resolve, reject) => {
            JSZipUtils.getBinaryContent(url, function (err, data) {
               if(err) {
                  console.log(err); // or handle the error
               }
               zip.file(url.split('/').pop(), data, {binary:true});
              finish++
              btn.html('Downloading ' + finish + '/' + Quene.length + 'Files...')
               resolve(true)
            })
          })
          Quene.push(p)
        })

        btn.html('Downloading ' + finish + '/' + Quene.length + 'Files...')

        Promise.all(Quene).then(function(data){
          btn.button('reset')
          return zip.generateAsync({type:"blob"})
        }).then(function(content){
          saveAs(content, task.tid + "all.zip");
        })
      })

      if(task.btnDLSelected){
        $('#'+task.btnDLSelected).click(function(){
          var btn = $(this)
          var ogText = btn.html()
          btn.button('loading')
          console.log('dl Sel')
          var Quene = []
          var finish = 0
          var zip = new JSZip();

          $('#'+task.galleryel+ ' > .picked').each(function(){
            var url = $(this).attr('href')
            console.log($(this).attr('href'))
            var p = new Promise((resolve, reject) => {
              JSZipUtils.getBinaryContent(url, function (err, data) {
                 if(err) {
                    console.log(err); // or handle the error
                 }
                 zip.file(url.split('/').pop(), data, {binary:true});
                 finish++
                 btn.html('Downloading ' + finish + '/' + Quene.length + 'Files...')
                 resolve(true)
              })
            })
            Quene.push(p)
          })

          btn.html('Downloading ' + finish + '/' + Quene.length + 'Files...')

          Promise.all(Quene).then(function(data){
            btn.button('reset')
            return zip.generateAsync({type:"blob"})
          }).then(function(content){
            saveAs(content, task.tid + "selected.zip");
          })
        })
      }
    }
    //schedule
    if(task.schedule){

      function updateDateTime(){
        let allday = $(`#${task.allday_id}`).prop('checked');
        let startdatetime = moment(`${$(`#${task.detail_id} .date.start`).val()} ${ allday ? "" : $(`#${task.detail_id} .time.start`).val()}`,'MM/DD/YYYY h:mm a');
        let enddatetime = moment(`${$(`#${task.detail_id} .date.end`).val()} ${ allday ? "" : $(`#${task.detail_id} .time.end`).val()}`,'MM/DD/YYYY h:mm a');

        $('#'+task.scheduleEnd_id).val(enddatetime.format());
        $('#'+task.schedule_id).val(startdatetime.format());
        return {
          allday,
          startdatetime,
          enddatetime
        }
      }

      function updateAllDay(){
        let allday = $(`#${task.allday_id}`).prop('checked');
        if(allday){
          $(`#${task.detail_id} .time`).hide();
        }else{
          $(`#${task.detail_id} .time`).show();
        }
      }

      var startDate = task.startDate == 'null' ? null : moment(task.startDate);
      var endDate = task.endDate == 'null' ? null : moment(task.endDate);

      $(`#${task.detail_id} .time`).timepicker({
          'showDuration': true,
          'timeFormat': 'g:i a'
      });

      $(`#${task.detail_id} .time`).on('change', (e)=>{
         updateDateTime()
      });

      $(`#${task.detail_id} .date`).datepicker({
          'format': 'mm/dd/yyyy',
          'autoclose': true
      });

      $(`#${task.detail_id} .date`).datepicker()
        .on('changeDate', (e)=>{
          console.log(e, updateDateTime());
      });

      $(`#${task.allday_id}`).change(function(e){
        updateAllDay()
      })

      // initialize datepair
      $(`#${task.detail_id}`).datepair();

      let allday = $(`#${task.allday_id}`).prop('checked');
      if(task.startDate){
        $(`#${task.detail_id} .date.start`).datepicker('update', moment(task.startDate).format());
        if(!allday) $(`#${task.detail_id} .time.start`).timepicker('setTime', moment(task.startDate).toDate());
      }else{
        $(`#${task.detail_id} .date.start`).datepicker('update', moment().format());
        $(`#${task.detail_id} .time.start`).timepicker('setTime', new Date());
      }

      if(task.endDate){
        $(`#${task.detail_id} .date.end`).datepicker('update', moment(task.endDate).format());
        if(!allday) $(`#${task.detail_id} .time.end`).timepicker('setTime', moment(task.endDate).toDate());
      }else{
        $(`#${task.detail_id} .date.end`).datepicker('update', moment().format());
        $(`#${task.detail_id} .time.end`).timepicker('setTime', moment().add(1,'hours').toDate());
      }

      $(`#${task.detail_id}`).datepair('refresh');
      updateAllDay();

    }
  })
  //convert backend result to xedit format
  function option2source(options){
    var source = [];
    options.forEach(function(status){
      source.push({value:status.type, text: status.type.charAt(0).toUpperCase() + status.type.slice(1)});
    });
    return source;
  }
});