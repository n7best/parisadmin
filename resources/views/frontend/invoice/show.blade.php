@extends('frontend.layouts.master')
@section('content')
    <div class="row">

        <div class="col-md-12">

            <div class="panel panel-default has-inner-drawer">
                <div id="drawerLogs" class="drawer drawer-inside dw-xs-5 fold" aria-labelledby="drawerLogs">
                        <div class="drawer-contents">
                            @include('frontend.includes.chatbox')
                        </div>
                    </div>
                <div class="panel-heading clearfix">
                <i class="fa fa-home"></i> {!! $order->rid !!}
                <div class="btn-group pull-right">
                    <a href="#drawerLogs" data-toggle="drawer" href="#drawerLogs" aria-foldedopen="false" aria-controls="drawerLogs" class="btn btn-primary btn-sm">Logs</a>
                    <a id="btnPrintInvoice" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#printOption">Print</a>
                </div>
                </div>

                <div class="panel-body">
                    <div>

                      <!-- Nav tabs -->
                      <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="{!! $nav ?  "" : "active" !!}"><a href="#order" aria-controls="order" role="tab" data-toggle="tab">Order</a></li>
                        <li role="presentation" class="{!! $nav ?  "active" : "" !!}"><a href="#tasks" aria-controls="tasks" role="tab" data-toggle="tab">Tasks</a></li>
                      </ul>

                      <!-- Tab panes -->
                      <div class="tab-content">
                        <div role="tabpanel" class="tab-pane {!! $nav ?  "" : "active" !!}" id="order">
                            <br/>
                            {!! Form::model($order, ['route' => ['invoice.update', $order->id], 'method' => 'patch']) !!}
                            <div class="row">
                                <div class="col-md-6">
                                    <ul class="list-group">
                                        <li class="list-group-item">
                                            <h4>Order Info</h4>
                                        </li>
                                        <li class="list-group-item">
                                            <span class="prefix">Status:</span>
                                            <span class="orderStatus" id="orderStatus" data-pk="{!! $order->id !!}" data-type="select">{!! $order->status !!}</span>
                                            <input type="hidden" id="formOrderStatus" value="{!! $order->status !!}" name="status"></input>
                                        </li>
                                        <li class="list-group-item">
                                            <span class="prefix">Date created:</span>
                                            <span class="label label-default">{!! $order->created_at !!}</span>
                                        </li>
                                        <li class="list-group-item">
                                            <span class="prefix">Last update:</span>
                                            <span class="label label-default">{!! $order->updated_at !!}</span>
                                        </li>

                                        <li class="list-group-item">
                                            <span class="prefix">Pakcage:</span>
                                            <span class="label label-default">{!! $order->package ? $order->package->display_name : 'None' !!}</span>
                                        </li>

                                        <li class="list-group-item">
                                            <span class="prefix">Total Price:</span>
                                            $<span id="orderPrice">{!! $order->price !!}</span>
                                            <input type="hidden" id="formOrderPrice" value="{!! $order->price !!}" name="price"></input>
                                            <span class="prefix">Tax:</span>
                                            <span class="label label-default" id="orderTax">${!! $order->tax !!}</span>
                                            <span class="prefix">Services:</span>
                                            <span class="label label-default" id="orderServices">${!! $order->price_services !!}</span>
                                            <span class="prefix">Discounts:</span>
                                            <span class="label label-default" >${!! $order->discounts !!}</span>
                                            <span class="prefix">Adjustment:</span>
                                            <span class="label label-default" >${!! $order->adjustment !!}</span>
                                        </li>

                                        <li class="list-group-item">
                                            <span class="prefix">Balance:</span>
                                            <span class="label label-default">{!! $order->balance !!}</span>
                                        </li>

                                        <li class="list-group-item">
                                            <span class="prefix">Sales:</span>
                                            <style>
                                            .select2-container{
                                                display:inline-block;
                                                width: 80% !important;
                                            }
                                            </style>
                                            <select class="user-select-ajax" multiple="multiple" name="sales_name[]">
                                              @foreach($order->sales_name as $uid => $name)
                                                <option value="{!! $uid !!}" selected="selected">{!! $name !!}</option>
                                              @endforeach
                                            </select>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-md-6">
                                    @include('frontend.invoice.showCustomer')
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <ul class="list-group">
                                        <li class="list-group-item">
                                            <div class="btn-group btn-group-justified" role="group">
                                            {!! Form::submit('Update Order & Customer Info', ['class' => 'btn btn-primary', 'style' => 'width:100%;']) !!}
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            {!! Form::close() !!}
                            <div class="row">
                                <div class="col-md-12">
                                @include('frontend.invoice.showPayment')
                                </div>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane {!! $nav ? "active" : "" !!}" id="tasks">
                            @include('frontend.invoice.showTask')
                        </div>
                      </div>

                    </div>
                </div>
            </div><!-- panel -->

        </div><!-- col-md-10 -->

    </div><!-- row -->

    <!-- Modal -->
    <div class="modal fade" id="printOption" tabindex="-1" role="dialog">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="modalUploadLabel">Print - Chose What to include</h4>
          </div>
          <div class="modal-body">
            <ul class="list-group">
                <li class="list-group-item">
                    <span class="prefix">Invoice:</span>
                    <input type="checkbox" name="customerNotification" id="chkPrintInvoice" checked>
                </li>
                <li class="list-group-item">
                    <span class="prefix">Policy:</span>
                    <input type="checkbox" name="customerNotification" id="chkPrintPolicy" checked>
                </li>
                <li class="list-group-item">
                    <span class="prefix">Tips:</span>
                    <input type="checkbox" name="customerNotification" id="chkPrintTips" checked>
                </li>
            </ul>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary" id="btnPrintGenerate">Generate</button>
          </div>
        </div>
      </div>
    </div>
@endsection

@section('after-scripts-end')
<script>
    @include('frontend.invoice.showjs')
</script>

@endsection