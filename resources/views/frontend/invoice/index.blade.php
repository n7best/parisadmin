@extends('frontend.layouts.master')

@section('content')
    <div class="row">

        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading"><i class="fa fa-home"></i> Invoices</div>

                <div class="panel-body">
                    <table class="table table-bordered table-responsive" id="invoice-table">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Status</th>
                                <th>Package</th>
                                <th>Customer</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                    </table>
                    <!-- Indicates a successful or positive action -->

                    @permission('create-invoice-pa')
                    <a href="{!! route('invoice.create') !!}?c=1" class="btn btn-success">Create New PA Invoice</a>
                    @endauth
                    @permission('create-invoice-la')
                    <a href="{!! route('invoice.create') !!}?c=2" class="btn btn-success">Create New LA Invoice</a>
                    @endauth
                    @permission('create-invoice-ca')
                    <a href="{!! route('invoice.create') !!}?c=3" class="btn btn-success">Create New CA Invoice</a>
                    @endauth
                </div>
            </div><!-- panel -->


        </div><!-- col-md-10 -->

    </div><!-- row -->
@endsection

@section('after-scripts-end')
<script>
    $(function() {
        $('#invoice-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{!! route('frontend.invoice.tabledata') !!}',
            columns: [
                { data: 'rid', name: 'orders.rid' },
                { data: 'status', name: 'orders.status' },
                { data: 'package.display_name', name: 'package.display_name' },
                { data: 'customer.display_name', name: 'customer.display_name' },
                {data: 'action', name: 'action', orderable: false, searchable: false}
            ]
        });
    });
</script>

@endsection