$(function(){
  //invoice start
  //$.fn.editable.defaults.mode = 'inline';
  $(document).ready(function() {

    var newInvoice = {
      _idCounter: new Date().getTime(),
      _services:[],
      _packages:[],
      _packagesSelected:{},
      _packageSerices: [],
      _servicesSelected:[],
      _invoicesDetails:{
        order:{
          company: {!! $company->id !!},
          company_code: '{!! $company->code !!}',
          sales_init: '{!! substr(access()->user()->name,0,1) !!}',
          sales: '{!! access()->user()->id !!}'
        },
        customer:{
          id: false,
          notification: true
        },
        services:{},
        signatures:{
          customer:null,
          officer:null
        },
        coupons:[],
        price:{
          services: 0,
          discounts: 0,
          adjustments: 0,
          tax: 0,
          total: 0
        }
      },

      _init: function(){
        //initialize all ediatable field
        var fields = ['invoiceWeddingDate','invoiceCustomerID','invoiceBrideName','invoiceGroomName','invoiceStudioDate','invoiceCustomerAddress','invoiceCustomerAddress',
                      'invoiceOutdoorDate','invoiceBridePhone','invoiceGroomPhone','invoiceCustomerEmail','invoiceCustomRID'];

        fields.forEach(function(field){
          if($.inArray(field,['invoiceWeddingDate', 'invoiceStudioDate','invoiceOutdoorDate']) > -1){
            var _date = new Date();
            var _date2 = new Date();
            _date.setFullYear(_date.getFullYear() + 10);
            $('#'+field).editable({
              combodate: {
                minYear: _date2.getFullYear(),
                maxYear: _date.getFullYear(),
              }
            });
          }else{
            $('#'+field).editable();
          }

          $('#'+field).on('save',function(e,params){
            newInvoice._invoicesDetails.customer[field] = params.submitValue;
            console.log(newInvoice._invoicesDetails);
          });
        })

        //switch
        $('#chkCustomerNotification').bootstrapSwitch();
        $('#chkCustomerNotification').on('switchChange.bootstrapSwitch', function(event, state) {
          newInvoice._invoicesDetails.customer['notification'] = state;
        });

        //customer search
        $(".customer-select-ajax").select2({
          theme: "bootstrap",
          ajax: {
            url: "{!! route('api.selects') !!}?d=customers",
            dataType: 'json',
            delay: 250,
            cache: true,
            processResults: function (data) {
              return {
                results: data
              };
            }
          },
          minimumInputLength: 1,
          templateResult: function(data) {
            if(data.metas && data.metas.wc_avatar) {
              var $state = $(
                '<span><img src="' + data.metas.wc_avatar + '" width="25px" /> ' + data.text + '</span>'
              );
              return $state;
            }else{
              return data.text
            }
          }
        });
        $(".customer-select-ajax").on("select2:select", function (evt) { 
          if (!evt) {
            var args = {data:false};
          } else {
            var args = evt.params
            newInvoice.customer.update(args.data);
          }
        });


        //initial services value
        newInvoice.get.services();
        newInvoice.get.packages();

        //initial dragging for service select module
        dragula([document.getElementById('servicesSelected'), document.getElementById('servicesAll')],{
          copy: true,
          accepts: function (el, target, source, sibling){
            if(target == document.getElementById('servicesAll')) return false;
            return true;
          }
        })
          .on('drop', function (el, container, source, sibling) {
              if(source == document.getElementById('servicesSelected') && !container && !$(el).data('package')) {
                var elid = $(el).attr('id');
                newInvoice.items.remove(elid);
                $(source).find('div#'+elid).remove();
              }

              if(container){
                var elid =newInvoice.items.add($(el).data('itemid'), $(el).data('catalog'), null, 0, $(el).data('sid'));
                $(el).attr('id', elid);
              }

              newInvoice.items.update();
           });

        dragula([document.getElementById('packagesSelected'), document.getElementById('packagesAll')],{
          copy: true
        })
          .on('drop', function (el, container, source, sibling) {
              if(source == document.getElementById('packagesSelected') && !container) {
                var elid = $(el).attr('id');
                $(source).find('div#'+elid).remove();
                newInvoice.packages.remove(elid);
              }

              if(container){
                var elid = newInvoice.packages.add($(el).data('packageindex'),$(el).data('itemid'));
                $(el).attr('id', elid);

              }

              newInvoice.packages.update();
           });

        $('#btnSaveServices').click(function(){
          $('#modalAddServices').modal('hide');
        });

        $('#btnSubmitInvoice').click(function(){
          var btn = $(this);
          btn.button('loading')
          var data = newInvoice._invoicesDetails;
          data['packages'] = newInvoice._packagesSelected;
          data['__token'] = $('meta[name=_token]').attr("content");
          $.post('/invoice', data)
            .success(function(order){
              if(order['id']){
                window.location.assign("/invoice/"+order['id']);
              }
            })
            .fail(function(data){
              var errors = data.responseJSON;
              var message = '';
              console.log(errors);
              Object.keys(errors).forEach(function(key){
                var error = errors[key];
                error.forEach(function(msg){
                   message += msg + '\n';
                })
              })
              alert(message);
            }).always(function(){
              btn.button('reset')
            });
        })

        //coupon
        newInvoice.coupons.init();

        //initial signature pad
        newInvoice.signatures.init();

      },

      customer: {
        reset: function() {
          $('#chkCustomerNotification').bootstrapSwitch('state', true);
          $('#invoiceGroomName').editable('setValue', '')
          newInvoice._invoicesDetails.customer['groomname'] = ''
          $('#invoiceBrideName').editable('setValue', '')
          newInvoice._invoicesDetails.customer['bridename'] = ''
          $('#invoiceGroomPhone').editable('setValue', '')
          newInvoice._invoicesDetails.customer['groomPhone'] = ''
          $('#invoiceCustomerEmail').editable('setValue', '')
          newInvoice._invoicesDetails.customer['email'] = ''
          $('#invoiceCustomerAddress').editable('setValue', '')
          newInvoice._invoicesDetails.customer['address'] = ''
          $('#invoiceWeddingDate').editable('setValue', '')
          newInvoice._invoicesDetails.customer['weddingDate'] = ''
          $('#invoiceStudioDate').editable('setValue', '')
          newInvoice._invoicesDetails.customer['studioDate'] = ''
          $('#invoiceOutdoorDate').editable('setValue', '')
          newInvoice._invoicesDetails.customer['outdoorDate'] = ''

          newInvoice._invoicesDetails.customer.id = false
        },
        update: function(data) {
          this.reset()
          console.log(data)

          newInvoice._invoicesDetails.customer['id'] = data.id

          $('#invoiceGroomName').editable('setValue', data.groomname)
          newInvoice._invoicesDetails.customer['invoiceGroomName'] = data.groomname

          $('#invoiceBrideName').editable('setValue', data.bridename)
          newInvoice._invoicesDetails.customer['invoiceBrideName'] = data.bridename

          if(data.metas){


            if(data.metas.notification) {
              var notification = (data.metas.notification == 'true')
              $('#chkCustomerNotification').bootstrapSwitch('state', notification);
            }

            if(data.metas.groomPhone) {
              $('#invoiceGroomPhone').editable('setValue', data.metas.groomPhone)
              newInvoice._invoicesDetails.customer['invoiceGroomPhone'] = data.metas.groomPhone
            }

            if(data.metas.email) {
              $('#invoiceCustomerEmail').editable('setValue', data.metas.email)
              newInvoice._invoicesDetails.customer['invoiceCustomerEmail'] = data.metas.email
            }

            if(data.metas.address) {
              $('#invoiceCustomerAddress').editable('setValue', data.metas.address)
              newInvoice._invoicesDetails.customer['invoiceCustomerAddress'] = data.metas.address
            }

            if(data.metas.address) {
              $('#invoiceCustomerAddress').editable('setValue', data.metas.address)
              newInvoice._invoicesDetails.customer['invoiceCustomerAddress'] = data.metas.address
            }

            if(data.metas.weddingDate) {
              $('#invoiceWeddingDate').editable('setValue', data.metas.weddingDate)
              newInvoice._invoicesDetails.customer['invoiceWeddingDate'] = data.metas.weddingDate
            }

            if(data.metas.studioDate) {
              $('#invoiceStudioDate').editable('setValue', data.metas.studioDate)
              newInvoice._invoicesDetails.customer['invoiceStudioDate'] = data.metas.studioDate
            }

            if(data.metas.outdoorDate) {
              $('#invoiceOutdoorDate').editable('setValue', data.metas.outdoorDate)
              newInvoice._invoicesDetails.customer['invoiceOutdoorDate'] = data.metas.outdoorDate
            }
          }
        }
      },

      coupons: {
        init: function() {
          $('#couponSelect').select2();

          $('#btnAddCouponPublic').click(function(){
            var code = $('#couponSelect').val()
            newInvoice.coupons.verify(code, $(this))
          })

          $('#btnAddCouponPrivate').click(function(){
            var code = $('#CouponPrivateText').val()
            newInvoice.coupons.verify(code, $(this))
          })
        },

        updateView: function() {
          $('#appliedCoupons').html('')
          if(newInvoice._invoicesDetails.coupons.length > 0){
            $.each(newInvoice._invoicesDetails.coupons, function(i, coupon){
              $('#appliedCoupons').append('<li class="list-group-item">'+ coupon +'</li>')
            })
          }
        },

        verify: function(code, btn) {
          if(Object.keys(newInvoice._invoicesDetails.services).length < 1){
            alert('Please Add some services first')
          }else{
            console.log(code)
            btn.button('loading')
            $.post('{{route('frontend.verifycoupon')}}', {
              code: code,
              applied: newInvoice._invoicesDetails.coupons.length > 0 ? newInvoice._invoicesDetails.coupons : false,
              services: newInvoice._invoicesDetails.services,
              packages: Object.keys(newInvoice._packagesSelected).length > 0 ? newInvoice._packagesSelected : false,
            }, function(data){
              console.log('success', data)
              if(!data.fail){
                newInvoice._invoicesDetails.price.discounts = data.discounts
                newInvoice._invoicesDetails.coupons = data.allcoupons
                newInvoice.coupons.updateView();
                newInvoice.updateTotal();
                alert(data.message)
              }
            }).fail(function(data){
              console.log('fail', data)
              var res = data.responseJSON

              alert(res.msg)
            }).always(function(){
              btn.button('reset')
            })
          }
        }
      },

      packages: {
        add: function(packageIndex, itemid){
          var elid = itemid + newInvoice.uid();
          var _package = newInvoice._packages[packageIndex];

          var packageInfo = {
            elid: elid,
            serviceIds: [],
            price: _package.price,
            name: _package.display_name,
            pid: _package.name
          };

          _package.services.forEach(function(detail){
              var service = detail['name'];
              var serviceQty = parseInt(detail['qty']);
              var serviceDetail = newInvoice.getService(service);

              var options = _package.options !== null ? _package.options[parseInt(detail['id'])] : {}

              if(serviceQty > 1){
                for (var i = 0; i < serviceQty; i++) {
                  var _elid = newInvoice.items.add(service, serviceDetail.catalog, options[i] ? options[i] : null, 0, serviceDetail.sid);
                  $('#servicesSelected').append('<div class="service__item" data-sid="'+ serviceDetail.sid +'" data-package="'+ elid +'" data-itemid="'+ service +'" data-catalog="'+ serviceDetail.catalog +'" id="'+ _elid +'">'+ serviceDetail.name +'</div>');
                  packageInfo.serviceIds.push(_elid);
                }
              }else {
                var _elid = newInvoice.items.add(service, serviceDetail.catalog, options[0] ? options[0] : null, 0, serviceDetail.sid);
                $('#servicesSelected').append('<div class="service__item" data-sid="'+ serviceDetail.sid +'" data-package="'+ elid +'" data-itemid="'+ service +'" data-catalog="'+ serviceDetail.catalog +'" id="'+ _elid +'">'+ serviceDetail.name +'</div>');
                packageInfo.serviceIds.push(_elid);
              }
          });

          newInvoice._packagesSelected[elid] = packageInfo;

          return elid;
        },
        remove: function(elid){
          //remove all services
          var _package = newInvoice._packagesSelected[elid];

          if(_package){
            var services = newInvoice._packagesSelected[elid].serviceIds;
            //remove service block
            services.forEach(function(_sid){
              console.log(_sid);
              newInvoice.items.remove(_sid);
              $('#servicesSelected > div#'+_sid).remove();
            })
            //remove package
            delete newInvoice._packagesSelected[elid];
          }

        },
        update: function(){
          newInvoice.items.update();
          newInvoice.updateTotal();
        }
      },

      items: {
        add: function(itemid,catalog,options,total,sid) {
          var elid = itemid + newInvoice.uid();
          newInvoice._invoicesDetails.services[elid] = {itemid:itemid,catalog:catalog,ogOptions:options,total:total};
          return elid;
        },
        remove: function(elid) {
          if(newInvoice._invoicesDetails.services[elid]) delete newInvoice._invoicesDetails.services[elid];
          $('#tr'+elid).remove();
        },
        update: function() {
          newInvoice._servicesSelected = [];
          $('#servicesSelected div').each(function(index){
            var item = $(this);
            newInvoice._servicesSelected.push({elid:item.attr('id'),id:item.data('itemid'),sid:item.data('sid'),catalog:item.data('catalog'), name:item.text(),package:item.data('package') });
          });
          newInvoice.sync.servicesSelected();
        }
      },

      signatures: {
        init: function() {
          var canvas = document.getElementById('customerSignCanvas');
          var signaturePad = new SignaturePad(canvas);

          $('#modalCustomerSign').on('shown.bs.modal', function () {
            newInvoice.resizeCanvas(canvas);
          });

          $('#clearCustomerSign').click(function(){
            signaturePad.clear();
          });

          $('#saveCustomerSign').click(function(){
            var _signImg = signaturePad.toDataURL();
            var el = '<img src="'+ _signImg +'" width="100%" height="50px"/>';

            newInvoice._invoicesDetails.signatures.customer = _signImg;
            $('#customerSignaturePreview').html(el);
            $('#modalCustomerSign').modal('hide');
          });

          var canvas2 = document.getElementById('officerSignCanvas');
          var signaturePad2 = new SignaturePad(canvas2);

          $('#modalOfficerSign').on('shown.bs.modal', function () {
            newInvoice.resizeCanvas(canvas2);
          });

          $('#clearOfficerSign').click(function(){
            signaturePad2.clear();
          });

          $('#saveOfficerSign').click(function(){
            var _signImg = signaturePad2.toDataURL();
            var el = '<img src="'+ _signImg +'" width="100%" height="50px"/>';

            newInvoice._invoicesDetails.signatures.officer = _signImg;
            $('#officerSignaturePreview').html(el);
            $('#modalOfficerSign').modal('hide');
          });
        }
      },

      get: {
        services: function() {
          var reqServices = $.get( "/api/v1/services", function(data) {
            newInvoice._services = $.parseJSON(data);
            console.log(newInvoice._services);
            newInvoice._services.forEach(function(service){
              $('#servicesAll').append('<div class="service__item '+ service.catalog+'" data-sid="'+ service.sid +'" data-itemid="'+ service.id +'" data-catalog="'+ service.catalog +'">' + service.name + '</div>');
            });

            $('#modalAddServices').one('shown.bs.modal', function () {
              //patch1 update height
              $('#servicesSelected').height($('#servicesAll').height());
            })

          })
        },
        packages: function() {
          var reqPackages = $.get( "/api/v1/packages", function(data) {
            newInvoice._packages = data;
            console.log(newInvoice._packages);
            newInvoice._packages.forEach(function(package,i){
              $('#packagesAll').append('<div class="package__item" data-itemid="'+ package.name +'" data-packageindex="'+i +'">' + package.display_name + '</div>');
            });

            $('#modalAddPackage').one('shown.bs.modal', function () {
              //patch1 update height
              $('#packagesSelected').height($('#packagesAll').height());
            })

          })
        }
      },

      sync: {
        servicesSelected: function(){
          var _container = $('.services__selected');

          newInvoice._servicesSelected.forEach(function(item){
              var _uids = [];
              var _values = {};
              var _options = {}, _types = {};
              //check if it's already exists, otherwise create, if over delete.
              //if(!newInvoice._invoicesDetails.services[item.elid].options){
                //mark
                newInvoice._invoicesDetails.services[item.elid]['options'] = {};
                newInvoice._invoicesDetails.services[item.elid]['sid'] = item.sid;
                var elmetas = '';
                var metas = newInvoice.getMeta(item.id);

                metas.forEach(function(meta){
                  //console.log(newInvoice._invoicesDetails.services[item.elid].ogOptions)
                  var uid = meta.type + newInvoice.uid();
                  _uids.push(uid);
                  _types[uid] = meta.type;

                  //add to values
                  newInvoice._invoicesDetails.services[item.elid]['options'][uid] = {
                    type:meta.type,
                    name:meta.name,
                    value: newInvoice._invoicesDetails.services[item.elid].ogOptions[meta.name] ? newInvoice._invoicesDetails.services[item.elid].ogOptions[meta.name] : null,
                    price_factor:meta.price_factor
                  };

                  var _value = newInvoice._invoicesDetails.services[item.elid]['options'][uid].value
                  _values[uid] = _value

                  elmetas = elmetas + '<td><label>'+meta.name+'</label>'
                  switch(meta.type){
                    case 'integer':
                      elmetas += '<a href="#" id="'+ uid +'" data-type="text" data-pk="1" data-title="Enter '+meta.type+'" value="'+ _value +'"></a></td>';
                      break;
                    case 'select':
                      elmetas += '<a href="#" id="'+ uid +'" data-type="select" data-pk="1" data-value="'+ _value +'" data-title="Enter '+meta.type+'"></a>';
                      _options[uid] = meta.options;
                      break;
                    default:
                      elmetas += '<a href="#" id="'+ uid +'" data-type="text" data-pk="1" data-title="Enter '+meta.type+'" value="'+ _value +'"></a></td>';
                  }
                });

                var price = item.package ? newInvoice._packagesSelected[item.package].name : newInvoice._invoicesDetails.services[item.elid]['total'];
                var elPrice = item.package ? '<span id="total' + item.elid + '"> '+ price +'</span>' : '$<span id="total' + item.elid + '"> '+ price +'</span>';

                var element =
                  '<tr id="tr' + item.elid + '" class="'+ item.catalog +'">' +
                  ' <td width="20%">' +
                  '  <label>Catalog</label><span class="table-catalog">' + item.catalog +
                  ' </span></td>' +
                  ' <td width="20%">' +
                  '  <label>Service</label>' + item.name +
                  ' </td>' + elmetas +
                  ' <td>' +
                  '   <label>Price</label>' + elPrice +
                  ' </td>' +
                  '</tr>';

                _container.append(element);

                _uids.forEach(function(elid){
                  switch(_types[elid]){
                    case 'select':
                      $('#'+elid).editable({
                        source: _options[elid].map(function(item,i){ return {value: item, text: item}; }),
                        //value: _values[elid]
                      });
                      break;
                    default:
                      $('#'+elid).editable({
                        value: _values[elid]
                      });
                      break;
                  }
                  $('#'+elid).on('save', function(e, params) {
                      newInvoice._invoicesDetails.services[item.elid]['options'][elid]['value'] = params.submitValue;
                      //counting price
                      var total = 0;
                      Object.keys(newInvoice._invoicesDetails.services[item.elid]['options']).forEach(function(key){
                        var meta = newInvoice._invoicesDetails.services[item.elid]['options'][key];
                        switch(_types[elid]){
                          case 'integer':
                            if(typeof meta.price_factor[0] !== 'undefined' && meta.price_factor[0] ){
                              total+= parseInt(meta.price_factor[0]);
                            }
                            break;
                          case 'select':
                            if(typeof meta.price_factor !== 'undefined' && meta.price_factor ){
                              metas.forEach(function(_meta){
                                if(_meta.name == meta.name) {
                                  _meta.options.forEach(function(_value,index){
                                    if(_value == params.submitValue) total+= parseInt(meta.price_factor[index]);
                                  });
                                }
                              });
                            }
                            break;
                        }
                      });

                      if(!item.package){
                        newInvoice._invoicesDetails.services[item.elid]['total'] = total;
                        $('#total'+item.elid).text(total);
                      }

                      newInvoice.updateTotal();
                      console.log(newInvoice._invoicesDetails);
                  });
                });
          });

        }
      },

      updateTotal: function(){
        var nyctax = __paTax,
            serviceTotal = 0,
            invoiceTax = 0,
            invoiceTotal = 0,
            lblServiceTotal = $('#serviceTotal'),
            lblInvoiceTax = $('#invoiceTax'),
            lblInvoiceDiscounts = $('#invoiceDiscounts'),
            lblInvoiceTotal = $('#invoiceTotal');

            Object.keys(newInvoice._invoicesDetails.services).forEach(function(i){
              var service = newInvoice._invoicesDetails.services[i];
              serviceTotal += service.total;
            });

            Object.keys(newInvoice._packagesSelected).forEach(function(key){
              var _package = newInvoice._packagesSelected[key]
              serviceTotal += _package.price;
            });

            serviceTotal = serviceTotal - newInvoice._invoicesDetails.price.discounts

            if(serviceTotal < 0) serviceTotal = 0;
            invoiceTax = serviceTotal * nyctax;

            if(invoiceTax < 0) invoiceTax = 0;
            invoiceTax = invoiceTax.toFixed(2);

            invoiceTotal = serviceTotal;

            serviceTotal = invoiceTotal - invoiceTax;


            newInvoice._invoicesDetails.price.services = serviceTotal;
            newInvoice._invoicesDetails.price.tax = invoiceTax;
            newInvoice._invoicesDetails.price.total = invoiceTotal;

            lblServiceTotal.text(serviceTotal);
            lblInvoiceTax.text(invoiceTax);
            lblInvoiceTotal.text(invoiceTotal);
            lblInvoiceDiscounts.text(newInvoice._invoicesDetails.price.discounts)
      },

      uid: function(){
        newInvoice._idCounter++;
        return newInvoice._idCounter;
      },

      getMeta: function(id){
        var metas;
        newInvoice._services.forEach(function(item){
          if(item.id == id) metas = item.metas;
        });
        return metas;
      },

      getService: function(id){
        var serviceDetail;
        newInvoice._services.forEach(function(_service){
          if(_service.id == id) serviceDetail = _service;
        });
        return serviceDetail;
      },

      resizeCanvas: function(canvas){
        canvas.width = $(canvas).parent().outerWidth();
        canvas.height = $(canvas).parent().height();
      }

    };

    newInvoice._init();

  });
  //invoice end
});