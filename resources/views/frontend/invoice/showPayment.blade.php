@permission('create-payment')
<ul class="list-group">
    <li class="list-group-item">
        <h4>Payments</h4>
    </li>
    {!! Form::model($payment, ['route' => ['payment.store'], 'method' => 'post']) !!}
    <li class="list-group-item">
        <input type="hidden" name="orderid" value="{!! $order->id !!}">
        <span class="prefix">Add:</span>
        <div class="input-group">
         <span class="input-group-addon" style="padding: 0;">
             <select class="form-control" style="width: 100px;" name="type">
                <option value="cash" selected>Cash</option>
                <option value="creditcard">Credit Card</option>
                <option value="debitcard">Debit Card</option>
                <option value="wire">Bank Wire</option>
                <option value="check">Check</option>
                <option value="quickpay">Chase Quickpay</option>
                <option value="pmcredit">Promotional Credit</option>
             </select>
         </span>
         <input type="number"  step="0.01" class="form-control" placeholder="Enter Payment Amount..." name="amount">
         <span class="input-group-btn">
           {!! Form::submit('Add', ['class' => 'btn btn-primary']) !!}
         </span>
       </div><!-- /input-group -->
    </li>
    <li class="list-group-item">
        <h4>Payment History</h4>
    </li>
    <li class="list-group-item">
        <ul class="list-group">
        @foreach($payment as $single)
            <li class="list-group-item"><span class="label label-default">{{$single->status}}</span>${{$single->amount}} - {{$single->created_at}}</li>
        @endforeach
        </ul>
    </li>
    {!! Form::close() !!}
</ul>
@endauth