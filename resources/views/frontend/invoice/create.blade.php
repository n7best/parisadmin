@extends('frontend.layouts.master')

@section('content')
    <div class="row">

        <div class="col-md-10 col-md-offset-1">

            <div class="panel panel-default">
                <div class="panel-heading">
                New Invoice
                </div>

                <div class="panel-body">
                    <div class="invoice">
                        <div class="invoice__id">
                            @if(Access::allow('edit-rid'))
                                 {!! $company->code !!}:<a href="#" id="invoiceCustomRID" data-type="text" data-pk="1" data-title="Enter Custom ID">{!! substr(access()->user()->name,0,1) !!} <span>----</span></a>
                            @else
                                {!! $company->code !!}: {!! substr(access()->user()->name,0,1) !!} <span>----</span>
                            @endif
                        </div>
                        <div class="header">
                            <div class="company">
                                <img src="{!! $company->logo !!}" style="height: 100px; width: auto; margin: 10px;"/>
                                <p>{!! $company->address !!}</p>
                                <p>Tel: {!! $company->phones !!}</p>
                                <p>Email: {!! $company->email !!}</p>
                            </div>
                            <div class="infobar">
                                
                            </div>
                            <div class="invoice__detail">
                                <h4>Customer Info</h4>
                                <table id="userOptions" class="table table-bordered table-striped" style="clear: both">
                                <tbody>
                                  <tr>
                                      <td width="33%">
                                          <label>Customer Notification</label>
                                          <input type="checkbox" name="customerNotification" id="chkCustomerNotification" checked>
                                      </td>
                                      <td width="33%">
                                      <label>Customer Search</label>
                                      <style>
                                      .select2-container{
                                          display:inline-block;
                                          width: 80% !important;
                                      }
                                      </style>
                                        <select class="customer-select-ajax" name="assign_name[]">
                                        </select>
                                      </td>
                                      <td width="33%"></td>
                                  </tr>
                                </tbody>
                                </table>
                                <table id="userDetail" class="table table-bordered table-striped" style="clear: both">
                                    <tbody>
                                        
                                        <tr>
                                            <td width="65%">
                                                <div class="col-xs-6 xs-pl-0">
                                                    <label>Name (Bride)</label>
                                                    <a href="#" id="invoiceBrideName" data-type="text" data-pk="1" data-title="Enter Bride Name"></a>
                                                </div>
                                                <div class="col-xs-6">
                                                    <label>Name (Groom)</label>
                                                    <a href="#" id="invoiceGroomName" data-type="text" data-pk="1" data-title="Enter Groom Name"></a>
                                                </div>
                                            </td>
                                            <td width="35%">
                                                <label>Wedding Date</label>
                                                <a href="#" id="invoiceWeddingDate" data-type="combodate" data-format="YYYY-MM-DD" data-viewformat="MM/DD/YYYY" data-template="MMM / D / YYYY" data-pk="1"  date-maxYear="2026" data-title="Select Wedding Date"></a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="65%">
                                                <label>Address</label>
                                                <a href="#" id="invoiceCustomerAddress" data-type="text" data-pk="1" data-title="Enter Address"></a>
                                            </td>
                                            <td width="35%">
                                                <label>Studio Date</label>
                                                <a href="#" id="invoiceStudioDate" data-type="combodate" data-format="YYYY-MM-DD" data-viewformat="MM/DD/YYYY" data-template="MMM / D / YYYY" data-pk="1"  data-title="Select Studio Date"></a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="65%">
                                                <div class="col-xs-6 xs-pl-0">
                                                    <label>Bride #</label>
                                                    <a href="#" id="invoiceBridePhone" data-type="text" data-pk="1" data-title="Enter Bride Number"></a>
                                                </div>
                                                <div class="col-xs-6">
                                                    <label>Groom #</label>
                                                    <a href="#" id="invoiceGroomPhone" ata-type="text" data-pk="1" data-title="Enter Groom Number"></a>
                                                </div>
                                            </td>
                                            <td width="35%">
                                                <label>Outdoor Date</label>
                                                <a href="#" id="invoiceOutdoorDate" data-type="combodate" data-format="YYYY-MM-DD" data-viewformat="MM/DD/YYYY" data-template="MMM / D / YYYY" data-pk="1"  data-title="Select Outdoor Date"></a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="65%">
                                                <label>Email</label>
                                                <a href="#" id="invoiceCustomerEmail" data-type="text" data-pk="1" data-title="Enter Customer Email"></a>
                                            </td>
                                            <td width="35%"></td>
                                        </tr>
                                    </tbody>
                                </table>
                                <h4>Services</h4>
                                <div id="services" class="services">
                                    <button class="btn btn-default" data-toggle="modal" data-target="#modalAddServices">Add Service</button>
                                    <button class="btn btn-default" data-toggle="modal" data-target="#modalAddPackage">Add Package</button>
                                    <div>
                                        <table class="table table-bordered table-striped xs-mt-15" style="clear: both">
                                            <tbody class="services__selected">
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <h4>Adjustment</h4>
                                <button class="btn btn-default" data-toggle="modal" data-target="#modalAddPromotion">Add Promotion Code</button>
                                {{-- <div id="adjustment" class="invoice__adjustment">
                                </div> --}}
                                <h4>Summary</h4>
                                <div id="summary" class="invoice__summary">
                                    <p>Services: $<span id="serviceTotal">0</span></p>
                                    <p>Discounts: $<span id="invoiceDiscounts">0</span></p>
                                    <p>Sales Tax: $<span id="invoiceTax">0</span></p>
                                    <p>Total: $<span id="invoiceTotal">0</span></p>
                                </div>
                                <h4>Store Policy</h4>
                                <div id="storePolicy" class="storePolicy">
                                    {!! App\Models\Template::render('store_policy') !!}
                                </div>
                                <h4>Signitures</h4>
                                <div id="signs" class="invoice__signs">
                                  <div class="row">
                                    <div class="col-xs-6 dflex">
                                      <label style="min-width: 130px;">
                                      Customer Signature
                                      <p>顾客签名</p>
                                      </label>
                                      <div id="customerSignature" class="invoice__signs--area">
                                        <button type="button" id="customerSignaturePreview" class="btn btn-default" data-toggle="modal" data-target="#modalCustomerSign">Click To Sign</button>
                                      </div>
                                    </div>
                                    <div class="col-xs-6 dflex">
                                      <label style="min-width: 130px;">
                                      Officer's Signature
                                      <p>工作人员签名</p>
                                      </label>
                                      <div id="customerSignature" class="invoice__signs--area">
                                        <button type="button" id="officerSignaturePreview" class="btn btn-default" data-toggle="modal" data-target="#modalOfficerSign">Click To Sign</button>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <a id="btnSubmitInvoice" class="btn btn-success btn-block xs-mt-15">Save</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- panel -->

        </div><!-- col-md-10 -->

    </div><!-- row -->

    <!-- Modal -->
    <div class="modal fade" id="modalAddPromotion" tabindex="-1" role="dialog" aria-labelledby="addCoupon">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="modalAddServicesLabel">Add Coupon</h4>
          </div>
          <div class="modal-body">
                <div class="input-group">
                <span class="input-group-addon">Add Coupon:</span>
                 <input type="text" class="form-control" placeholder="Add Coupon..." id="CouponPrivateText">
                 <span class="input-group-btn">
                   <button class="btn btn-default" type="button" id="btnAddCouponPrivate">Add</button>
                 </span>
               </div><!-- /input-group -->
              <div class="input-group">
                <span class="input-group-addon">Public Availabe Coupon:</span>
                <select class="cpselect" id="couponSelect">
                  @foreach($coupons as $coupon)
                  <option value="{{$coupon->code}}">{{$coupon->code}}</option>
                  @endforeach
                </select>
                <span class="input-group-btn">
                  <button class="btn btn-default" type="button" id="btnAddCouponPublic">Add</button>
                </span>
              </div>
                
            <h4>Applied Coupon</h4>
              <ul class="list-group" id="appliedCoupons">
              </ul>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div>
      </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="modalAddServices" tabindex="-1" role="dialog" aria-labelledby="addServices">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="modalAddServicesLabel">Add Services</h4>
          </div>
          <div class="modal-body">
            <!-- Services -->
            <div class="row">
                <div class="col-xs-6">
                    <label>Selected Service</label>
                    <div id="servicesSelected"></div>
                </div>
                <div class="col-xs-6">
                    <label>All Service</label>
                    <div id="servicesAll"></div>
                </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div>
      </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="modalAddPackage" tabindex="-1" role="dialog" aria-labelledby="addPackage">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="modalAddPackageLabel">Add Package</h4>
          </div>
          <div class="modal-body">
            <!-- Services -->
            <div class="row">
                <div class="col-xs-6">
                    <label>Selected Package</label>
                    <div id="packagesSelected"></div>
                </div>
                <div class="col-xs-6">
                    <label>All Packages</label>
                    <div id="packagesAll"></div>
                </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div>
      </div>
    </div>

    <!-- signModal -->
    <div class="modal fade" id="modalCustomerSign" tabindex="-1" role="dialog" aria-labelledby="addServices">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Sign Below</h4>
          </div>
          <div class="modal-body" style="height:400px; width: 100%;">
            <canvas id="customerSignCanvas"></canvas>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            <button type="button" class="btn btn-default" id="clearCustomerSign">Clear</button>
            <button type="button" class="btn btn-primary" id="saveCustomerSign">Save</button>
          </div>
        </div>
      </div>
    </div>

    <!-- signModalofficer -->
    <div class="modal fade" id="modalOfficerSign" tabindex="-1" role="dialog" aria-labelledby="addServices">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Sign Below</h4>
          </div>
          <div class="modal-body" style="height:400px; width: 100%;">
            <canvas id="officerSignCanvas"></canvas>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            <button type="button" class="btn btn-default" id="clearOfficerSign">Clear</button>
            <button type="button" class="btn btn-primary" id="saveOfficerSign">Save</button>
          </div>
        </div>
      </div>
    </div>
@endsection

@section('before-scripts-end')
<script>
    var __paTax = {!! config('system.tax') !!};
</script>
@endsection

@section('after-scripts-end')
<script>
    @include('frontend.invoice.createjs')
</script>
@endsection