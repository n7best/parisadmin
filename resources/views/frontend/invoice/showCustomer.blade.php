<ul class="list-group">
    <li class="list-group-item">
        <h4>Customer Info</h4>
    </li>
    <li class="list-group-item">
        <span class="prefix">Name:</span>
        <span class="label label-default">{!! $customer->display_name !!}</span>
{{--         <span class="prefix">Bride Name:</span>
        <span class="label label-default">{!! $customer->bridename !!}</span>
        <span class="prefix">Groom Name:</span>
        <span class="label label-default">{!! $customer->groomname !!}</span> --}}
    </li>

    <li class="list-group-item form-group">
        <label class="">Groom Phone:</label>
        <input type="text" class="form-control" value="{!! $customer->getMeta('groomPhone') !!}" name="customer[groomPhone]"></input>
    </li>

    <li class="list-group-item form-group">
        <label class="">Bride Phone:</label>
        <input type="text" class="form-control" value="{!! $customer->getMeta('bridePhone') !!}" name="customer[bridePhone]"></input>
    </li>

    <li class="list-group-item form-group">
        <label class="">Email:</label>
        <input type="text" class="form-control" value="{!! $customer->getMeta('email') !!}" name="customer[email]"></input>
    </li>

    @if( $customer->getMeta('weddingDate'))
    <li class="list-group-item">
        <span class="prefix">Wedding Date:</span>
        <span class="label label-default">{!! $customer->getMeta('weddingDate') !!}</span>
    </li>
    @endif

    @if( $customer->getMeta('studioDate'))
    <li class="list-group-item">
        <span class="prefix">Studio Date:</span>
        <span class="label label-default">{!! $customer->getMeta('studioDate') !!}</span>
    </li>
    @endif

    @if( $customer->getMeta('outdoorDate'))
    <li class="list-group-item">
        <span class="prefix">Outdoor Date:</span>
        <span class="label label-default">{!! $customer->getMeta('outdoorDate') !!}</span>
    </li>
    @endif

    @if( is_numeric($customer->providers) && intval($customer->providers) >= 100000 && intval($customer->providers) < 999999 )
    <li class="list-group-item">
        <span class="prefix">Verification Code:</span>
        <span class="label label-default">{!! $customer->providers !!}</span>
    </li>
    @endif

    @if( $customer->getMeta('wc_nickname') && $customer->getMeta('wc_avatar') )
    <li class="list-group-item">
        <span class="prefix">WeChat Info:</span>
        <span class=""><img src="{{$customer->getMeta('wc_avatar')}}" width="50px" style="margin: 5px;"><span class="label label-default">{!! $customer->getMeta('wc_nickname') !!}</span></span>
    </li>
    @endif
</ul>