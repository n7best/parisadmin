<br/>
<div class="panel panel-default">
    <ul class="nav nav-tabs panel-heading" role="tablist">
      @foreach($tasks as $catalog => $task)
        <li role="presentation" @if($navcatalog && $catalog == $navcatalog) class="active" @elseif (!$nav && $task == reset($tasks)) class="active"@endif><a href="#{!! seoUrl($catalog) !!}" aria-controls="{!! seoUrl($catalog) !!}" role="tab" data-toggle="tab">{!! $catalog !!} <span class="label @if ($taskCounts[$catalog] != 0) label-primary @else label-default @endif">{!! $taskCounts[$catalog] !!}</span></a></li>
      @endforeach
    </ul>
    <script>
      var tasks = [];
    </script>
    <div class="tab-content">
    @foreach($tasks as $catalog => $task)
      <div role="tabpanel" class="tab-pane @if($navcatalog && $catalog == $navcatalog) active @elseif (!$nav && $task == reset($tasks)) active @endif" id="{!! seoUrl($catalog) !!}">
      <style>
        .list-group hr{
          margin-top: 5px;
          margin-bottom: 5px;
        }
      </style>
      <ul class="list-group">
          @foreach(collect($task)->chunk(3) as $singleTasks)
          <div class="row">
            @foreach($singleTasks as $singleTask)
            <div class="col-md-4">
              <form method="POST" action="{!! route('task.post').'?tid='.$singleTask['detail']->id !!}" style="margin: 5px;" >
              {!! csrf_field() !!}
              <input id="inputStatus{!! $singleTask['detail']->id !!}" type="hidden" name="status" value="{!! $singleTask['detail']->status !!}" />
              <li class="list-group-item" @if($singleTask['nav']) style="border:2px #7d7d7d solid;" @endif>
                  <span data-type="select" class="label label-default taskStatus{!! $singleTask['detail']->id !!}" >{!! $singleTask['detail']->status !!}</span>
                  <span class="prefix"><b>{!! $singleTask['service']->display_name !!}</b></span>
                  <span class="pull-right">
                    <button type="submit" class="btn btn-success btn-xs">Update 更新</button>
                  </span>
                  <hr/>
                  @if($singleTask['detail']->allowSchedule())
{{--                   <div class="input-group">
                    <span class="input-group-addon" id="basic-addon1">Schedule</span>
                    <input type="text" class="form-control taskScheduleStart{!! $singleTask['detail']->id !!}" name="daterange" value="Click to Set"/>
                  </div> --}}
                  <input id="inputScheduleStart{!! $singleTask['detail']->id !!}" type="hidden" name="schedule" value="{!! $singleTask['detail']->schedule_at !!}"/>
                  <input id="inputScheduleEnd{!! $singleTask['detail']->id !!}" type="hidden" name="scheduleEnd" value="{!! $singleTask['detail']->schedule_end !!}"/>
                  <div class="input-group">
                    <div class="form-group registration-date">
                      <div class="input-group registration-date-time" id="detailDateTime{!! $singleTask['detail']->id !!}">
                        <span class="input-group-addon" ><span class="glyphicon glyphicon-calendar" aria-hidden="true"></span></span>
                        <input class="form-control date start" name="registration_date" type="text">
                        <input class="form-control time start" name="registration_time" type="text">
                        <span class="input-group-addon" >To</span>
                        <input class="form-control date end" name="registration_time" type="text">
                        <input class="form-control time end" name="registration_time" type="text">
                      </div>
                    </div>
                  </div>
                  <div class="input-group">
                    <span class="input-group-addon" id="basic-addon1">All Day</span>
                    <input class="form-control" type="checkbox" name="allday" id="inputAllDay{!! $singleTask['detail']->id !!}" @if($singleTask['detail']->isAllDay()) checked @endif>
                  </div>
                  @endif
                  <div class="input-group">
                    <span class="input-group-addon" id="basic-addon1">Assigns</span>
                    <select class="user-select-ajax" name="assign_name[]" multiple="multiple">
                      @if( count($singleTask['detail']->assign_name) > 0 )
                      @foreach($singleTask['detail']->assign_name as $uid => $name)
                        <option value="{!! $uid !!}" selected="selected">{!! $name !!}</option>
                      @endforeach
                      @endif
                    </select>
                  </div>
                  @foreach($singleTask['detail']->options() as $option)
                    <div class="input-group">
                      <span class="input-group-addon">{{$option->name}}</span>
                      @if($option->type == 'text')
                      <input type="text" class="form-control taskOp{{$option->name}}{!! $singleTask['detail']->id !!}" name="options[{{$option->name}}]" value="{{$singleTask['options']['op-'.$option->name] or ''}}"/>
                      @elseif ($option->type == 'integer')
                      <input type="number" class="form-control taskOp{{$option->name}}{!! $singleTask['detail']->id !!}" name="options[{{$option->name}}]" value="{{$singleTask['options']['op-'.$option->name] or ''}}"/>
                      @elseif ($option->type == 'select')
                      <select class="form-control optselect taskOp{{$option->name}}{!! $singleTask['detail']->id !!}" name="options[{{$option->name}}]" >
                        @foreach($option->options as $seOption)
                        <option value="{{$seOption}}" @if(!empty($singleTask['options']['op-'.$option->name]) && $singleTask['options']['op-'.$option->name] == $seOption) selected @endif>{{$seOption}}</option>
                        @endforeach
                      </select>
                      @endif
                    </div>
                  @endforeach

                  @if($singleTask['detail']->status == 'submited' || $singleTask['detail']->status == 'pending')
                  <div class="input-group">
                    <span class="input-group-addon">Photos</span>
                    <span class="btn btn-success btn-xs" data-toggle="modal" data-target="#galleryModal{!! $singleTask['detail']->id !!}">Browse</span>
                  </div>

                  <!-- Modal -->
                  <div class="modal fade" id="galleryModal{!! $singleTask['detail']->id !!}" tabindex="-1" role="dialog" >
                    <div class="modal-dialog" role="document">
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                          <h4 class="modal-title">Photos</h4>
                        </div>
                        <div class="modal-body">
                          <div class="parent-container" id="containerGallery{!! $singleTask['detail']->id !!}">
                            @foreach($singleTask['detail']->galleryDesktop() as $photo)
                            <a class="@if($photo['selected']) picked @endif" href="{{ $photo['src'] }}"><img src="{{ $photo['thumbnail'] }}" class="gallery-photo-preview @if($photo['selected']) picked @endif"></a>
                            @endforeach
                          </div>
                        </div>
                        <div class="modal-footer">
                          <button type="button" id="galleryDLALL{!! $singleTask['detail']->id !!}" class="btn btn-primary">Download All</button>
                          @if($singleTask['detail']->status == 'pending')
                          <button type="button" id="galleryDLSelected{!! $singleTask['detail']->id !!}" class="btn btn-primary">Download Selected</button>
                          @endif
                          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                      </div>
                    </div>
                  </div>
                  @endif
                  <hr/>
                  <div class="form-group">
                    <label>Details:</label>
                    <textarea class="form-control" name="details" rows="2">{!! $singleTask['detail']->details !!}</textarea>
                  </div>
              </li>
              </form>
              <script>
                tasks.push({
                  tid : '{!! $singleTask['detail']->id !!}',
                  class: 'taskStatus{!! $singleTask['detail']->id !!}',
                  status:'{!! $singleTask['detail']->status !!}',
                  id: 'inputStatus{!! $singleTask['detail']->id !!}',
                  options: {!! $singleTask['detail']->avaible_options !!},
                  @if($singleTask['detail']->status == 'submited' || $singleTask['detail']->status == 'pending')
                  gallery: true,
                  galleryel: 'containerGallery{!! $singleTask['detail']->id !!}',
                  btnDlAll: 'galleryDLALL{!! $singleTask['detail']->id !!}',
                  @else
                  gallery: false,
                  @endif
                  @if($singleTask['detail']->status == 'pending')
                  btnDLSelected: 'galleryDLSelected{!! $singleTask['detail']->id !!}',
                  @else
                  btnDLSelected: false,
                  @endif
                  @if($singleTask['detail']->allowSchedule())
                  detail_id: 'detailDateTime{!! $singleTask['detail']->id !!}',
                  schedule_id: 'inputScheduleStart{!! $singleTask['detail']->id !!}',
                  schedule_class: 'taskScheduleStart{!! $singleTask['detail']->id !!}',
                  scheduleEnd_id: 'inputScheduleEnd{!! $singleTask['detail']->id !!}',
                  allday_id: 'inputAllDay{!! $singleTask['detail']->id !!}',
                  startDate: '{!! $singleTask['detail']->schedule_at ? $singleTask['detail']->schedule_at : 'false'!!}',
                  endDate: '{!! $singleTask['detail']->schedule_end ?  $singleTask['detail']->schedule_end : 'false' !!}',
                  schedule: true,
                  @else
                  schedule: false
                  @endif
                });
              </script>
            </div>
            @endforeach
          </div>
          @endforeach
      </ul>
    </div>
    @endforeach
    </div>
</div>