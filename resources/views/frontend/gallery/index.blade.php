@extends('frontend.layouts.master')

@section('content')
    <div class="row">

        <div class="col-md-10 col-md-offset-1">

            <div class="panel panel-default">
                <div class="panel-heading"><i class="fa fa-home"></i> Galleries</div>
                <table class="table table-responsive">
                    <thead>
                      <th>Id</th>
                      <th>Name</th>
                      <th>Start At</th>
                      <th width="50px">Action</th>
                    </thead>
                    <tbody>
                    @foreach($galleries as $gallery)
                        <tr>
                            <td>{!! $gallery->id !!}</td>
                            <td>{!! $gallery->name !!}</td>
                            <td>{!! $gallery->created_at !!}</td>
                            <td>
                              <a href="{!! route('gallery.edit', [$gallery->id]) !!}"><i class="glyphicon glyphicon-edit"></i></a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <div class="panel-body">
                  {{-- @permission('view-hr') --}}
                  <a href="{!! route('gallery.create') !!}" class="btn btn-success">Create New gallery</a>
                  {{-- @endauth --}}
                </div>
            </div><!-- panel -->

        </div><!-- col-md-10 -->

    </div><!-- row -->
@endsection

@section('after-scripts-end')
<script>



</script>
@endsection