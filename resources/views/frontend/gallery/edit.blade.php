@extends('frontend.layouts.master')

@section('content')
    <div class="row">

        <div class="col-md-10 col-md-offset-1">

            <div class="panel panel-default">
                <div class="panel-heading"><i class="fa fa-home"></i> Update Gallery</div>
                <div class="panel-body">
                  <div>
                  {!! Form::model($gallery, ['route' => ['gallery.update', $gallery->id], 'method' => 'patch']) !!}

                      <div class="form-group col-sm-6">
                          {!! Form::label('name', 'Name:') !!}
                          {!! Form::text('name', null, ['class' => 'form-control']) !!}
                      </div>

                      <div class="form-group col-sm-6">
                          {!! Form::label('orderid', 'Order ID:') !!}
                          {!! Form::text('orderid', $gallery->getMeta('orderid'), ['class' => 'form-control']) !!}
                      </div>


                      <div class="form-group col-sm-6">
                          {!! Form::label('customer', 'Customer:') !!}
                          {!! Form::hidden('customer', $gallery->customer_id, ['class' => 'form-control', 'id'=>'inputCustomer']) !!}
                          <style>
                          .select2-container{
                              display: block;
                              width: 80% !important;
                          }
                          </style>
                          <select class="customer-select-ajax" name="assign_name[]">
                            @if(!empty($gallery->customer_id))
                             <option value="{!! $gallery->customer_id !!}" selected="selected">{!! $gallery->customer->display_name !!}</option>
                            @endif
                          </select>
                      </div>

                      <div class="form-group col-sm-12">
                          {!! Form::submit('Update', ['class' => 'btn btn-primary']) !!}
                          <a href="{!! route('gallery.index') !!}" class="btn btn-default">Cancel</a>
                      </div>

                  {!! Form::close() !!}
                  </div>

                </div>
            </div><!-- panel -->

            <div class="panel panel-default">
                <div class="panel-heading clearfix">Photos
                  <div class="btn-group pull-right">
                      <a id="btnPrintInvoice" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#printOption">Add Photos</a>
                  </div>
                </div>
                <div class="panel-body">
                  <div id="aniimated-thumbnials">
                    loading...
                  </div>
                </div>

                <div class="panel-footer clearfix">
                  <img class="netdata-badge" src="http://zipper.pariswedding.nyc:19999/api/v1/badge.svg?chart=system.cpu&group=average&label=zip-server" />
                  <div class="btn-group pull-right" id="galleryDownload">
                      <a class="btn btn-default btn-sm">Loading...</a>

                  </div>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading clearfix">Shares
                  <div class="btn-group pull-right">
                      <a data-toggle="modal" data-target="#modalShare" class="btn btn-info btn-sm">Add Share</a>
                  </div>
                </div>
                <div class="panel-body">
                  <div id="sharesContainer">
                  loading...
                  </div>
                </div>
            </div>

        </div><!-- col-md-10 -->

    </div><!-- row -->


    <!-- Fine Uploader Thumbnails template w/ customization
       ====================================================================== -->
       <script type="text/template" id="qq-template-manual-trigger">
           <div class="qq-uploader-selector qq-uploader" qq-drop-area-text="Drop files here">
               <div class="qq-total-progress-bar-container-selector qq-total-progress-bar-container">
                   <div role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" class="qq-total-progress-bar-selector qq-progress-bar qq-total-progress-bar"></div>
               </div>
               <div class="qq-upload-drop-area-selector qq-upload-drop-area" qq-hide-dropzone>
                   <span class="qq-upload-drop-area-text-selector"></span>
               </div>
               <div class="buttons">
                   <div class="qq-upload-button-selector qq-upload-button">
                       <div>Select files</div>
                   </div>
                   <button type="button" id="trigger-upload" class="btn btn-primary">
                       <i class="icon-upload icon-white"></i> Upload
                   </button>
               </div>
               <span class="qq-drop-processing-selector qq-drop-processing">
                   <span>Processing dropped files...</span>
                   <span class="qq-drop-processing-spinner-selector qq-drop-processing-spinner"></span>
               </span>
               <ul class="qq-upload-list-selector qq-upload-list" aria-live="polite" aria-relevant="additions removals">
                   <li>
                       <div class="qq-progress-bar-container-selector">
                           <div role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" class="qq-progress-bar-selector qq-progress-bar"></div>
                       </div>
                       <span class="qq-upload-spinner-selector qq-upload-spinner"></span>
                       <img class="qq-thumbnail-selector" qq-max-size="100" qq-server-scale>
                       <span class="qq-upload-file-selector qq-upload-file"></span>
                       <span class="qq-edit-filename-icon-selector qq-edit-filename-icon" aria-label="Edit filename"></span>
                       <input class="qq-edit-filename-selector qq-edit-filename" tabindex="0" type="text">
                       <span class="qq-upload-size-selector qq-upload-size"></span>
                       <button type="button" class="qq-btn qq-upload-cancel-selector qq-upload-cancel">Cancel</button>
                       <button type="button" class="qq-btn qq-upload-retry-selector qq-upload-retry">Retry</button>
                       <button type="button" class="qq-btn qq-upload-delete-selector qq-upload-delete">Delete</button>
                       <span role="status" class="qq-upload-status-text-selector qq-upload-status-text"></span>
                   </li>
               </ul>

               <dialog class="qq-alert-dialog-selector">
                   <div class="qq-dialog-message-selector"></div>
                   <div class="qq-dialog-buttons">
                       <button type="button" class="qq-cancel-button-selector">Close</button>
                   </div>
               </dialog>

               <dialog class="qq-confirm-dialog-selector">
                   <div class="qq-dialog-message-selector"></div>
                   <div class="qq-dialog-buttons">
                       <button type="button" class="qq-cancel-button-selector">No</button>
                       <button type="button" class="qq-ok-button-selector">Yes</button>
                   </div>
               </dialog>

               <dialog class="qq-prompt-dialog-selector">
                   <div class="qq-dialog-message-selector"></div>
                   <input type="text">
                   <div class="qq-dialog-buttons">
                       <button type="button" class="qq-cancel-button-selector">Cancel</button>
                       <button type="button" class="qq-ok-button-selector">Ok</button>
                   </div>
               </dialog>
           </div>
       </script>

       <style>
           #trigger-upload {
               color: white;
               background-color: #00ABC7;
               font-size: 14px;
               padding: 7px 20px;
               background-image: none;
           }

           #fine-uploader-manual-trigger .qq-upload-button {
               margin-right: 15px;
           }

           #fine-uploader-manual-trigger .buttons {
               width: 36%;
           }

           #fine-uploader-manual-trigger .qq-uploader .qq-total-progress-bar-container {
               width: 60%;
           }
       </style>


    <!-- Modal -->
    <div class="modal fade" id="modalShare" tabindex="-1" role="dialog">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">Share
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          </div>
          <div class="modal-body">
           <div class="form-group">
               <label for="exampleInputEmail1">Store</label>
               @foreach($companies as $key => $company)
               <label class="radio-inline">
                 <input type="radio" name="companyOptions" class="companyFilter" value="{!! $company->id !!}" @if($key == 0 ) checked="checked" @endif> {!! $company->code !!}
               </label>
               @endforeach
           </div>
           <div class="form-group">
               <label for="exampleInputEmail1">Options</label>
               <label class="checkbox-inline">
                 <input type="checkbox" id="cbWatermark" value="1" checked> Watermark
               </label>
               <label class="checkbox-inline">
                 <input type="checkbox" id="cbText" value="1" checked> Show Makeup & Photographer
               </label>
           </div>

           <div class="form-group">
               <label for="exampleInputEmail1">Max Selects</label>
               <input type="number" id="ps-ms" class="form-control" placeholder="50" value="50">
             </div>
           <hr/>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-info" id="genPs">Generate PhotoSelect</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div>
      </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="printOption" tabindex="-1" role="dialog">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="modalUploadLabel">Add Photos</h4>
          </div>
          <div class="modal-body">
            <div class="row">
              <style>
              .pac-container {
                z-index: 9999 !important;
              }
              </style>
              <div class="form-group col-sm-12">
                  {!! Form::label('location', 'Location:') !!}
                  {!! Form::text('location', null, ['class' => 'form-control', 'id'=>"location"]) !!}
              </div>
              <div class="form-group col-sm-6">
                  {!! Form::label('photographer', 'Photographer:') !!}
                  <select class="photographer-select-ajax" name="photographer" id="photographer">
                  </select>
              </div>
              <div class="form-group col-sm-6">
                  {!! Form::label('makeup', 'Makeup Artist:') !!}
                  <select class="photographer-select-ajax" name="makeup" id="makeup">
                  </select>
              </div>
            </div>

            <div id="fine-uploader-manual-trigger"></div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div>
      </div>
    </div>
@endsection

@section('after-scripts-end')
<script>
var gallery_info = {
  type: '{!! addslashes(get_class($gallery)) !!}',
  type_id: {!! $gallery->id !!},
  zips: {!! json_encode($gallery->ziplist()) !!}
}
console.log('gallery', gallery_info)

$(document).ready(function(){

  function requestDownload(folder, files, $btn, $type, $typeid, useSelf){
    return function(event){
      var data = {
        type : $type ? $type : gallery_info.type,
        type_id: $typeid ? $typeid: gallery_info.type_id,
        folder: folder,
        files: files
      }
      $el = useSelf ? $btn : $btn.parent();
      $el.html('loading');

      $.post('{!! route('zips.store') !!}', data, function(data){
        console.log('generate data', data);
        updateDownoad(data.hash, $el)
      }).fail(function(err){
        console.log('generate error', err);
        $el.html($btn);
      })
    }
  }

  function updateDownoad(hash, $el, timer){
    $.get('{!! route('zips.show', '') !!}/' + hash, function(data){
      var zip = $.parseJSON(data);
      console.log(zip)

      if(zip.status == 'complete'){
        $btn = $('<a href="' + zip.url +'" target="_blank" class="btn btn-default btn-sm">Download</a>')
        $el.html($btn);
      }else if(zip.status == 'error'){
        $btngdl = $('Error! <a class="btn btn-default btn-sm">Request Download Again</a>');
        $btngdl.click(requestDownload(gallery_info.type_id, null, $btngdl));

        $el.html($btngdl);
      }else{
        $el.html('Download ' + zip.status);

        setTimeout(()=>updateDownoad(hash, $el), 10000);
      }
    }).fail(function(err){
      console.log(err)
      $el.html('refresh error');
    })
  }

  if(gallery_info.zips.length == 0){
    $btngdl = $('<a class="btn btn-default btn-sm">Request Download</a>');
    $btngdl.click(requestDownload(gallery_info.type_id, null, $btngdl));
  }else{
    $btngdl = $('<p>pending</p>');
    var hash = gallery_info.zips[0].hash
    updateDownoad(hash, $btngdl);
  }

  $('#galleryDownload').html($btngdl)
  //request photoselct
  function fetchPs(){
    var $el = $('#sharesContainer');
    $el.html('loading...');
    $.get('{!! route('galleryapips', $gallery->id) !!}', function(data){
      $el.html('');
      $.each($.parseJSON(data),function(i, ps){
        var $list = $('<div class="panel panel-default"></div>');
        var $title = $('<div class="panel-heading"></div>');
        $title.append('ID:' + ps.id)
        $title.append(' Status: '+ ps.status)
        $list.append($title)

        var $body = $('<div class="panel-body"></div>')
        var $inputgroup = $('<div class="input-group"></div>')
        $inputgroup.append('<span class="input-group-addon">Link</span>')
        $inputgroup.append('<input type="text" class="form-control" id="copy'+ps.id+'" value="{!! url('photoselect') !!}/'+ ps.link + '" >')
        $inputgroup.append('<span class="input-group-btn""><button class="btn btncopy" data-clipboard-target="#copy'+ps.id+'">Copy</button></span>')
        $body.append($inputgroup)
        $list.append($body)

        if(ps.selected.length > 0){
          $body.append('<hr/>')
          var $psiContainer = $('<div></div>')
          $.each(ps.selected, function(i, psi){
            $psiContainer.append('<a href="'+psi.src+'"><img src="'+ psi.thumb +'"/></a>')
          })
          $psiContainer.lightGallery({
              thumbnail: true,
              download: false
          });
          $body.append($psiContainer)
          //downlaod
          $body.append('<hr/>')
          $body.append('<pre>'+ps.remark+'</pre>')

          //download section
          if(ps.zips.length > 0){

            $psbtngdl = $('<p>pending</p>');
            var pshash = ps.zips[0].hash
            updateDownoad(pshash, $psbtngdl);

            $body.append($psbtngdl)
          }else{
            if(ps.status == 'selected'){
              $psbtngdl = $('<a class="btn btn-default btn-sm">Request Download</a>');
              var folder = gallery_info.type_id
              var files = ps.selected.map((obj) => obj.key)
              console.log(folder, files)
              $psbtngdl.click(requestDownload(folder, files, $psbtngdl, 'App\\Models\\Media\\PhotoSelect', ps.id, true));
              $body.append($psbtngdl)
            }
          }
        }

        $el.append($list)
      })
      var clipboard = new Clipboard('.btncopy')
      console.log('photo selects', $.parseJSON(data));
    }).fail(function(err){
      $el.html('Load Photoselect fail!')
    })
  }

  fetchPs();
  //end request photoseclt

  //gen photoselect

  $('#genPs').click(function(){
    var $btn = $(this);
    $btn.button('loading');

    var data = {
      gid: '{!!$gallery->id!!}',
      store: $( "input:radio[name=companyOptions]:checked" ).val(),
      ms: $('#ps-ms').val()
    }

    if($('#cbWatermark').prop('checked')) data['wm'] = 'true';
    if($('#cbText').prop('checked')) data['text'] = 'true';

    console.log(data);

    $.post('{!! route('psstore') !!}', data, function(data){
      console.log(data);
      fetchPs();
    }).fail(function(err){
      console.log(err);
      alert(err.responseJSON.msg);
    }).always(function(){
      $btn.button('reset');
    })

  })
  //end gen photoselect

  var NETDATA_BADGES_AUTOREFRESH_SECONDS = 10;
  function refreshNetdataBadges() {
    var now = new Date().getTime().toString();
    $('.netdata-badge').each(function() {
      this.src = this.src.replace(/\&_=\d*/, '') + '&_=' + now;
    });
    setTimeout(refreshNetdataBadges, NETDATA_BADGES_AUTOREFRESH_SECONDS * 1000);
  }
  setTimeout(refreshNetdataBadges, NETDATA_BADGES_AUTOREFRESH_SECONDS * 1000);

  function fetchPhotos(cb){
    $('#aniimated-thumbnials').html('loading...');
    $.get('{!! route('galleryapishow', $gallery->id) !!}', function(data){
      var photos = $.parseJSON(data);
      console.log(photos);
      $('#aniimated-thumbnials').html('');
      if(photos.length == 0){
        $('#aniimated-thumbnials').html('<h4>No photos</h4>');
      }else{
        $.each(photos,function(i,photo){
          $('#aniimated-thumbnials').append('<a href="'+photo.src+'"><img src="'+ photo.thumb +'"/></a>')
        })
        cb();
      }
    })
  }

  fetchPhotos(function(){
    $('#aniimated-thumbnials').lightGallery({
        thumbnail: true,
        download: false
    });
  });

  var manualUploader = new qq.s3.FineUploader({
        element: document.getElementById('fine-uploader-manual-trigger'),
        template: 'qq-template-manual-trigger',
        thumbnails: {
            placeholders: {
                waitingPath: '/img/placeholders/waiting-generic.png',
                notAvailablePath: '/img/placeholders/not_available-generic.png'
            }
        },
        autoUpload: false,
        debug: true,
        request: {
            endpoint: '{!! config('services.amazon.expectedBucketName') !!}' + '.s3.amazonaws.com',
            accessKey: '{!! config('services.amazon.clientPublicKey') !!}'
        },
        signature: {
            endpoint: '{!!route('s3sign')!!}',
            version: 4
        },
        chunking: {
            concurrent: {
                enabled: true
            },
            enabled: true,
            partSize: 20971520
        },
        uploadSuccess: {
            endpoint: '{!!route('s3sign')!!}?success'
        },
        validation:{
            sizeLimit:{!! config('services.amazon.expectedMaxSize') !!}
        },
        objectProperties:{
          acl:'public-read',
          key: function(fileid){
            return 'photos/{!! $gallery->id !!}/' + qq.getUniqueId() + '.' + qq.getExtension(manualUploader.getName(fileid));
          }
        },
        callbacks: {
            onComplete: function(id,name,obj,xhr) {
                fetchPhotos(function(){
                  $('#aniimated-thumbnials').data('lightGallery').destroy(true);
                  $('#aniimated-thumbnials').lightGallery({
                      thumbnail: true,
                      download: false
                  });
                });
            },
            onProgress: function(id, name, current, total){
                //cbs.updateProgress(current / total);
            },
            onSubmit: function(id,name){
              manualUploader.setUploadSuccessParams(
                {
                  tid:'photos',
                  jwtToken:'{!! $jwtToken !!}',
                  location: $('#location').val(),
                  photographer: $('#photographer').val(),
                  makeup: $('#makeup').val(),
                  gid: {!! $gallery->id !!}
                }
                ,id);
            }
        }
    });

    qq(document.getElementById("trigger-upload")).attach("click", function() {
        manualUploader.uploadStoredFiles();
    });

  $("#location").geocomplete({
    types: ['geocode', 'establishment']
  });

  $(".photographer-select-ajax").select2({
    theme: "bootstrap",
    dropdownParent: $("#printOption"),
    ajax: {
      url: "{!! route('api.selects') !!}?d=user",
      dataType: 'json',
      delay: 250,
      cache: true,
      processResults: function (data) {
        return {
          results: data
        };
      }
    },
    minimumInputLength: 1,
  });
  $(".customer-select-ajax").select2({
    theme: "bootstrap",
    ajax: {
      url: "{!! route('api.selects') !!}?d=customers",
      dataType: 'json',
      delay: 250,
      cache: true,
      processResults: function (data) {
        return {
          results: data
        };
      }
    },
    minimumInputLength: 1,
    templateResult: function(data) {
      if(data.metas && data.metas.wc_avatar) {
        var $state = $(
          '<span><img src="' + data.metas.wc_avatar + '" width="25px" /> ' + data.text + '</span>'
        );
        return $state;
      }else{
        return data.text
      }
    }
  });
  $(".customer-select-ajax").on("select2:select", function (evt) {
    if (!evt) {
      var args = {data:false};
    } else {
      var args = evt.params
      //console.log(args.data);
      $('#inputCustomer').val(args.data.id);
    }
  });
})


</script>
@endsection

@section('before-scripts-end')
<script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyCOQXTBmDr3HTVNRkPXnAAOOzWzEgGCZsc&libraries=places"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/geocomplete/1.7.0/jquery.geocomplete.js"></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/lightgallery/1.2.21/js/lightgallery.js'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/lightgallery/1.2.21/js/lg-zoom.js'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/lightgallery/1.2.21/js/lg-thumbnail.js'></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/clipboard.js/1.5.12/clipboard.min.js"></script>

@endsection

@section('after-styles-end')
<link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/lightgallery/1.2.21/css/lightgallery.css'>
@endsection