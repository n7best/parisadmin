@extends('frontend.layouts.master')

@section('content')
    <div class="row">

        <div class="col-md-10 col-md-offset-1">

            <div class="panel panel-default">
                <div class="panel-heading"><i class="fa fa-home"></i> Create Gallery</div>
                <div class="panel-body">
                  {!! Form::open(['route' => 'gallery.store']) !!}

                      <div class="form-group col-sm-6">
                          {!! Form::label('name', 'Name:') !!}
                          {!! Form::text('name', null, ['class' => 'form-control']) !!}
                      </div>


                      <div class="form-group col-sm-6">
                          {!! Form::label('customer', 'Customer:') !!}
                          {!! Form::hidden('customer', null, ['class' => 'form-control', 'id'=>'inputCustomer']) !!}
                          <style>
                          .select2-container{
                              display: block;
                              width: 80% !important;
                          }
                          </style>
                          <select class="customer-select-ajax" name="assign_name[]">
                          </select>
                      </div>

                      <div class="form-group col-sm-12">
                          {!! Form::submit('Create', ['class' => 'btn btn-primary']) !!}
                          <a href="{!! route('gallery.index') !!}" class="btn btn-default">Cancel</a>
                      </div>

                  {!! Form::close() !!}
                </div>
            </div><!-- panel -->

        </div><!-- col-md-10 -->

    </div><!-- row -->
@endsection

@section('after-scripts-end')
<script>

$(document).ready(function(){
  $(".customer-select-ajax").select2({
    theme: "bootstrap",
    ajax: {
      url: "{!! route('api.selects') !!}?d=customers",
      dataType: 'json',
      delay: 250,
      cache: true,
      processResults: function (data) {
        return {
          results: data
        };
      }
    },
    minimumInputLength: 1,
    templateResult: function(data) {
      if(data.metas && data.metas.wc_avatar) {
        var $state = $(
          '<span><img src="' + data.metas.wc_avatar + '" width="25px" /> ' + data.text + '</span>'
        );
        return $state;
      }else{
        return data.text
      }
    }
  });
  $(".customer-select-ajax").on("select2:select", function (evt) { 
    if (!evt) {
      var args = {data:false};
    } else {
      var args = evt.params
      //console.log(args.data);
      $('#inputCustomer').val(args.data.id);
    }
  });
})


</script>
@endsection