@extends('frontend.layouts.master')

@section('content')
<style type="text/css">
  .imagebg {
    width: 100%;
    height: 100%;
    background-color: #222426;
    position: fixed;
    left: 15px;
    top: 52px;
    background-size: cover;
    transition: background-image 2s linear;
  }
  .panel-transfer {
    box-shadow: 0px 0px 20px 5px rgba(0, 0, 0, 0.75);
    border-radius: 10px;
    border: 3px solid #e8e8e8;
    min-width: 400px;
    max-width: 400px;
    margin-top: 10%;
  }
  .select2-container{
      display: block;
      width: 100% !important;
  }
</style>

  <div class="row imagebg">

    <div class="col-md-4 col-md-offset-1">
      <div class="panel panel-default panel-transfer">
        <!-- Default panel contents -->
        <div class="panel-body">
          <div id="fine-uploader-manual-trigger"></div>
        </div>

        <ul class="list-group">
            <li class="list-group-item">
              <div class="form-group" style="margin-bottom: 0">
                  <label for="iptPhotographer">Photographer</label>
                  <select class="photographer-select-ajax" name="photographer" id="photographer">
                  </select>
                </div>
            </li>
            <li class="list-group-item">
              <div class="form-group" style="margin-bottom: 0">
                  <label for="iptPhotographer">Makeup Artist</label>
                  <select class="photographer-select-ajax" name="makeup" id="makeup">
                  </select>
                </div>
            </li>
            <li class="list-group-item">
              <div class="form-group" style="margin-bottom: 0">
                  <label for="location">Location</label>
                  <input type="text" class="form-control" id="location" placeholder="location...">
                </div>
            </li>
            <li class="list-group-item">
              <button type="button" id="trigger-upload" class="btn btn-primary">
                  <i class="icon-upload icon-white"></i> Upload
              </button>
            </li>
          </ul>
      </div>
    </div>

  </div>
  <!-- Fine Uploader Thumbnails template w/ customization
     ====================================================================== -->
     <script type="text/template" id="qq-template-manual-trigger">
         <div class="qq-uploader-selector qq-uploader" qq-drop-area-text="Drop files here">
             <div class="qq-total-progress-bar-container-selector qq-total-progress-bar-container">
                 <div role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" class="qq-total-progress-bar-selector qq-progress-bar qq-total-progress-bar"></div>
             </div>
             <div class="qq-upload-drop-area-selector qq-upload-drop-area" qq-hide-dropzone>
                 <span class="qq-upload-drop-area-text-selector"></span>
             </div>
             <div class="buttons">
                 <div class="qq-upload-button-selector qq-upload-button btn btn-primary">
                     <div>Add Files</div>
                 </div>
             </div>
             <span class="qq-drop-processing-selector qq-drop-processing">
                 <span>Processing dropped files...</span>
                 <span class="qq-drop-processing-spinner-selector qq-drop-processing-spinner"></span>
             </span>
             <ul class="qq-upload-list-selector qq-upload-list" aria-live="polite" aria-relevant="additions removals">
                 <li>
                     <div class="qq-progress-bar-container-selector">
                         <div role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" class="qq-progress-bar-selector qq-progress-bar"></div>
                     </div>
                     <span class="qq-upload-spinner-selector qq-upload-spinner"></span>
                     {{-- <img class="qq-thumbnail-selector" qq-max-size="100" qq-server-scale> --}}
                     <span class="qq-upload-file-selector qq-upload-file"></span>
                     <span class="qq-edit-filename-icon-selector qq-edit-filename-icon" aria-label="Edit filename"></span>
                     <input class="qq-edit-filename-selector qq-edit-filename" tabindex="0" type="text">
                     <span class="qq-upload-size-selector qq-upload-size"></span>
                     <button type="button" class="qq-btn qq-upload-cancel-selector qq-upload-cancel">Cancel</button>
                     <button type="button" class="qq-btn qq-upload-retry-selector qq-upload-retry">Retry</button>
                     <button type="button" class="qq-btn qq-upload-delete-selector qq-upload-delete">Delete</button>
                     <span role="status" class="qq-upload-status-text-selector qq-upload-status-text"></span>
                 </li>
             </ul>

             <dialog class="qq-alert-dialog-selector">
                 <div class="qq-dialog-message-selector"></div>
                 <div class="qq-dialog-buttons">
                     <button type="button" class="qq-cancel-button-selector">Close</button>
                 </div>
             </dialog>

             <dialog class="qq-confirm-dialog-selector">
                 <div class="qq-dialog-message-selector"></div>
                 <div class="qq-dialog-buttons">
                     <button type="button" class="qq-cancel-button-selector">No</button>
                     <button type="button" class="qq-ok-button-selector">Yes</button>
                 </div>
             </dialog>

             <dialog class="qq-prompt-dialog-selector">
                 <div class="qq-dialog-message-selector"></div>
                 <input type="text">
                 <div class="qq-dialog-buttons">
                     <button type="button" class="qq-cancel-button-selector">Cancel</button>
                     <button type="button" class="qq-ok-button-selector">Ok</button>
                 </div>
             </dialog>
         </div>
     </script>

     <style>
         .qq-upload-list{
          max-height: 100px;
         }
         .qq-uploader {
          min-height: 0;
          padding: 10px;
         }
     </style>
@endsection

@section('after-scripts-end')
<script>

$(document).ready(function(){
  window.gallery_id = null;
  //$('.imagebg').css({"background-image": "url('https://source.unsplash.com/random/1920x1080')"});
  $('.imagebg').fadeTo('slow', 0.1, function()
  {
      $(this).css('background-image', 'url("https://source.unsplash.com/random/1920x1080")');
  }).delay(100).fadeTo('slow', 1);

  $(".photographer-select-ajax").select2({
    theme: "bootstrap",
    ajax: {
      url: "{!! route('api.selects') !!}?d=user",
      dataType: 'json',
      delay: 250,
      cache: true,
      processResults: function (data) {
        return {
          results: data
        };
      }
    },
    minimumInputLength: 1,
  });

  $("#location").geocomplete({
    types: ['geocode', 'establishment']
  });

  var manualUploader = new qq.s3.FineUploader({
        element: document.getElementById('fine-uploader-manual-trigger'),
        template: 'qq-template-manual-trigger',
        thumbnails: {
            placeholders: {
                waitingPath: '/img/placeholders/waiting-generic.png',
                notAvailablePath: '/img/placeholders/not_available-generic.png'
            }
        },
        autoUpload: false,
        debug: true,
        request: {
            endpoint: '{!! config('services.amazon.expectedBucketName') !!}' + '.s3.amazonaws.com',
            accessKey: '{!! config('services.amazon.clientPublicKey') !!}'
        },
        signature: {
            endpoint: '{!!route('s3sign')!!}',
            version: 4
        },
        chunking: {
            concurrent: {
                enabled: true
            },
            enabled: true,
            partSize: 20971520
        },
        uploadSuccess: {
            endpoint: '{!!route('s3sign')!!}?success'
        },
        validation:{
            sizeLimit:{!! config('services.amazon.expectedMaxSize') !!}
        },
        objectProperties:{
          acl:'public-read',
          key: function(fileid){
            return 'photos/'+window.gallery_id+'/' + qq.getUniqueId() + '.' + qq.getExtension(manualUploader.getName(fileid));
          }
        },
        callbacks: {
            onComplete: function(id,name,obj,xhr) {

            },
            onProgress: function(id, name, current, total){
                //cbs.updateProgress(current / total);
            },
            onAllComplete: function(){
              swal({
                title: "Upload Complete",
                text: "Do You like go to the uploaded page?",
                type: "success",
                showCancelButton: true,
                confirmButtonText: "Yes, Take me there!",
                closeOnConfirm: false
              }, function(){
                location.href = "{!! url('gallery')!!}/" + window.gallery_id +'/edit';
              });

              $('#trigger-upload').button('reset');
              $('#photographer').prop('disabled', false);
              $('#location').prop('disabled', false);
              $('#makeup').prop('disabled', false);
              manualUploader.reset()
            },
            onUpload: function(id,name){
              manualUploader.setUploadSuccessParams(
                {
                  tid:'photos',
                  jwtToken:'{!! $jwtToken !!}',
                  location: $('#location').val(),
                  photographer: $('#photographer').val(),
                  makeup: $('#makeup').val(),
                  gid: window.gallery_id
                }
                ,id);
            }
        }
    });

    $('#trigger-upload').click(function(){

      if(!$('#location').val()) return alert('Enter a location')
      if(!$('#photographer').val()) return alert('Enter a photographer')
      if(!$('#makeup').val()) return alert('Enter a makeup')
      if(manualUploader.getUploads().length == 0) return alert('Upload a file')

      $btn = $(this);
      $btn.button('loading')

      $('#photographer').prop('disabled', true);
      $('#location').prop('disabled', true);
      $('#makeup').prop('disabled', true);

      $.post('{!! route('gallery.store') !!}', {}, function(data){
        console.log('new galleryid', data);
        window.gallery_id = data.id;

        manualUploader.uploadStoredFiles();
      }).fail(function(err){
        console.log(err);
        $btn.button('reset');
        $('#photographer').prop('disabled', false);
        $('#location').prop('disabled', false);
        $('#makeup').prop('disabled', false);
        alert(err.responseJSON.msg);
      })

    })
})


</script>
@endsection

@section('before-scripts-end')
<script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyCOQXTBmDr3HTVNRkPXnAAOOzWzEgGCZsc&libraries=places"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/geocomplete/1.7.0/jquery.geocomplete.js"></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/lightgallery/1.2.21/js/lightgallery.js'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/lightgallery/1.2.21/js/lg-zoom.js'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/lightgallery/1.2.21/js/lg-thumbnail.js'></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/clipboard.js/1.5.12/clipboard.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>

@endsection

@section('after-styles-end')
<link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/lightgallery/1.2.21/css/lightgallery.css'>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css" />
@endsection