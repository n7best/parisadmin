@extends('frontend.layouts.master')

@section('content')
<div class="container">
    @include('frontend.customers.show_fields')
</div>
@endsection
