<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $customers->id !!}</p>
</div>

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Name:') !!}
    <p>{!! $customers->name !!}</p>
</div>

<!-- Bridename Field -->
<div class="form-group">
    {!! Form::label('bridename', 'Bridename:') !!}
    <p>{!! $customers->bridename !!}</p>
</div>

<!-- Groomname Field -->
<div class="form-group">
    {!! Form::label('groomname', 'Groomname:') !!}
    <p>{!! $customers->groomname !!}</p>
</div>

<!-- Providers Field -->
<div class="form-group">
    {!! Form::label('providers', 'Providers:') !!}
    <p>{!! $customers->providers !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $customers->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $customers->updated_at !!}</p>
</div>

