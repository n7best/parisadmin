<table class="table table-responsive">
    <thead>
    <th>Id</th>
			<th>Name</th>
			<th>Bridename</th>
			<th>Groomname</th>
			<th>Providers</th>
			<th>Created At</th>
			<th>Updated At</th>
    <th width="50px">Action</th>
    </thead>
    <tbody>
    @foreach($customers as $customers)
        <tr>
            <td>{!! $customers->id !!}</td>
			<td>{!! $customers->name !!}</td>
			<td>{!! $customers->bridename !!}</td>
			<td>{!! $customers->groomname !!}</td>
			<td>{!! $customers->providers !!}</td>
			<td>{!! $customers->created_at !!}</td>
			<td>{!! $customers->updated_at !!}</td>
            <td>
                <a href="{!! route('customers.edit', [$customers->id]) !!}"><i class="glyphicon glyphicon-edit"></i></a>
                <a href="{!! route('customers.delete', [$customers->id]) !!}" onclick="return confirm('Are you sure wants to delete this Customers?')">
                    <i class="glyphicon glyphicon-trash"></i>
                </a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>