<!--- Id Field --->

<!--- Name Field --->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Name:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!--- Bridename Field --->
<div class="form-group col-sm-6">
    {!! Form::label('bridename', 'Bridename:') !!}
    {!! Form::text('bridename', null, ['class' => 'form-control']) !!}
</div>

<!--- Groomname Field --->
<div class="form-group col-sm-6">
    {!! Form::label('groomname', 'Groomname:') !!}
    {!! Form::text('groomname', null, ['class' => 'form-control']) !!}
</div>

<!--- Providers Field --->
<div class="form-group col-sm-6">
    {!! Form::label('providers', 'Providers:') !!}
    {!! Form::text('providers', null, ['class' => 'form-control']) !!}
</div>


<!--- Submit Field --->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('customers.index') !!}" class="btn btn-default">Cancel</a>
</div>
