@extends('frontend.layouts.master')

@section('content')
<div class="container">

    <div class="row">
        <div class="col-sm-12">
            <h1 class="pull-left">Create New Customers</h1>
        </div>
    </div>

    @include('core-templates::common.errors')

    <div class="row">
        {!! Form::open(['route' => 'customers.store']) !!}

            @include('frontend.customers.fields')

        {!! Form::close() !!}
    </div>
</div>
@endsection