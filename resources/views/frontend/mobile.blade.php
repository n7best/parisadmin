@extends('frontend.layouts.mobile')

@section('content')
<div class="weui_tab" style="width: 100%">
  <div class="weui_tab_bd" style="width: 100%">
    <div id="invoice" class="weui_tab_bd_item weui_tab_bd_item_active" >
      @include('frontend.mobile.invoice')
    </div>
    <div id="settings" class="weui_tab_bd_item">
      @include('frontend.mobile.settings')
    </div>
  </div>
  <div class="weui_tabbar">
    <a href="#invoice" class="weui_tabbar_item weui_bar_item_on">
      <div class="weui_tabbar_icon">
        <span class="lnr lnr-calendar-full"></span>
      </div>
      <p class="weui_tabbar_label">订单状态</p>
    </a>
    <a href="#settings" class="weui_tabbar_item">
      <div class="weui_tabbar_icon">
        <span class="lnr lnr-user"></span>
      </div>
      <p class="weui_tabbar_label">用户设置</p>
    </a>
  </div>
</div>
@endsection

@section('after-scripts-end')
    <script>
        //Being injected from FrontendController
        $(document).ready(function(){
          //$('#invoice').pullToRefresh();
          $('#setNotification').prop('checked', {{$user->getMeta('notification', true)}})
          $('#btnUpdateUser').click(function(){
            $.showLoading('验证中');

            $.post('{{route('frontend.mobile_user_update')}}', {
              uid: '{{$uid}}',
              phone: $('#setPhone').val(),
              email: $('#setEmail').val(),
              notification: $('#setNotification').prop('checked'),
            }, function(data){
              console.log('success', data)
              $('#setPhone').val(data.phone)
              $('#setEmail').val(data.email)
              $('#setNotification').prop('checked', data.notification)
              $.alert('更新成功')
            }).fail(function(data){
              console.log('fail', data)
              $.alert(data.responseJSON.msg);
            }).always(function(){
              $.hideLoading();
            })
          })
        })

        $(document).ready(function(){
          $('.open-invoice').click(function(){
            var _tid = $(this).data('tid')
            imagePicker(galleries[_tid], _tid)

            $('.picker-container').one('selected', function(e, data){
              console.log(data)
            })
          })
        })

        function imagePicker(images, tid, options, cb){
          var selected = {}
          var config = $.extend({}, {
            limit: 3,
            title: '选择照片',
            modal: '#imagePicker',
            container: '.picker-container',
            btnSelect: '.picker-btn-select',
            elTitle: '.picker-title',
            elClose: '.picker-close',
            tplBox: '<div class="weui-col-25 picker-box" ><span class="picker-box-checker" data-id="@{{id}}"><i class="weui_icon_success_circle picker-box-checker__unchecked"></i></span><a href="@{{url}}" title="Photo ID:@{{id}}"><img class="picker-img" src="@{{thumbnail}}" width="100%"></a></div>',
            tplConfirm: '<div class="picker-confirm">@{{#photos}}<img class="picker-confirm-image" src="@{{url}}">@{{/photos}}</div>'
          }, options);
          //init
          $(config.elTitle).html(config.title)
          selected = []
          updateCounter()
          $(config.container).html('')

          var boxTemplate = $.t7.compile(config.tplBox);
          $.each(images, function(i, img){
            //console.log(img)
            var el = boxTemplate(img)
            selected[img.id] = false
            $(config.container).append(el)
          })

          $(config.container).magnificPopup({
            delegate: 'a', // child items selector, by clicking on it popup will open
            type: 'image',
            gallery: {enabled: true},
            zoom: {enabled: true}
            // other options
          });

          //toogle

          $('.picker-box-checker').click(function(){
            var _id = $(this).data('id')
            if(selected[_id]){
              selected[_id] = false
              $(this).children().removeClass().addClass('weui_icon_success_circle picker-box-checker__unchecked')
            }else{
              //console.log(counter(), config.limit)
              if(counter() >= config.limit && config.limit != 0){
                alert('最多' + config.limit + '照片可选')
              }else{
                selected[_id] = true
                $(this).children().removeClass().addClass('weui_icon_success')
              }
            }
            updateCounter()
          })

          $(config.elClose).click(function(){
            close()
          })

          $(config.btnSelect).click(function(){
            if(!$(config.btnSelect).hasClass('weui_btn_disabled')){
              var confirmTemplate = $.t7.compile(config.tplConfirm);
              var photos = {photos: getSelected()}
              //console.log(photos)
              $.confirm(confirmTemplate(photos),'确认选择', function() {
                //console.log('s:', selected)
                //$(config.container).trigger('selected', [selected])

                $.post('{{route('frontend.mobile_user_photoselected')}}', {
                  uid: '{{$uid}}',
                  tid: tid,
                  selected: selectToPost(),
                }, function(data){
                  console.log('success', data)
                  $.alert('更新成功',function(){
                    close()
                    location.reload()
                  })

                }).fail(function(data){
                  console.log('fail', data)
                  $.alert(data.responseJSON.msg);
                }).always(function(){
                  $.hideLoading();
                })
              });
            }
          })

          //update counter
          function updateCounter(){
            var _length = counter()
            //console.log('l:', _length)
            if(_length > 0){
              $(config.btnSelect).removeClass('weui_btn_disabled')
              $(config.btnSelect).html('('+ _length + ') 选择')
            }else{
              $(config.btnSelect).addClass('weui_btn_disabled')
              $(config.btnSelect).html('选择')
            }
          }

          function selectToPost(){
            var output = []
            $.each(Object.keys(selected), function(i, key){
              if(selected[key]) output.push(key)
            })
            return output
          }

          function getSelected(){
            var output = []
            $.each(images, function(i, img){
              if(selected[img.id]) output.push({url: img.thumbnail})
            })
            return output
          }

          function counter() {
            var count = 0
            $.each(Object.keys(selected), function(i, key){
              if(selected[key]) count++
            })
            return count
          }

          function close(){
            $.closePopup()
          }

          $(config.modal).popup()
        }
    </script>
@stop