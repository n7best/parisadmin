@extends('frontend.layouts.master')
@section('content')
    <div class="row">
        <div class="col-md-12">

            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-home"></i> Announcements
                </div>

                <div class="panel-body">
                    {{-- {!! App\Models\Template::render('announcements') !!} --}}
                    <style>
                        .past-event {
                          -webkit-filter: brightness(125%); 
                          filter: brightness(125%);
                          opacity: 0.85;
                        }
                    </style>
                    {!! $calendar->calendar() !!}
                </div>
            </div><!-- panel -->

        </div><!-- col-md-10 -->
    </div><!--row-->
    <div id="fullCalModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span> <span class="sr-only">close</span></button>
                    <h4 id="modalTitle" class="modal-title"><input type="text" class="form-control" id="eventTitle" placeholder="Event Title..." style="margin-bottom: 5px;"></h4>
                    <form class="form-inline" id="detailDateTime">
                        <div class="form-group">
                            <label class="sr-only">Date Start</label>
                            <input type="text" class="form-control date start" style="width: 125px;"/>
                        </div>
                        <div class="form-group">
                            <label class="sr-only">Date Start</label>
                            <input type="text" class="form-control time start" style="width: 125px;"/>
                        </div>
                        <span class="label label-default">To</span>
                        <div class="form-group">
                            <label class="sr-only">Date Start</label>
                            <input type="text" class="form-control time end" style="width: 125px;"/>
                        </div>
                        <div class="form-group">
                            <label class="sr-only">Date Start</label>
                            <input type="text" class="form-control date end" style="width: 125px;"/>
                        </div>
                    </form>
                    <div class="checkbox">
                      <label>
                        <input type="checkbox" id="eventAllday" value="">
                        All Day
                      </label>
                    </div>
                </div>
                <div id="modalBody" class="modal-body">
                    <h4 id="detailSpinner"><span class="label label-info" style="padding: 1em;"><i class="fa fa-spinner fa-spin" style="font-size:24px;margin-right: 10px;"></i>Loading Task Detail ...</span></h4>
                    <div class="form-horizontal" id="detailBody">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-danger" id="eventSave">Save</button>
                    <a class="btn btn-primary" id="eventUrl" target="_blank">Open Task Detail</a>
                </div>
            </div>
        </div>
    </div>
   
@endsection

@section('after-scripts-end')
    <script>
        var __token = '{!! $jwtToken !!}'
        console.log(__token)

        // initialize input widgets first
        $(document).ready(function(){
            $('#detailDateTime .time').timepicker({
                'showDuration': true,
                'timeFormat': 'g:i a'
            });

            $('#detailDateTime .date').datepicker({
                'format': 'mm/dd/yyyy',
                'autoclose': true
            });

            // initialize datepair
            $('#detailDateTime').datepair();
            $('#detailDateTime .date.start').datepicker('update', moment().format());
            $('#detailDateTime .time.start').timepicker('setTime', new Date());
            $('#detailDateTime .time.end').timepicker('setTime', moment().add(1,'hours').toDate());
            $('#detailDateTime').datepair('refresh');

            $('#detailBody').hide();
        })
    </script>
    {!! $calendar->script() !!}
@stop