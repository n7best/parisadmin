@extends('frontend.layouts.master')

@section('content')
    <div class="row">

        <div class="col-md-10 col-md-offset-1">

            <div class="panel panel-default">
                <div class="panel-heading"><i class="fa fa-home"></i> Tasks</div>
                <script>
                    var galleries = {};
                    var tasks = [];
                </script>
                <div class="panel-body">

                   @if(!empty($tasks))
                        @foreach($tasks as $catalog => $task)
                          <div class="panel-heading">
                            <h4 class="panel-title">
                                </span>{!! $catalog !!}</a>
                            </h4>
                          </div>
                          <div class="panel-body">
                          <style>
                            .list-group hr{
                              margin-top: 5px;
                              margin-bottom: 5px;
                            }
                          </style>
                          <ul class="list-group">
                              @foreach($task as $singleTask)
                              <form method="POST" action="{!! route('task.post').'?tid='.$singleTask['detail']->id !!}">
                              {!! csrf_field() !!}
                              <input id="inputStatus{!! $singleTask['detail']->id !!}" type="hidden" name="status" value="{!! $singleTask['detail']->status !!}"/>
                              <li class="list-group-item">
                                  <span data-type="select" class="label label-default taskStatus{!! $singleTask['detail']->id !!}" >{!! $singleTask['detail']->status !!}</span>
                                  <span class="prefix"><span class="label label-primary">{!! $singleTask['detail']->schedule_at !!}</span><b>{!! $singleTask['detail']->order_rid.' '.$singleTask['service']->display_name !!}</b> </span>
                                  <span class="pull-right">
                                    <button type="submit" class="btn btn-success btn-xs">Update 更新</button>
                                  </span>
                                  @if($singleTask['detail']->allowUpload())
                                  <div class="input-group">
                                    <span class="input-group-addon" id="basic-addon1">Photos</span>
                                    <!-- Button trigger modal -->
                                    <button type="button" class="btn btn-primary btn-xs btn-upload" data-tid="{!! $singleTask['detail']->id !!}" data-rid="{!! $singleTask['detail']->order_rid !!}">
                                      Media Upload
                                    </button>
                                  </div>
                                  @endif
                                  @foreach($singleTask['detail']->options() as $option)
                                    <div class="input-group">
                                      <span class="input-group-addon">{{$option->name}}</span>
                                      @if($option->type == 'text')
                                      <input type="text" class="taskOp{{$option->name}}{!! $singleTask['detail']->id !!}" name="options[{{$option->name}}]" value="{{$singleTask['options']['op-'.$option->name] or ''}}"/>
                                      @elseif ($option->type == 'integer')
                                      <input type="number" class="taskOp{{$option->name}}{!! $singleTask['detail']->id !!}" name="options[{{$option->name}}]" value="{{$singleTask['options']['op-'.$option->name] or ''}}"/>
                                      @elseif ($option->type == 'select')
                                      <select class="optselect taskOp{{$option->name}}{!! $singleTask['detail']->id !!}" name="options[{{$option->name}}]" >
                                        @foreach($option->options as $seOption)
                                        <option value="{{$seOption}}" @if(!empty($singleTask['options']['op-'.$option->name]) && $singleTask['options']['op-'.$option->name] == $seOption) selected @endif>{{$seOption}}</option>
                                        @endforeach
                                      </select>
                                      @endif
                                    </div>
                                  @endforeach
                                  <hr/>
                                  <div class="form-group">
                                    <label>Details:</label>
                                    <textarea class="form-control" name="details" rows="2">{!! $singleTask['detail']->details !!}</textarea>
                                  </div>
                              </li>
                              </form>
                              <script>
                               galleries[{!! $singleTask['detail']->id !!}] = {!! $singleTask['detail']->gallery() !!};
                               tasks.push({
                                 tid : '{!! $singleTask['detail']->id !!}',
                                 class: 'taskStatus{!! $singleTask['detail']->id !!}',
                                 status:'{!! $singleTask['detail']->status !!}',
                                 id: 'inputStatus{!! $singleTask['detail']->id !!}',
                                 options: {!! $singleTask['detail']->avaible_options !!},
                               });
                              </script>
                              @endforeach
                          </ul>
                        </div>
                        @endforeach
                   @else

                   @endif
                </div>
            </div><!-- panel -->

        </div><!-- col-md-10 -->

    </div><!-- row -->

    <!-- Modal -->
    <div class="modal fade" id="modalUpload" tabindex="-1" role="dialog" aria-labelledby="modalUploadLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="modalUploadLabel">Upload</h4>
          </div>
          <div class="modal-body">
                <h6>Uploaded Photos</h6>
                <div id="previewImages" style="
                  padding: 20px;
                  border: 1px solid #eee;
                  margin: 10px 0;
                "></div>
                <div id="pdfButton" role="button" class="btn btn-primary">Upload</div>
               </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary">Confirm Upload</button>
          </div>
        </div>
      </div>
    </div>

    <!-- The Bootstrap Image Gallery lightbox, should be a child element of the document body -->
    <div id="blueimp-gallery" class="blueimp-gallery">
        <!-- The container for the modal slides -->
        <div class="slides"></div>
        <!-- Controls for the borderless lightbox -->
        <h3 class="title"></h3>
        <a class="prev">‹</a>
        <a class="next">›</a>
        <a class="close">×</a>
        <a class="play-pause"></a>
        <ol class="indicator"></ol>
        <!-- The modal dialog, which will be used to wrap the lightbox content -->
        <div class="modal fade">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" aria-hidden="true">&times;</button>
                        <h4 class="modal-title"></h4>
                    </div>
                    <div class="modal-body next"></div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-left prev">
                            <i class="glyphicon glyphicon-chevron-left"></i>
                            Previous
                        </button>
                        <button type="button" class="btn btn-primary next">
                            Next
                            <i class="glyphicon glyphicon-chevron-right"></i>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="uploadPopup" style="
        position: fixed;
        width: 500px;
        bottom: 50px;
        right: 10px;
        border: 1px solid #eee;
        padding: 20px;
        background-color: #FBFBFB;
        display: none;
    ">
        <div id="closeUploadPopup" role="button" class="btn btn-primary" style="margin-bottom: 10px;">Close</div>
       <!-- The element where Fine Uploader will exist. -->
        <div id="fine-uploader" style="margin-top: 10px;">
    </div>

    <script type="text/template" id="qq-template">
        <div class="qq-uploader-selector qq-uploader">
            <div class="qq-total-progress-bar-container-selector qq-total-progress-bar-container">
                <div role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" class="qq-total-progress-bar-selector qq-progress-bar qq-total-progress-bar"></div>
            </div>
            <ul class="qq-upload-list-selector qq-upload-list" aria-live="polite" aria-relevant="additions removals">
                <li>
                    <div class="qq-progress-bar-container-selector">
                        <div role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" class="qq-progress-bar-selector qq-progress-bar"></div>
                    </div>
                    <span class="qq-upload-spinner-selector qq-upload-spinner"></span>
                    <img class="qq-thumbnail-selector" qq-max-size="100" qq-server-scale>
                    <span class="qq-upload-file-selector qq-upload-file"></span>
                    <span class="qq-edit-filename-icon-selector qq-edit-filename-icon" aria-label="Edit filename"></span>
                    <input class="qq-edit-filename-selector qq-edit-filename" tabindex="0" type="text">
                    <span class="qq-upload-size-selector qq-upload-size"></span>
                    <button type="button" class="qq-btn qq-upload-cancel-selector qq-upload-cancel">Cancel</button>
                    <button type="button" class="qq-btn qq-upload-retry-selector qq-upload-retry">Retry</button>
                    <button type="button" class="qq-btn qq-upload-delete-selector qq-upload-delete">Delete</button>
                    <span role="status" class="qq-upload-status-text-selector qq-upload-status-text"></span>
                </li>
            </ul>

            <dialog class="qq-alert-dialog-selector">
                <div class="qq-dialog-message-selector"></div>
                <div class="qq-dialog-buttons">
                    <button type="button" class="qq-cancel-button-selector">Close</button>
                </div>
            </dialog>

            <dialog class="qq-confirm-dialog-selector">
                <div class="qq-dialog-message-selector"></div>
                <div class="qq-dialog-buttons">
                    <button type="button" class="qq-cancel-button-selector">No</button>
                    <button type="button" class="qq-ok-button-selector">Yes</button>
                </div>
            </dialog>

            <dialog class="qq-prompt-dialog-selector">
                <div class="qq-dialog-message-selector"></div>
                <input type="text">
                <div class="qq-dialog-buttons">
                    <button type="button" class="qq-cancel-button-selector">Cancel</button>
                    <button type="button" class="qq-ok-button-selector">Ok</button>
                </div>
            </dialog>
        </div>
    </script>
@endsection

@section('after-scripts-end')
<script>
console.log(tasks)
//status select xedit
tasks.forEach(function(task){
  $('.'+task.class).editable({
      value: task.status,
      source: option2source(task.options)
  }).on('save',function(e, params){
      console.log(params.newValue);
      $('#'+task.id).val(params.newValue);
  });

  //convert backend result to xedit format
  function option2source(options){
    var source = [];
    options.forEach(function(status){
      source.push({value:status.type, text: status.type.charAt(0).toUpperCase() + status.type.slice(1)});
    });
    return source;
  }
})
var currentUploader = {
    tid: null,
    rid: null,
    uploader: null,
    gallery: null
};

var uploadStacks = {};

currentUploader.uploader = new qq.s3.FineUploader({
    debug: true,
    element: document.getElementById('fine-uploader'),
    request: {
        endpoint: '{!! config('services.amazon.expectedBucketName') !!}' + '.s3.amazonaws.com',
        accessKey: '{!! config('services.amazon.clientPublicKey') !!}'
    },
    signature: {
        endpoint: '{!!route('s3sign')!!}',
        version: 4
    },
    chunking: {
        concurrent: {
            enabled: true
        },
        enabled: true,
        partSize: 20971520
    },
    uploadSuccess: {
        endpoint: '{!!route('s3sign')!!}?success'
    },
    validation:{
        sizeLimit:{!! config('services.amazon.expectedMaxSize') !!}
    },
    objectProperties:{
      acl:'public-read',
      key: function(fileid){
        return uploadStacks[fileid].rid + '/' + uploadStacks[fileid].tid + '/' + qq.getUniqueId() + '.' + qq.getExtension(currentUploader.uploader.getName(fileid));
      }
    },
    extraButtons: [
        {
            element: document.getElementById("pdfButton"),
            validation: {
                allowedExtensions: ["jpg"]
            }
        }
    ],
    callbacks: {
        onComplete: function(id,name,obj,xhr) {
            let key = currentUploader.uploader.getKey(id);
            let imgUrl = 'https://s3.amazonaws.com/'+'{!! config('services.amazon.expectedBucketName') !!}'+'/'+key;
            console.log(imgUrl);
        },
        onSubmit: function(id,name){
          uploadStacks[id] = {
            rid: currentUploader.rid,
            tid: currentUploader.tid
          };

          currentUploader.uploader.setUploadSuccessParams({tid:currentUploader.tid, jwtToken:'{!! $jwtToken !!}'},id);
          $('.uploadPopup').show();
        },
        onAllComplete: function(){
          //$('.uploadPopup').hide();
        }
    }
});



$('.btn-upload').click(function(){
    currentUploader.tid = $(this).data('tid');
    currentUploader.rid = $(this).data('rid');
    $('#previewImages').html('');
    if(galleries[currentUploader.tid].length > 0) {

        galleries[currentUploader.tid].forEach(function(file){
            $('#previewImages').append('<a href="' + file.withlogo + '" title="' + file.title + '" data-gallery><img src="'+ file.thumbnail +'" alt="'+ file.title +'"></a>')
        });
        //blueimp.Gallery($('#links a'), $('#blueimp-gallery').data())
    }

    $('#modalUpload').modal('show');
});

$('#closeUploadPopup').click(function(){
  $('.uploadPopup').hide();
});

</script>
@endsection