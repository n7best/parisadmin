<!doctype html>
<html class="no-js" lang="">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Wedding Invite</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Economica|Great+Vibes">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="apple-touch-icon" href="apple-touch-icon.png">
    <script>
      window._xt_ = JSON.parse({!! $einvite->options !!});
      window._logRoute_ = "{!! route('api.storeeinvitelog') !!}";
      window._donateRoute_ = "{!! route('api.storedonations') !!}";
      window._token_ = "{!! $jwtToken !!}";
      window._userName_ = "{!! $userName !!}";
      window._checkedIn_ = {!! $checkedIn !!};
      console.log(window._xt_)
    </script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pace/1.0.2/pace.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/pace/1.0.2/themes/black/pace-theme-loading-bar.min.css">
    <script type="text/javascript"
          src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBMizn-17GxbP-GPG0RsWNUDZx7uz0AdQ8&libraries=geometry,places,visualization">
    </script>
  </head>
  <body>
    <div id="container"></div>
    <!-- <script src="/wxinvite/dist/main.js"></script> -->
    <script src="https://s3.amazonaws.com/paris-photo/einvite/build.js"></script>
    <script src="http://res.wx.qq.com/open/js/jweixin-1.0.0.js" type="text/javascript" charset="utf-8"></script>
    <script type="text/javascript" charset="utf-8">
        var xt = window._xt_;
        wx.config({!! $wcjs->config(array('onMenuShareTimeline', 'onMenuShareAppMessage'), false) !!});
        wx.ready(function(){
            // config信息验证后会执行ready方法，所有接口调用都必须在config接口获得结果之后，config是一个客户端的异步操作，所以如果需要在页面加载时就调用相关接口，则须把相关接口放在ready函数中调用来确保正确执行。对于用户触发时才调用的接口，则可以直接调用，不需要放在ready函数中。
            wx.onMenuShareTimeline({
                title: xt.infos.groom + ' & ' + xt.infos.bride + '我们结婚啦!', // 分享标题
                link: window.location.href, // 分享链接
                imgUrl: 'https://s3.amazonaws.com/paris-photo/einvite/20356248-04ac-412c-9051-42ce422a228d.png', // 分享图标
                success: function () {
                    // 用户确认分享后执行的回调函数
                },
                cancel: function () {
                    // 用户取消分享后执行的回调函数
                }
            });

            wx.onMenuShareAppMessage({
                title: xt.infos.groom + ' & ' + xt.infos.bride + ' 我们结婚啦!', // 分享标题
                desc: '结婚呢，最要紧是开心。你饿不饿?请你来吃喜宴。呐，不要说我没提醒你', // 分享描述
                link: window.location.href, // 分享链接
                imgUrl: 'https://s3.amazonaws.com/paris-photo/einvite/20356248-04ac-412c-9051-42ce422a228d.png', // 分享图标
                type: 'link', // 分享类型,music、video或link，不填默认为link
                dataUrl: '', // 如果type是music或video，则要提供数据链接，默认为空
                success: function () {
                    // 用户确认分享后执行的回调函数
                },
                cancel: function () {
                    // 用户取消分享后执行的回调函数
                }
            });
        });
    </script>
  </body>
</html>
