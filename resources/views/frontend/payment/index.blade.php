@extends('frontend.layouts.master')

@section('content')
    <div class="row">

        <div class="col-md-10 col-md-offset-1">

            <div class="panel panel-default">
                <div class="panel-heading"><i class="fa fa-home"></i> Payments</div>
                <div class="panel-body">
                  <div class="row" style="margin: 10px;">
                    @foreach($companies as $company)
                    <label class="checkbox-inline">
                      <input type="checkbox" class="companyFilter" value="{!! $company->code !!}" checked> {!! $company->code !!}
                    </label>
                    @endforeach

                    <span class="pull-right">
                      <div id="reportrange" class="pull-right" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%">
                          <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
                          <span></span> <b class="caret"></b>
                      </div>
                    </span>
                  </div>
                  <div class="row" style="margin-top:20px;">
                        <div class="col-md-3">
                            <a class="info-tiles tiles-inverse has-footer" href="#tOrders" aria-controls="tOrders" role="tab" data-toggle="tab">
                              <div class="tiles-heading">
                                  Orders
                            </div>
                            <div class="tiles-body text-center" id="dOrders">
                                0
                            </div>
                            <div class="tiles-footer">
                              see details
                            </div>
                        </a>
                        </div>
                          <div class="col-md-3">
                            <a class="info-tiles tiles-green has-footer" href="#tRevenues" aria-controls="tRevenues" role="tab" data-toggle="tab">
                            <div class="tiles-heading">
                                Revenues
                            </div>
                            <div class="tiles-body text-center" id="dRevenues">
                                $0
                            </div>
                            <div class="tiles-footer">
                              see details
                            </div>
                        </a>
                        </div>
                          <div class="col-md-3">
                            <a class="info-tiles tiles-blue has-footer" href="#tCustomers" aria-controls="tCustomers" role="tab" data-toggle="tab">
                            <div class="tiles-heading">
                                Customers
                                <span class="pull-right"> w/ Order</span>
                            </div>
                            <div class="tiles-body text-center" id="dCustomers">
                                0
                            </div>
                            <div class="tiles-footer">
                              see all customers
                            </div>
                        </a>
                        </div>
                          <div class="col-md-3">
                            <a class="info-tiles tiles-midnightblue has-footer" href="#tRaffles" aria-controls="tRaffles" role="tab" data-toggle="tab">
                            <div class="tiles-heading">
                                Raffles
                            </div>
                            <div class="tiles-body text-center" id="dRaffles">
                                0
                            </div>
                            <div class="tiles-footer">
                              see all logs
                            </div>
                        </a>
                        </div>
                    </div>
                    <div class="row" style="margin:15px;">
                      <!-- Tab panes -->
                       <div class="tab-content">
                         <div role="tabpanel" class="tab-pane active" id="tOrders">
                           <table class="table table-striped">
                            <thead>
                              <tr>
                                <th>ID</th>
                                <th>Invoice Total</th>
                                <th>Invoice Balance</th>
                                <th>Create Time</th>
                              </tr>
                            </thead>
                             <tbody id="tableOrder"></tbody>
                           </table>
                         </div>
                         <div role="tabpanel" class="tab-pane" id="tRevenues">
                           <table class="table table-striped">
                            <thead>
                              <tr>
                                <th>ID</th>
                                <th>Payment Type</th>
                                <th>Amount</th>
                                <th>Status</th>
                                <th>Action</th>
                              </tr>
                            </thead>
                             <tbody id="tableRevenues"></tbody>
                           </table>
                         </div>
                         <div role="tabpanel" class="tab-pane" id="tCustomers">
                           <table class="table table-striped">
                            <thead>
                              <tr>
                                <th>Name</th>
                                <th>Time</th>
                              </tr>
                            </thead>
                             <tbody id="tableCustomers"></tbody>
                           </table>
                         </div>
                         <div role="tabpanel" class="tab-pane" id="tRaffles">
                           <ul class="list-group" id="tableRaffles">
                           </ul>
                         </div>
                       </div>
                    </div>
                </div>
            </div><!-- panel -->

        </div><!-- col-md-10 -->

    </div><!-- row -->
@endsection

@section('after-scripts-end')
<script>
var paymentsData = {!! $payments !!}
var ordersData = {!! $orders !!}
var customersData = {!! $customers !!}
var rafflesData = {!! $raffles !!}
console.log(rafflesData)

$(function() {
    var filters = []
    var start = moment("{!! $start->toDateString() !!}");
    var end = moment("{!! $end->toDateString() !!}");

    function cb(start, end, init = false) {
      if(init === true){
        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
      }else{
        var url = "{!! url()->current() !!}?start=" + start.format('MMMM D, YYYY') + "&end=" + end.format('MMMM D, YYYY')

        window.location = encodeURI(url);
      }
    }

    $('#reportrange').daterangepicker({
        startDate: start,
        endDate: end,
        ranges: {
           'Today': [moment(), moment()],
           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
           'This Month': [moment().startOf('month'), moment().endOf('month')],
           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
    }, cb);


    $('.companyFilter').change(function(){
      updateAll()
    })

    cb(start, end, true);
    updateAll()

    function updateAll(){
      updateFilter()
      updateRevenues()
      updateOrder()
      updateCustomer()
      updateRaffle()
    }

    //updates
    function updateFilter(){
      filters = []
      $('.companyFilter').each(function(index){
        if($(this).prop("checked")) filters.push($(this).val())
      })
      console.log('Filter ->', filters)
    }

    function updateRevenues(){
      var elRevenues = $('#dRevenues');
      var elTable = $('#tableRevenues')
      var paymentTotal = 0
      elTable.html('')
      paymentsData.forEach(function(payment){
        if($.inArray(payment.company, filters) > -1) {
          paymentTotal += payment.amount
          elTable.append('<tr><td>' + payment.rid + 
            '</td><td>' + payment.type + 
            '</td><td>' + payment.amount + 
            '</td><td>' + payment.status + 
            '</td><td></td></tr>'
          )
        }
      })
      elRevenues.html('$' + paymentTotal)
    }

    function updateOrder(){
      var elOrder = $('#dOrders');
      var elTable = $('#tableOrder')
      var total = 0;
      elTable.html('')
      ordersData.forEach(function(order){
        if($.inArray(order.company, filters) > -1) {
          total++
          elTable.append('<tr><td>' + order.rid + '</td><td>' + order.price + '</td><td>' + order.balance + '</td><td>' + order.created_at + '</td></tr>')
        }
      })
      elOrder.html(total)
    }

    function updateCustomer(){
      var elOrder = $('#dCustomers');
      var elTable = $('#tableCustomers')
      var total = 0;
      elTable.html('')
      customersData.forEach(function(customer){
        if($.inArray(customer.company, filters) > -1) {
          total++
          elTable.append('<tr><td>' + customer.name + 
            '</td><td>' + customer.created_at + 
            '</td><td></td></tr>'
          )
        }
      })
      elOrder.html(total)
    }

    function updateRaffle(){
      var elRaffle = $('#dRaffles');
      var elTable = $('#tableRaffles')

      rafflesData.forEach(function(raffle){
        elTable.append('<li class="list-group-item"><span class="badge">' + raffle.created_at + 
          '</span>' + raffle.detail + 
          '</li>'
        )
      })
      elRaffle.html(rafflesData.length)
    }
});

</script>
@endsection