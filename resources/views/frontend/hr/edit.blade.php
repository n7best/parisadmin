@extends('frontend.layouts.master')

@section('content')
    <div class="row">

        <div class="col-md-10 col-md-offset-1">

            <div class="panel panel-default">
                <div class="panel-heading"><i class="fa fa-home"></i> Human Resource</div>

                <div class="panel-body">
                  <form method="POST" action="{!! route('hr.update', ['id' => $hr->id]) !!}">
                  <input type="hidden" name="_method" value="PUT">
                  {!!csrf_field()!!}
                  <div class="form-group col-sm-6">
                      {!! Form::label('startat', 'Start:') !!}
                      <input class="form-control" id="hrStart" type="text" name="hrStart" value=""/>
                  </div>

                  <div class="form-group col-sm-6">
                      {!! Form::label('endat', 'End:') !!}
                      <input class="form-control" id="hrEnd" type="text" name="hrEnd" value=""/>
                  </div>
                  @permission('approve-hr')
                  <div class="form-group col-sm-12">
                      {!! Form::label('status', 'Status:') !!}
                      <select class="form-control js-status-select" id="status" name="status">
                        <option id="Pending" value="Pending">Pending</option>
                        <option id="Approve" value="Approve">Approve</option>
                        <option id="Decline" value="Decline">Decline</option>
                      </select>
                  </div>
                  @endauth
                  <div class="form-group col-sm-12">
                      {!! Form::label('type', 'Type:') !!}
                      <select class="form-control js-type-select" id="type" name="type">
                        <option id="vacation" value="vacation">Vacation</option>
                        <option id="special" value="special">Special</option>
                        <option id="work" value="work">Work</option>
                      </select>
                  </div>

                  <div class="form-group col-sm-12">
                      {!! Form::label('Reason', 'Reason:') !!}
                      @include('tinymce::tpl')
                      <textarea class="tinymcereason" name="reason">{!! $hr->remark !!}</textarea>
                  </div>

                  <div class="form-group col-sm-12">
                      {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
                      <a href="{!! route('hr.index') !!}" class="btn btn-default">Cancel</a>
                  </div>
                  </form>
                </div>
            </div><!-- panel -->

        </div><!-- col-md-10 -->

    </div><!-- row -->
@endsection

@section('after-scripts-end')
<script>

$('document').ready(function(){
  $('#hrStart').daterangepicker({singleDatePicker: true, startDate: moment('{!! Carbon::parse($hr->start_at)->toIso8601String() !!}')});
  $('#hrEnd').daterangepicker({singleDatePicker: true, startDate: moment('{!! Carbon::parse($hr->end_at)->toIso8601String() !!}')});

  $('#{!!$hr->type!!}').attr('selected', 'selected');
  $(".js-type-select").select2();
  @permission('approve-hr')
  $('#{!!$hr->status!!}').attr('selected', 'selected');
  $(".js-status-select").select2();
  @endauth
});

</script>
@endsection