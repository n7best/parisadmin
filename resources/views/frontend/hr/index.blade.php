@extends('frontend.layouts.master')

@section('content')
    <div class="row">

        <div class="col-md-10 col-md-offset-1">

            <div class="panel panel-default">
                <div class="panel-heading"><i class="fa fa-home"></i> Human Resource</div>
                <table class="table table-responsive">
                    <thead>
                      <th>Id</th>
                      <th>Status</th>
                      <th>Type</th>
                      <th>Start At</th>
                      <th>End At</th>
                      <th width="50px">Action</th>
                    </thead>
                    <tbody>
                    @foreach($hrs as $hr)
                        <tr>
                            <td>{!! $hr->id !!}</td>
                            <td>{!! $hr->status !!}</td>
                            <td>{!! $hr->type !!}</td>
                            <td>{!! $hr->start_at !!}</td>
                            <td>{!! $hr->end_at !!}</td>
                            <td>
                                @if($hr->status == 'Pending')
                                <a href="{!! route('hr.edit', [$hr->id]) !!}"><i class="glyphicon glyphicon-edit"></i></a>
                                <a href="{!! route('hr.delete', [$hr->id]) !!}" onclick="return confirm('Are you sure wants to delete this Customers?')">
                                    <i class="glyphicon glyphicon-trash"></i>
                                </a>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <div class="panel-body">
                  @permission('view-hr')
                  <a href="{!! route('hr.create') !!}" class="btn btn-success">Create New Request</a>
                  @endauth
                </div>
            </div><!-- panel -->

        </div><!-- col-md-10 -->

    </div><!-- row -->
@endsection

@section('after-scripts-end')
<script>



</script>
@endsection