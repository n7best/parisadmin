@extends('frontend.layouts.master')

@section('content')
    <div class="row">

        <div class="col-md-10 col-md-offset-1">

            <div class="panel panel-default">
                <div class="panel-heading"><i class="fa fa-home"></i> Human Resource</div>

                <div class="panel-body">
                  <form method="POST" action="{!! route('hr.store') !!}">
                  {!!csrf_field()!!}
                  <div class="form-group col-sm-6">
                      {!! Form::label('startat', 'Start:') !!}
                      <input class="form-control" id="hrStart" type="text" name="hrStart" value=""/>
                  </div>

                  <div class="form-group col-sm-6">
                      {!! Form::label('endat', 'End:') !!}
                      <input class="form-control" id="hrEnd" type="text" name="hrEnd" value=""/>
                  </div>

                  <div class="form-group col-sm-12">
                      {!! Form::label('type', 'Type:') !!}
                      <select class="form-control js-type-select" id="type" name="type">
                        <option value="vacation">Vacation</option>
                        <option value="special">Special</option>
                        <option value="work">Work</option>
                      </select>
                  </div>

                  <div class="form-group col-sm-12">
                      {!! Form::label('Reason', 'Reason:') !!}
                      @include('tinymce::tpl')  
                      <textarea class="tinymcereason" name="reason"></textarea>
                  </div>

                  <div class="form-group col-sm-12">
                      {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
                      <a href="{!! route('hr.index') !!}" class="btn btn-default">Cancel</a>
                  </div>
                  </form>
                </div>
            </div><!-- panel -->

        </div><!-- col-md-10 -->

    </div><!-- row -->
@endsection

@section('after-scripts-end')
<script>

$('document').ready(function(){
  $('#hrStart').daterangepicker({singleDatePicker: true});
  $('#hrEnd').daterangepicker({singleDatePicker: true});
  $(".js-type-select").select2();
});

</script>
@endsection