@extends('frontend.layouts.mobile')

@section('content')
<div class="weui_msg">
  <div class="weui_icon_area">
    <img src="/img/parislogoblack.png" width="33%" />
  </div>
  <div class="weui_text_area">
    <h2 class="weui_msg_title">夏季抽奖活动</h2>
    <p class="weui_msg_desc">点击抽奖！快来试试你的运气吧！各种大奖等您你来拿！</p>
  </div>
  <div class="weui_opr_area">
    <div class='container'>
      <div class='rafflebox'>
        <div class='pointer'></div>
        <div class='boxwrapper'>
          <ul class='giftwrapper'>
          </ul>
        </div>
      </div>
    </div>
    <p class="weui_btn_area">
      <a href="javascript:;" class="weui_btn weui_btn_primary open-popup" data-target="#userinfo">抽奖</a>
    </p>
  </div>
  <div class="weui_extra_area">
    <a href="javascript:;">巴黎婚纱 & 法国婚纱</a>
  </div>
</div>

<div id="userinfo" class="weui-popup-container" style="z-index: 60;">
  <div class="weui-popup-overlay"></div>
  <div class="weui-popup-modal" >
    <div class="weui_cells_title">输入基本信息</div>
    <div class="weui_cells weui_cells_form" >
      <div class="weui_cell">
        <div class="weui_cell_hd"><label class="weui_label">名称</label></div>
        <div class="weui_cell_bd weui_cell_primary">
          <input class="weui_input" type="text" placeholder="请输入您的名称" id="formname">
        </div>
      </div>
      <div class="weui_cell">
        <div class="weui_cell_hd"><label class="weui_label">电话</label></div>
        <div class="weui_cell_bd weui_cell_primary">
          <input class="weui_input" type="test" placeholder="请输入您的电话" id="formphone">
        </div>
      </div>
      <div class="weui_cell">
        <div class="weui_cell_hd"><label class="weui_label">EMail</label></div>
        <div class="weui_cell_bd weui_cell_primary">
          <input class="weui_input" type="text" placeholder="请输入您的电子邮箱" id="formemail">
        </div>
      </div>
    </div>
    <div class="button_sp_area" style="margin: 15px;">
      <a href="javascript:;" class="weui_btn weui_btn_primary" id="btnConfirm">抽奖</a>
      <a href="javascript:;" class="weui_btn weui_btn_plain_default close-popup">取消</a>
    </div>
  </div>
</div>
@endsection

@section('after-scripts-end')
    <script>
        var prizes = {!!$prizes!!}
        var user = {!!$user!!}
        console.log(prizes, user);
        $.each(prizes, function(i, prize){
          var tpl = '<li class="gift"><img src="'+ prize.img +'"><p>'+prize.name+'</p></li>'
          $('.giftwrapper').append(tpl)
        })
        $(document).ready(function(){
          var giftamount = prizes.length - 1;
          var gw = $('.gift').outerWidth(true);
          var giftcenter = gw/2;
          var cycle = 7;

          var containercenter = $('.boxwrapper').outerWidth(true)/2;
          for(var i = 0; i <=4; i++)
          {
            var giftduplicate = $('.giftwrapper').children().clone(true,true);
             $('.giftwrapper').append(giftduplicate);
          }

          $('#btnConfirm').click(function(){
            if(!isValid()){
              $.alert("请填写必要的讯息");
            }else{
              var btn = $(this);
              $.closePopup()
              btn.hide();
              $.showLoading('验证中');

              var data = {
                uid: '{{$uid}}',
                wc_user: user,
                contacts: {
                  name: $('#formname').val(),
                  phone: $('#formphone').val(),
                  email: $('#formemail').val()
                }
              }

              $.post('{{route('frontend.raffledraw')}}', data, function(data){
                console.log('success', data)
                var won = 0;
                $.each(prizes, function(i, prize){
                  if(data.name == prize.name) won = i
                })

                var randomgift = won
                var distance = giftamount * gw * cycle + containercenter + (randomgift*gw) - giftcenter;

                $( ".giftwrapper" ).css({left: gw+'px'});

                $('.giftwrapper').animate({left: "-="+distance},3000,function(){
                  $.alert('You Won Gift' + prizes[randomgift].name + ',验证码已经发送到您的邮箱或则手机', "恭喜您！", function(){btn.show();});
                 });
              }).fail(function(data){
                if(data.status==500 || data.status==0){
                    $.alert('System Error, Make sure everything is fillin completely')
                }
                console.log('fail', data)
                $.alert(data.responseJSON.msg, function(){btn.show();});
              }).always(function(){
                $.hideLoading();
              })
            }
          });
        })

        function isValid(){
          if(!$('#formname').val()) return false;
          if(!$('#formphone').val() && !$('#formemail').val()) return false;
          return true;
        }
    </script>
@stop