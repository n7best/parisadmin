@extends('frontend.layouts.mobile')

@section('content')
<div class="weui_msg">
  <div class="weui_icon_area">
    <img src="/img/logo.jpg" width="33%" />
  </div>
  <div class="weui_text_area">
    <h2 class="weui_msg_title">绑定账户</h2>
    <p class="weui_msg_desc">请输入客服人员给您的验证码</p>
  </div>
  <div class="weui_opr_area">
    <div class="weui_cells weui_cells_form">
      <div class="weui_cell">
        <div class="weui_cell_hd"><label class="weui_label">验证码</label></div>
        <div class="weui_cell_bd weui_cell_primary">
          <input class="weui_input" type="tel" placeholder="验证码" id="vcode">
        </div>
      </div>
    </div>
    <p class="weui_btn_area">
      <a href="javascript:;" class="weui_btn weui_btn_primary" id="btnConfirm">确定</a>
    </p>
  </div>
  <div class="weui_extra_area">
    <a href="javascript:;">巴黎婚纱 & 法国婚纱</a>
  </div>
</div>

@endsection

@section('after-scripts-end')
    <script>
        //Being injected from FrontendController
        console.log('test');
        $(document).ready(function(){
          //$('#invoice').pullToRefresh();
          $('#btnConfirm').click(function(){
            $.showLoading('验证中');

            $.post('{{route('frontend.mobileverify')}}', { uid: '{{$uid}}', vcode: $('#vcode').val()}, function(data){
              console.log('success', data)
              $.alert('您已经和'+data.data.name+'的帐户绑定', function(){
                window.location = "/mobile";
              });
            }).fail(function(data){
              console.log('fail', data)
              $.alert(data.responseJSON.msg);
            }).always(function(){
              $.hideLoading();
            })
          })
        })
    </script>
@stop