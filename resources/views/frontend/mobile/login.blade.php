@extends('frontend.layouts.mobile')

@section('content')
<div class="weui_msg">
  <div class="weui_icon_area">
    <img src="/img/parislogoblack.png" width="33%" />
  </div>
  <div class="weui_text_area">
    <h2 class="weui_msg_title">登录</h2>
    <p class="weui_msg_desc">点击抽奖！快来试试你的运气吧！各种大奖等您你来拿！</p>
  </div>
  <div class="weui_opr_area">
    <div class='container'>
      
    </div>
    <p class="weui_btn_area">
      <a href="javascript:;" class="weui_btn weui_btn_primary open-popup" data-target="#userinfo">抽奖</a>
    </p>
  </div>
  <div class="weui_extra_area">
    <a href="javascript:;">巴黎婚纱 & 法国婚纱</a>
  </div>
</div>
@stop

@section('after-scripts-end')
    <script>
        
    </script>
@stop