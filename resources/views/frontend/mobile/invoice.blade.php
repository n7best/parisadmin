<div class="weui_panel weui_panel_access">
  <div class="weui_panel_hd">您的订单</div>
  <script>
      var galleries = {};
  </script>
  <div class="weui_panel_bd">
    @foreach($user->orders as $order)
    <div class="weui_media_box weui_media_appmsg " >
      <div class="weui_media_hd">
        @if($order->status == 'created' || $order->status == 'progress')
        <span class="lnr lnr-hourglass"></span>
        @elseif($order->status == 'complete')
        <span class="lnr lnr-thumbs-up"></span>
        @else
        <span class="lnr lnr-cross"></span>
        @endif
      </div>
      <div class="weui_media_bd">
        {{-- <h4 class="weui_media_title">PA-A0038</h4>
        <ul class="weui_media_info">
          <li class="weui_media_info_meta">创建时间: 20160104</li>
          <li class="weui_media_info_meta">更新: 20160705</li>
          <li class="weui_media_info_meta weui_media_info_meta_extra"><span class="lnr lnr-hourglass"></span> 进度中</li>
        </ul> --}}
        <div class="weui_media_box weui_media_text" >
          <h4 class="weui_media_title">{{$order->rid}}</h4>

          <ul class="weui_media_info">
            @if($order->status == 'created' || $order->status == 'progress')
            <li class="weui_media_info_meta" style="color: #777;">状态: 进行中</li>
            @elseif($order->status == 'complete')
            <li class="weui_media_info_meta" style="color: #777;">状态: 完成</li>
            @else
            <li class="weui_media_info_meta" style="color: #777;">状态: 取消</li>
            @endif
          </ul>
          <ul class="weui_media_info">
            <li class="weui_media_info_meta">创建时间: {{$order->created_at}}</li>
            <li class="weui_media_info_meta">更新: {{$order->updated_at}}</li>
          </ul>
          <div>
            <a href="javascript:;" class="weui_btn weui_btn_mini weui_btn_primary open-popup" data-target="#record{{$order->rid}}">具体纪录</a>
            @foreach($order->tasks as $task)
              @if($task->status == 'approved')
              <script>
                galleries[{!! $task->id !!}] = {!! $task->galleryMobile() !!};
              </script>
              <a href="javascript:;" class="weui_btn weui_btn_mini weui_btn_primary open-invoice" data-tid="{!! $task->id !!}">选择照片</a>
              @endif
            @endforeach
          </div>
        </div>
      </div>
    </div>
    <div class="weui-popup-container" id="record{{$order->rid}}">
      <div class="weui-popup-modal">
        <div class="weui_panel">
          <div class="weui_panel_hd">{{$order->rid}}订单纪录:</div>
          <div class="weui_panel_bd">
            @foreach($order->notes->note as $note)
            <div class="weui_media_box weui_media_text">
              {{-- <h4 class="weui_media_title">{!! $note->name !!}</h4> --}}
              <p class="weui_media_desc">{!! $note->message !!}</p>
              <ul class="weui_media_info">
                <li class="weui_media_info_meta">{!! $note->name !!}</li>
                <li class="weui_media_info_meta">{!! $note->created_at !!}</li>
              </ul>
            </div>
            @endforeach
          </div>
        </div>
        <a href="javascript:;" class="weui_btn weui_btn_primary close-popup" style="margin: 10px;">关闭</a>
      </div>
    </div>
    @endforeach
  </div>
</div>



<div id="imagePicker" class="weui-popup-container">
  <div class="weui-popup-modal">
    <div class="weui-row picker-container" style="margin-top: 50px; position: relative;"></div>
    <div class="toolbar" style="position:fixed; top: 0;z-index:200 !important;">
      <div class="toolbar-inner">
        <a href="javascript:;" class="picker-button picker-btn-left picker-close">关闭</a>
        <span href="javascript:;" class="picker-button">
          <a href="javascript:;" class="weui_btn weui_btn_disabled weui_btn_mini weui_btn_plain_primary picker-btn-action picker-btn-select">选择</a>
        </span>
        <h1 class="title picker-title" style="line-height: inherit;">标题</h1>
      </div>
    </div>
  </div>
</div>
