<div class="weui_cells_title">基本信息</div>
<div class="weui_cells weui_cells_form">
  <div class="weui_cell">
    <div class="weui_cell_hd"><label class="weui_label">电话</label></div>
    <div class="weui_cell_bd weui_cell_primary">
      <input class="weui_input" type="tel" placeholder="请输入电话号" id="setPhone" value="{{$user->phone}}">
    </div>
  </div>
  <div class="weui_cell">
    <div class="weui_cell_hd"><label class="weui_label">Email</label></div>
    <div class="weui_cell_bd weui_cell_primary">
      <input class="weui_input" type="text" placeholder="请输入Email" id="setEmail" value="{{$user->getMeta('email', '')}}">
    </div>
  </div>
  <div class="weui_cell weui_cell_switch">
    <div class="weui_cell_hd weui_cell_primary">接受通知</div>
    <div class="weui_cell_ft">
      <input class="weui_switch" type="checkbox" id="setNotification">
    </div>
  </div>
</div>

<a href="javascript:;" class="weui_btn weui_btn_primary" style="margin: 5px;" id="btnUpdateUser">更新</a>
