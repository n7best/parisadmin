<style>

.portlet {
    margin-bottom: 15px;
}

.btn-white {
    border-color: #cccccc;
    color: #333333;
    background-color: #ffffff;
}

.portlet {
    border: 1px solid;
}

.portlet .portlet-heading {
    padding: 0 15px;
}

.portlet .portlet-heading h4 {
    padding: 1px 0;
    font-size: 16px;
}

.portlet .portlet-heading a {
    color: #444444;
}

.portlet .portlet-heading a:hover,
.portlet .portlet-heading a:active,
.portlet .portlet-heading a:focus {
    outline: none;
}

.portlet .portlet-widgets .dropdown-menu a {
    color: #444444;
}

.portlet .portlet-widgets ul.dropdown-menu {
    min-width: 0;
}

.portlet .portlet-heading .portlet-title {
    float: left;
}

.portlet .portlet-heading .portlet-title h4 {
    margin: 10px 0;
}

.portlet .portlet-heading .portlet-widgets {
    float: right;
    margin: 8px 0;
}

.portlet .portlet-heading .portlet-widgets .tabbed-portlets {
    display: inline;
}

.portlet .portlet-heading .portlet-widgets .divider {
    margin: 0 5px;
}

.portlet .portlet-body {
    padding: 15px;
    background: #fff;
}

.portlet .portlet-footer {
    padding: 10px 15px;
    background: #f5f5f5;
}

.portlet .portlet-footer ul {
    margin: 0;
}

.portlet-green,
.portlet-green>.portlet-heading {
    border-color: #16a085;
}

.portlet-green>.portlet-heading {
    color: #fff;
    background-color: #16a085;
}

.portlet-orange,
.portlet-orange>.portlet-heading {
    border-color: #f39c12;
}

.portlet-orange>.portlet-heading {
    color: #fff;
    background-color: #f39c12;
}

.portlet-blue,
.portlet-blue>.portlet-heading {
    border-color: #2980b9;
}

.portlet-blue>.portlet-heading {
    color: #fff;
    background-color: #2980b9;
}

.portlet-red,
.portlet-red>.portlet-heading {
    border-color: #e74c3c;
}

.portlet-red>.portlet-heading {
    color: #fff;
    background-color: #e74c3c;
}

.portlet-purple,
.portlet-purple>.portlet-heading {
    border-color: #8e44ad;
}

.portlet-purple>.portlet-heading {
    color: #fff;
    background-color: #8e44ad;
}

.portlet-default,
.portlet-default>.portlet-heading {
    border-color: #dddddd;
}

.portlet-default>.portlet-heading,
.portlet-dark-blue>.portlet-heading {
    color: #444444;
    background-color: #f5f5f5;
}

.portlet-basic,
.portlet-basic>.portlet-heading {
    border-color: #333;
}

.portlet-basic>.portlet-heading {
    border-bottom: 1px solid #333;
    color: #333;
    background-color: #fff;
}

@media(min-width:768px) {
    .portlet {
        margin-bottom: 30px;
    }
}

.text-green {
    color: #16a085;
}

.text-orange {
    color: #f39c12;
}

.text-red {
    color: #e74c3c;
}

.chat-widget hr{
    margin-top: 5px;
    margin-bottom: 5px;
}

.chat-widget b{
    text-decoration: underline;
    background: #eee;
    padding: 1px;
}
</style>
<div class="portlet portlet-default" id="chatbox">
    <div class="portlet-heading">
        <div class="portlet-title">
            <h4><i class="fa fa-circle text-green"></i> Notes</h4>
        </div>
        <div class="portlet-widgets">
            <span class="divider"></span>
            <a data-toggle="collapse" data-parent="#accordion" href="#chat"><i class="fa fa-chevron-down"></i></a>
        </div>
        <div class="clearfix"></div>
    </div>
    <div id="chat" class="panel-collapse collapse in">
        <div>
        <div class="portlet-body chat-widget"  style="overflow-y: auto; width: auto; height: 100%;">
            @foreach($notes as $note)
                @if($note->internal)
                @permission('view-note-internal')
                <div class="row">
                    <div class="col-lg-12">
                        <div class="media">
                            <div class="media-body">
                                <h6 class="media-heading"><span class="label label-info">Internal</span>  {!! $note->name !!}
                                    <span class="small pull-right">{!! $note->created_at !!}</span>
                                </h6>
                                <p>{!! $note->message !!}</p>
                            </div>
                        </div>
                    </div>
                </div>
                <hr>
                @endauth
                @else
                <div class="row">
                    <div class="col-lg-12">
                        <div class="media">
                            <div class="media-body">
                                <h6 class="media-heading">{!! $note->name !!}
                                    <span class="small pull-right">{!! $note->created_at !!}</span>
                                </h6>
                                <p>{!! $note->message !!}</p>
                            </div>
                        </div>
                    </div>
                </div>
                <hr>
                @endif
            @endforeach
        </div>
        </div>
        @permission('post-note')
        <div class="portlet-footer">
            <form role="form" method="post" action="{!! route('notes.post',['ni'=>$notesid]) !!}">
                {!! csrf_field() !!}
                <div class="form-group">
                    <textarea class="form-control" name="message" placeholder="Enter message..."></textarea>
                </div>
                <div class="form-group">
                    @permission('post-note-internal')
                        <label><input type="checkbox" name="internal" value="1"> Internal</label>
                    @endauth
                    <button type="submit" class="btn btn-default pull-right">Send</button>
                    <div class="clearfix"></div>
                </div>
            </form>
        </div>
        @endauth
    </div>
</div>
<script>
var chatbox = document.getElementById("chatbox");
chatbox.parentElement.scrollTop = chatbox.scrollHeight;
</script>