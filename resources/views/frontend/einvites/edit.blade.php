@extends('frontend.layouts.master')

@section('content')
    <div class="row">

        <div class="col-md-10 col-md-offset-1">

            <div class="panel panel-default">
                <div class="panel-heading"><i class="fa fa-home"></i> Edit EInvite {{$einvite->id}}</div>

                <div class="panel-body">

                  <div>

                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                      <li role="presentation" class="active"><a href="#settings" aria-controls="settings" role="tab" data-toggle="tab">Settings</a></li>
                      <li role="presentation"><a href="#logs" aria-controls="logs" role="tab" data-toggle="tab">logs</a></li>
                      <li role="presentation"><a href="#donations" aria-controls="donations" role="tab" data-toggle="tab">Donations</a></li>
                    </ul>

                    <!-- Tab panes -->
                    <div class="tab-content">
                      <div role="tabpanel" class="tab-pane active" id="settings">
                        <form method="POST" action="{!! route('einvite.update', $einvite->id) !!}">
                        <input type="hidden" name="_method" value="PUT">
                        {!!csrf_field()!!}
                        <div class="form-group col-sm-12">
                            <style>
                            #editor_holder h3{
                              font-size: 20px;
                            }
                            </style>
                            <input type="hidden" name="options" id="optionsJSON" value="" />
                            <div id="editor_holder"></div>
                        </div>

                        <div class="form-group col-sm-12">
                            {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
                            <a href="{!! route('einvite.index') !!}" class="btn btn-default">Cancel</a>
                        </div>
                        </form>
                      </div>
                      <div role="tabpanel" class="tab-pane" id="logs">
                        <table class="table table-condensed">
                          <thead>
                            <td>time</td>
                            <td>message</td>
                          </thead>
                          <tbody id="tableLogs">
                          </tbody>
                        </table>
                        <div class="row">
                          <div class="col-md-3">
                            <div class="input-group">
                              <input type="number" class="form-control" placeholder="Time in seconds" id="inputTime">
                            </div><!-- /input-group -->
                          </div><!-- /.col-md-6 -->
                          <div class="col-md-6">
                            <div class="input-group">
                              <input type="text" class="form-control" placeholder="Message" id="inputMessage">
                              <span class="input-group-btn">
                                <button class="btn btn-default" type="button" id="btnAddMessage">Add</button>
                              </span>
                            </div><!-- /input-group -->
                          </div><!-- /.col-lg-6 -->
                        </div><!-- /.row -->
                      </div>
                      <div role="tabpanel" class="tab-pane" id="donations">
                        <table class="table table-condensed">
                          <thead>
                            <td>Name</td>
                            <td>Amount</td>
                            <td>Type</td>
                            <td>Token</td>
                          </thead>
                          <tbody id="tableDonations">
                          </tbody>
                        </table>
                        <div class="row">
                          <div class="col-md-3">
                            <div class="input-group">
                              <input type="text" class="form-control" placeholder="Name" id="inputPayName">
                            </div><!-- /input-group -->
                          </div><!-- /.col-md-6 -->
                          <div class="col-md-3">
                            <div class="input-group">
                              <input type="number" class="form-control" placeholder="Amounts" id="inputPayAmount">
                            </div><!-- /input-group -->
                          </div><!-- /.col-md-6 -->
                          <div class="col-md-6">
                            <div class="input-group">
                              <label class="radio-inline"><input type="radio" name="optPayType" value="cash" checked>Cash</label>
                              <label class="radio-inline"><input type="radio" name="optPayType" value="wechat">WeChat</label>
                              <label class="radio-inline"><input type="radio" name="optPayType" value="stripe">Credit</label>
                              <span class="input-group-btn">
                                <button class="btn btn-default" type="button" id="btnAddDonation">Add</button>
                              </span>
                            </div><!-- /input-group -->
                          </div><!-- /.col-lg-6 -->
                        </div><!-- /.row -->
                      </div>
                    </div>

                  </div>
                </div>
            </div><!-- panel -->

        </div><!-- col-md-10 -->

    </div><!-- row -->
@endsection

@section('after-scripts-end')
<script>

$('document').ready(function(){
  JSONEditor.plugins.selectize.enable = true;

  JSONEditor.defaults.options.upload = function(type, file, cbs) {

    var currentUploader = {
        uploader: null,
    };

    var uploadStacks = {};

    currentUploader.uploader = new qq.s3.FineUploaderBasic({
        debug: true,
        request: {
            endpoint: 'paris-photo.s3.amazonaws.com',
            accessKey: '{!! config('services.amazon.clientPublicKey') !!}'
        },
        signature: {
            endpoint: '{!!route('s3sign')!!}',
            version: 4
        },
        chunking: {
            concurrent: {
                enabled: true
            },
            enabled: true,
            partSize: 20971520
        },
        uploadSuccess: {
            endpoint: '{!!route('s3sign')!!}?success'
        },
        validation:{
            sizeLimit:{!! config('services.amazon.expectedMaxSize') !!}
        },
        objectProperties:{
          acl:'public-read',
          key: function(fileid){
            return 'einvite/' + qq.getUniqueId() + '.' + qq.getExtension(currentUploader.uploader.getName(fileid));
          }
        },
        callbacks: {
            onComplete: function(id,name,obj,xhr) {
                let key = currentUploader.uploader.getKey(id);
                let imgUrl = 'https://s3.amazonaws.com/paris-photo/'+key;
                cbs.success(imgUrl);
            },
            onProgress: function(id, name, current, total){
                cbs.updateProgress(current / total);
            },
            onSubmit: function(id,name){
              currentUploader.uploader.setUploadSuccessParams({tid:'einvite', jwtToken:'{!! $jwtToken !!}'},id);
            }
        }
    });

    currentUploader.uploader.addFiles(file);
    currentUploader.uploader.uploadStoredFiles();
  };

  var schemaStyle1 = @include('frontend.einvites.schemas.style1')

  var defaultOptions = JSON.parse({!! $einvite->options !!})
  console.log(defaultOptions)

  var editor = new JSONEditor(document.getElementById('editor_holder'), {
    schema: schemaStyle1,
    no_additional_properties: true,
    disable_properties : true,
    //disable_edit_json: true,
    iconlib: "bootstrap3",
    theme: 'bootstrap3',
    startval: defaultOptions,
    required_by_default: true
  });

  editor.on('change',function() {
    $('#optionsJSON').val(JSON.stringify(editor.getValue()));
  });

  var logs = {!! $einvite->logs !!}
  console.log('logs', logs);

  updateLogs(logs);

  $('#btnAddMessage').click(function(){
    $.post('{!! route('api.storeeinvitelog') !!}', {
        token: '{!! $jwtToken !!}',
        time: $('#inputTime').val(),
        message: $('#inputMessage').val(),
        type: 'system'
      }, function(res){
        console.log(res)
        updateLogs(res.data);
      })
      .fail(function(data) {
        console.log( "error" );
    });
  })

  function updateLogs(data) {
    $('#tableLogs').html('');
    data.forEach(function(log){
      $('#tableLogs').append('<tr><td>'+log.time+'</td><td>'+log.message+'</td></tr>')
    })
  }


  var donations = {!! $einvite->donations !!}
  console.log('donations', donations);
  updateDonations(donations)

  $('#btnAddDonation').click(function(){
    $.post('{!! route('api.storedonations') !!}', {
        token: '{!! $jwtToken !!}',
        name: $('#inputPayName').val(),
        amount: $('#inputPayAmount').val(),
        type: $("input[name=optPayType]:checked").val()
      }, function(res){
        console.log(res)
        updateDonations(res.data);
      })
      .fail(function(data) {
        console.log( "error" );
    });
  })

  function updateDonations(data) {
    $('#tableDonations').html('');
    data.forEach(function(donation){
      $('#tableDonations').append('<tr><td>'+donation.name+'</td><td>'+donation.amount+'</td><td>'+donation.type+'</td><td>'+donation.token+'</td></tr>')
    })
  }
});

</script>
@endsection