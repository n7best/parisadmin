@extends('frontend.layouts.master')

@section('content')
    <div class="row">

        <div class="col-md-10 col-md-offset-1">

            <div class="panel panel-default">
                <div class="panel-heading"><i class="fa fa-home"></i> EInvites</div>
                <table class="table table-responsive">
                    <thead>
                      <th>Id</th>
                      <th>Start At</th>
                      <th width="50px">Action</th>
                    </thead>
                    <tbody>
                    @foreach($einvites as $einvite)
                        <tr>
                            <td>{!! $einvite->id !!}</td>
                            <td>{!! $einvite->created_at !!}</td>
                            <td>
                              <a href="{!! route('einvite.edit', [$einvite->id]) !!}"><i class="glyphicon glyphicon-edit"></i></a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <div class="panel-body">
                  {{-- @permission('view-hr') --}}
                  <a href="{!! route('einvite.create') !!}" class="btn btn-success">Create New EInvite</a>
                  {{-- @endauth --}}
                </div>
            </div><!-- panel -->

        </div><!-- col-md-10 -->

    </div><!-- row -->
@endsection

@section('after-scripts-end')
<script>



</script>
@endsection