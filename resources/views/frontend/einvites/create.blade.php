@extends('frontend.layouts.master')

@section('content')
    <div class="row">

        <div class="col-md-10 col-md-offset-1">

            <div class="panel panel-default">
                <div class="panel-heading"><i class="fa fa-home"></i> EInvite</div>

                <div class="panel-body">
                  <form method="POST" action="{!! route('einvite.store') !!}">
                  {!!csrf_field()!!}
                  <div class="form-group col-sm-12">
                      <style>
                      #editor_holder h3{
                        font-size: 20px;
                      }
                      </style>
                      <input type="hidden" name="options" id="optionsJSON" />
                      <div id="editor_holder"></div>
                  </div>

                  <div class="form-group col-sm-12">
                      {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
                      <a href="{!! route('einvite.index') !!}" class="btn btn-default">Cancel</a>
                  </div>
                  </form>
                </div>
            </div><!-- panel -->

        </div><!-- col-md-10 -->

    </div><!-- row -->
@endsection

@section('after-scripts-end')
<script>

$('document').ready(function(){
  JSONEditor.plugins.selectize.enable = true;

  JSONEditor.defaults.options.upload = function(type, file, cbs) {

    var currentUploader = {
        uploader: null,
    };

    var uploadStacks = {};

    currentUploader.uploader = new qq.s3.FineUploaderBasic({
        debug: true,
        request: {
            endpoint: 'paris-photo.s3.amazonaws.com',
            accessKey: '{!! config('services.amazon.clientPublicKey') !!}'
        },
        signature: {
            endpoint: '{!!route('s3sign')!!}',
            version: 4
        },
        chunking: {
            concurrent: {
                enabled: true
            },
            enabled: true,
            partSize: 20971520
        },
        uploadSuccess: {
            endpoint: '{!!route('s3sign')!!}?success'
        },
        validation:{
            sizeLimit:{!! config('services.amazon.expectedMaxSize') !!}
        },
        objectProperties:{
          acl:'public-read',
          key: function(fileid){
            return 'einvite/' + qq.getUniqueId() + '.' + qq.getExtension(currentUploader.uploader.getName(fileid));
          }
        },
        callbacks: {
            onComplete: function(id,name,obj,xhr) {
                let key = currentUploader.uploader.getKey(id);
                let imgUrl = 'https://s3.amazonaws.com/paris-photo/'+key;
                cbs.success(imgUrl);
            },
            onProgress: function(id, name, current, total){
                cbs.updateProgress(current / total);
            },
            onSubmit: function(id,name){
              currentUploader.uploader.setUploadSuccessParams({tid:'einvite', jwtToken:'{!! $jwtToken !!}'},id);
            }
        }
    });

    currentUploader.uploader.addFiles(file);
    currentUploader.uploader.uploadStoredFiles();
  };

  var schemaStyle1 = @include('frontend.einvites.schemas.style1')

  var editor = new JSONEditor(document.getElementById('editor_holder'), {
    schema: schemaStyle1,
    no_additional_properties: true,
    disable_properties : true,
    //disable_edit_json: true,
    iconlib: "bootstrap3",
    theme: 'bootstrap3'
  });

  editor.on('change',function() {
    $('#optionsJSON').val(JSON.stringify(editor.getValue()));
  });

});

</script>
@endsection