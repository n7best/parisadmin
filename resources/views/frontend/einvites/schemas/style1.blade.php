{
  "title": "EInvite",
  "type": "object",
  properties: {
    infos: {
      title: 'Basic Info',
      type: 'object',
      format: "grid",
      properties: {
        groom: {
          title: 'Groom',
          type: 'string',
          default: 'Wai'
        },
        bride: {
          title: 'Bride',
          type: 'string',
          default: 'Fiona'
        },
        weddingdate: {
          title: 'Wedding Datetime',
          type: 'string',
          format: 'datetime-local',
          default: '2016-08-21T19:00'
        }
      }
    },
    place: {
      title: 'Place',
      type: 'object',
      format: 'grid',
      options: {
        collapsed: true
      },
      properties: {
        name: {
          type: 'string',
          default: '东王朝',
          options: {
            grid_columns: 6
          }
        },
        addr: {
          title: 'Address',
          type: 'string',
          default: '42-1 Main St, New York, 11355',
          options: {
            grid_columns: 6
          }
        },
        longitude: {
          type: 'number',
          default: -73.828187,
          links: [
              {
                "rel": "Please use http://www.latlong.net/",
                "href": "http://www.latlong.net/",
              }
          ],
        },
        latitude: {
          type: 'number',
          default: 40.755247
        }
      }
    },
    slideshow: {
      title: 'Gallery',
      type: 'array',
      format: 'table',
      options: {
        collapsed: true
      },
      items: {
        type: 'object',
        title: 'Image',
        properties: {
          url: {
            title: 'Image',
            type: "string",
            format: "url",
            options: {
              upload: true
            },
            "links": [
              {
                  "href": "@{{self}}"
              }
            ]
          },
        }
      }
    },
    payment: {
      title: 'Payment',
      type: 'object',
      format: 'table',
      options: {
        collapsed: true
      },
      properties: {
        wepayUrl: {
          title: 'WeChat QRCode Image',
          type: "string",
          format: "url",
          default: "https://s3.amazonaws.com/paris-photo/einvite/fa10bc80-3176-4c1f-9d87-75a2c067ca8f.jpg",
          options: {
            upload: true
          },
          "links": [
            {
                "href": "@{{self}}"
            }
          ]
        },
        amounts: {
          title: 'Amounts',
          type: 'array',
          format: 'table',
          options: {
            collapsed: true
          },
          default: [
            { value: 800, label: '$8' },
            { value: 8800, label: '$88' },
            { value: 16800, label: '$168' },
            { value: 88800, label: '$888' },
            { value: 168800, label: '$1688' },
          ],
          items: {
            title: 'Payment Option',
            type: 'object',
            format: 'grid',
            properties: {
              value: {
                type: 'number'
              },
              label: {
                type: 'string'
              }
            }
          }
        },
        stripeKey: {
          title: 'Payment Mode',
          type: 'string',
          enum: ["pk_test_tNiXqqbwbKbTvltBhdhZWGh2", "pk_live_15wtsI34uawgeRbkPavxCRwa"],
          default: 'pk_test_tNiXqqbwbKbTvltBhdhZWGh2',
          options: {
            enum_titles: ["Test", "Live"]
          }
        }
      }
    },
    music: {
      'title': 'Music',
      'type': 'object',
      "id": "music",
      "format": "grid",
      options: {
        collapsed: true
      },
      properties: {
        url: {
          title: 'Song',
          type: 'string',
          enum: ["https://s3.amazonaws.com/paris-photo/einvite/bgmusic.mp3", "https://s3.amazonaws.com/paris-photo/einvite/bgmusic.mp3"],
          default: 'bgmusic',
          options: {
            enum_titles: ["Old Asia", "Old Asia2"]
          }
        },
        preview: {
          title: 'Url',
          type: 'string',
          "template": "@{{url}}",
          "watch": {
            "url": "music.url",
          },
          links: [
            {
              "href": "@{{url}}",
              "mediaType": "audio/mpeg3"
            }
          ]
        }
      }
    },
    decorations: {
      'title': 'Decorations',
      'type': 'object',
      "id": "decorations",
      "format": "grid",
      options: {
        collapsed: true
      },
      properties: {
        logo: {
          title: 'Logo',
          type: "string",
          format: "url",
          default: 'https://s3.amazonaws.com/paris-photo/einvite/7aa9845a-6e7f-4b7c-b1b5-d95b2d2953c7.png',
          options: {
            upload: true
          },
          "links": [
            {
                "href": "@{{self}}"
            }
          ]
        },
        page1bg: {
            title: 'Home Background',
            type: "string",
            format: "url",
            default: 'https://s3.amazonaws.com/paris-photo/einvite/36e74db7-11de-425c-8d74-2373f48f7676.jpg',
            options: {
              upload: true
            },
            "links": [
              {
                  "href": "@{{self}}"
              }
            ]
        },
        letterWord: {
            title: 'Letter left Image',
            type: "string",
            format: "url",
            default: 'https://s3.amazonaws.com/paris-photo/einvite/cad68f27-4f1c-4159-a785-99653fa8b139.png',
            options: {
              upload: true
            },
            "links": [
              {
                  "href": "@{{self}}"
              }
            ]
        },
        letterInvite: {
            title: 'Letter Right Image',
            type: "string",
            format: "url",
            default:'https://s3.amazonaws.com/paris-photo/einvite/20356248-04ac-412c-9051-42ce422a228d.png',
            options: {
              upload: true
            },
            "links": [
              {
                  "href": "@{{self}}"
              }
            ]
        }
      }
    },
    languages: {
      'title': 'Languages',
      'type': 'object',
      "id": "decorations",
      "format": "table",
      options: {
        collapsed: true
      },
      properties: {
        loader: {
          title: 'Loader',
          type: 'object',
          properties: {
            loaderLogo: {
              title: 'Heading',
              type: 'string',
              default: '💌 读取中,请稍等... 💌'
            },
            loaderShuffle: {
              title: 'Shuffles',
              type: 'array',
              default: ['💍预算计划中', '👠策划场地中', '😉定制婚纱中', '😘准备道具中', '🎂制作蛋糕中',
               '🚗预订婚车中', '🎊正在婚礼场地布置', '🎉庆祝告别单身派对', '🎁够买礼糖中', '💎正在打开红包',
               '⛲️外景拍摄中', '🏰内景拍摄中', '📝采购新居家具中', '👸新娘式婚礼妆', '💵正在购买红包袋', '🎈吹气球中',
              ],
              options: {
                collapsed: true
              },
              items: {
                title: 'Text',
                type: 'string'
              }
            }
          }
        },
        welcomebox: {
          title: 'Home Page',
          type: 'object',
          properties: {
            heading: {
              type: 'string',
              default: 'Invitation'
            }
          }
        },
        menus: {
          title: 'Menus',
          type: 'object',
          properties: {
            music: {
              type: 'string',
              default: '音乐'
            },
            contact: {
              type: 'string',
              default: '联系'
            },
            place: {
              type: 'string',
              default: '地点'
            },
            bless: {
              type: 'string',
              default: '祝福'
            },
            home: {
              type: 'string',
              default: '主页'
            },
            letter: {
              type: 'string',
              default: '邀请信'
            },
            gallery: {
              type: 'string',
              default: '相册'
            },
            blessrank: {
              type: 'string',
              default: '排行'
            },
            likes: {
              type: 'string',
              default: '点赞'
            },
          }
        },
        checkin: {
          title: 'Checkin',
          type: 'object',
          properties: {
            signIn: {
              type: 'string',
              default: '👉 点击签到 👈'
            },
            signInCheck: {
              type: 'string',
              default: '👌 谢谢您的签到！'
            },
            userCheckIn: {
              type: 'string',
              default: '点击了签到'
            },
          }
        },
        gift: {
          title: 'Gift',
          type: 'object',
          properties: {
            buttonLabel: {
              type: 'string',
              default: '给予祝福'
            },
            description: {
              type: 'string',
              default: 'Wedding Gift Bless'
            },
          }
        }
      }
    }
  }
}