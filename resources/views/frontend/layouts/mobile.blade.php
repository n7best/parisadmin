<!DOCTYPE html>
<html>
  <head>
    <title>@yield('title', app_name())</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <meta name="csrf_token" content="{!! csrf_token() !!}">
    @yield('meta')

     <!-- Styles -->
    @yield('before-styles-end')
    {!! Html::style(elixir('css/mobile.css')) !!}
    @yield('after-styles-end')

    <link rel="stylesheet" href="https://cdn.linearicons.com/free/1.0.0/icon-font.min.css">
  </head>

  <body ontouchstart>
    @yield('content')

    <!-- JavaScripts -->
    <!-- HTTPS required. HTTP will give a 403 forbidden response -->
    <script src="https://sdk.accountkit.com/en_US/sdk.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="{{asset('js/vendor/jquery/jquery-2.1.4.min.js')}}"><\/script>')</script>
    {!! Html::script('js/vendor/moment/moment.min.js') !!}
    @yield('before-scripts-end')
    {!! Html::script(elixir('js/mobile.js')) !!}
    @yield('after-scripts-end')
  </body>

</html>
