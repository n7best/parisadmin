<!DOCTYPE html>
<html >
  <head>
    <meta charset="UTF-8">
    <title>Pariswedding</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <style>
      html,body,div,span,applet,object,iframe,h1,h2,h3,h4,h5,h6,p,blockquote,pre,a,abbr,acronym,address,big,cite,code,del,dfn,em,img,ins,kbd,q,s,samp,small,strike,strong,sub,sup,tt,var,b,u,i,center,dl,dt,dd,ol,ul,li,fieldset,form,label,legend,table,caption,tbody,tfoot,thead,tr,th,td,article,aside,canvas,details,embed,figure,figcaption,footer,header,hgroup,menu,nav,output,ruby,section,summary,time,mark,audio,video{margin:0;padding:0;border:0;font-size:100%;font:inherit;vertical-align:baseline}article,aside,details,figcaption,figure,footer,header,hgroup,menu,nav,section{display:block}body{line-height:1}ol,ul{list-style:none}blockquote,q{quotes:none}blockquote:before,blockquote:after,q:before,q:after{content:'';content:none}table{border-collapse:collapse;border-spacing:0}
    </style>
    <link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css'>
    <link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/lightgallery/1.2.21/css/lightgallery.css'>
    <link href="https://maxcdn.bootstrapcdn.com/bootswatch/3.3.7/lumen/bootstrap.min.css" rel="stylesheet" integrity="sha384-gv0oNvwnqzF6ULI9TVsSmnULNb3zasNysvWwfT/s4l8k5I+g6oFz9dye0wg3rQ2Q" crossorigin="anonymous">

    <style>
      /* Hiding the checkbox, but allowing it to be focused */
      .badgebox
      {
          opacity: 0;
      }

      .badgebox + .badge
      {
          /* Move the check mark away when unchecked */
          text-indent: -999999px;
          /* Makes the badge's width stay the same checked and unchecked */
        width: 27px;
      }

      .badgebox:focus + .badge
      {
          /* Set something to make the badge looks focused */
          /* This really depends on the application, in my case it was: */
          /* Adding a light border */
          box-shadow: inset 0px 0px 5px;
          /* Taking the difference out of the padding */
      }

      .badgebox:checked + .badge
      {
          /* Move the check mark back when checked */
        text-indent: 0;
      }

      .selectedphoto
      {
        cursor: pointer;
      }
    </style>
  </head>

  <body>

    <div class="bs-example bs-navbar-top-example">
  <nav role="navigation" class="navbar navbar-default navbar-fixed-top">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <a href="#" class="navbar-brand">Paris Wedding</a>
    </div>

  </nav>
</div>

<div class="container" style="margin-top: 75px;">
  <div class="alert alert-info alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    @if($selected)
    <strong>Thanks</strong> You have already selected the photo
    @else
    <strong>Welcome</strong> Please reviews the following photo and select the best ones you like
    @endif
  </div>
  <div class="panel panel-default">
    <div class="panel-heading clearfix">Photos - Max Selected Amount: {!! $ps->max_allow !!}</div>

    <div class="panel-body">
      <div id="aniimated-thumbnials">
      </div>
    </div>
  </div>

  <div class="panel panel-default">
    <div class="panel-heading clearfix">Selected <span id="selectCount">@if($selected) {{ count($selected) }} @else 0 @endif</span> / {!! $ps->max_allow !!}</div>

    <div class="panel-body" >
      <div id="psc">
      @if(!$selected)
      <h4>No Photo Selected yet</h4>
      @endif
      </div>
      <hr/>
      @if(!$selected)
      <p id="tip">Click Photo to add comment quickly</p>
      @endif
      <div class="form-group" id="seccomment">
        <label for="comment">Comment:</label>
        @if($selected)
        <pre>{!!  $ps->remark !!}</pre>
        @else
        <textarea class="form-control" rows="5" id="comment"></textarea>
        @endif
      </div>
    </div>

    <div class="panel-footer" id="submitInfo">
      @if(!$selected)
      <button type="button" class="btn btn-info" id="genPs">Submit Selected</button>
      @endif
    </div>
  </div>
</div>


<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="{{asset('js/vendor/jquery/jquery-2.1.4.min.js')}}"><\/script>')</script>
{!! Html::script('js/vendor/bootstrap/bootstrap.min.js') !!}
<script src='https://cdnjs.cloudflare.com/ajax/libs/fastclick/1.0.6/fastclick.js'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/lightgallery/1.2.21/js/lightgallery.js'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/lightgallery/1.2.21/js/lg-zoom.js'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/lightgallery/1.2.21/js/lg-thumbnail.js'></script>

  <script>
  var initSelected = {!! $selected ? 'true' : 'false' !!}
  var photos = $.parseJSON('{!! json_encode($photos) !!}');
  var selected = initSelected ? $.parseJSON('{!! json_encode($selected) !!}') : []

  function updateSelected(){
    $cs = $('#psc');
    $('#selectCount').html(selected.length);
    if(selected.length == 0) return $cs.html('<h4>No Photo Selected</h4>')

    if(initSelected){
      $.each(photos, function(i, pi){
        if($.inArray(pi.id.toString(), selected) > -1){
          var $item = $('<img src="'+ pi.thumb + '" class="selectedphoto" data-id="'+pi.id+'"/>');
          $cs.append($item)
        }
      })

      return;
    }else{
      $cs.html('');
      $.each(selected, function(i, pi){
        var photo = photos[pi.aid];
        var $item = $('<img src="'+ photo.thumb + '" class="selectedphoto" data-id="'+pi.id+'"/>');
        $cs.append($item)
      })

      $('.selectedphoto').click(function(){
        var pt = $(this)
        //console.log('ps click ', pt.data('id'))
        $('#comment').val(  $('#comment').val() + '\nPhoto ID ' + pt.data('id') + ': ')
        $('#comment').focus()
      })
    }
  }
  function isSelected(id){
    var founded = false;
    $.each(selected, function(i, pi){
      //console.log('checking', pi, id, pi.id == id);
      if(pi.id == id) founded = true;
    })
    return founded;
  }
  function toogleSelect(item){
   //console.log($btn)
    if(selected.length == 0){
      selected.push(item);
    }else{
      var removei = -1;
      $.each(selected, function(i, s){
        //onsole.log('checking', s, item.id, s.id == item.id);
        if(s.id == item.id){
          removei = i;
        }
      })


      if(removei > -1){
        //console.log('removing', removei)
        selected.splice(removei, 1);
      }else{
        if(selected.length == {!! $ps->max_allow !!}){
          return alert('Max Photo Selected Reached')
        }
        selected.push(item);
      }
    }
    updateSelected();
  }

  
  $(document).ready(function () {

      if(!initSelected){
        $('#genPs').click(function(){
          $btn = $(this);
          $btn.button('loading');
          var data = {
            selected: selected,
            comment: $('#comment').val()
          }

          $.post('{!! route('psupdateapi', $id)!!}', data, function(data){
            $('#tip').hide();
            $('#comment').hide();
            $('#seccomment').append('<pre>'+ data.remark +'</pre>')
            $btn.hide();
            $('#submitInfo').append('<p> You have submited your choice. </p>')
          }).fail(function(err){
            console.log(err);
            alert(err.responseJSON.msg);
          }).always(function(){
            $btn.button('reset');
          })
        })
      }
      updateSelected();
      console.log(photos, selected);

      if(photos.length == 0){
        $('#aniimated-thumbnials').html('<h4>No photos</h4>');
      }else{
        $.each(photos,function(i,photo){
          var subhtml = initSelected ? '<b>Photo ID: '+ photo.id +' </b>' : '<b>Photo ID: '+ photo.id +' </b> <label for="info" class="btn btn-info">Select <input type="checkbox" id="info" class="badgebox btnselect" data-id="'+photo.id+'" data-aid="'+i+'"><span class="badge">&check;</span></label>'
          $('#aniimated-thumbnials').append('<a href="'+photo.src+'" data-sub-html=\''+subhtml+'\'"><img src="'+ photo.thumb +'"/></a>')
        })
      }

      $('#aniimated-thumbnials').lightGallery({
          thumbnail: true,
          download: false
      });
      $('#aniimated-thumbnials').on('onAfterAppendSubHtml.lg', function(e){
        if(!initSelected){
          var bid = $('.btnselect').data('id');
          //console.log(bid, isSelected(bid))
          if(isSelected(bid)){
            $('.btnselect').prop('checked',true);
          }
          $('.btnselect').click(function(e){
            $btn = $(this);
            //e.preventDefault()
            //console.log($btn.data('id'), $btn.data('aid'), photos[$btn.data('aid')])
            toogleSelect({
              id: $btn.data('id'),
              aid: $btn.data('aid')
            })

            if(isSelected($btn.data('id'))){
              $btn.prop('checked', true)
            }else{
              $btn.prop('checked', false)
            }

            console.log(selected)
          })
        }
      })
  });
  </script>
  </body>
</html>
