<style>

p,table,div {
  font-family: sans-serif;
  color:black;
  padding: 0;
}

p:empty{
  display: none;
}

h1,h2,h3,h4 {
  font-family: cursive;
  color: black;
  width: 100%;
  padding: 0px;
}

h1 {
  font-family: cursive;
  font-size: 10px;
  text-transform: uppercase;
  text-align: center;
  padding: 0;
  line-height: 10px;
  color: #676767 !important;
  margin: 1px;
}

h2 {
  font-size: 10px;
  background: #eee;
  margin: 0;
}

table{
  margin: 0;

}

table p {
  font-size: 9px;
  padding: 0px;
  margin: 0px;
}

table > tbody > tr > td {
  font-size: 10px;
  border-bottom: 1px solid #9C9C9C !important;
  margin-bottom: 0px;
}
table > tbody > tr, table > tbody > tr > td{
  padding:0;margin:0;height: 10px;
}

h5 { page-break-after: always; }

thead:before, thead:after { display: none; }
tbody:before, tbody:after { display: none; }
</style>