<?php

namespace App\Listeners;

use App\Events\InvoiceStatusChanged;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class InvoiceReminderNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  InvoiceStatusChanged  $event
     * @return void
     */
    public function handle(InvoiceStatusChanged $event)
    {
        //
    }
}
