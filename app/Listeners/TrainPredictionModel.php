<?php

namespace App\Listeners;

use App\Events\NewIntelReceived;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class TrainPredictionModel
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  NewIntelReceived  $event
     * @return void
     */
    public function handle(NewIntelReceived $event)
    {
        //
    }
}
