<?php

namespace App\Listeners;

use App\Events\NewOrderCreated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Twilio;
use Mail;
use App\Models\Template;


class SendWelcomeNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  NewOrderCreated  $event
     * @return void
     */
    public function handle(NewOrderCreated $event)
    {
        $order = $event->order;
        $customer = $order->customer;

        if($customer->getMeta('notification', 'true') == 'true'){

            $message = strip_tags(Template::render('welcome_msg', ['customer' => $customer]));

            $this->sendSMS($customer, $message);
        }
    }

    public function sendSMS($customer, $msg){
        $bridePhone = $customer->getMeta('bridePhone', false);
        $groomPhone = $customer->getMeta('groomPhone', false);

        if($bridePhone || $groomPhone){
            if(!empty($bridePhone)){
                Twilio::message( $bridePhone, $msg);
            }elseif(!empty($groomPhone)){
                Twilio::message( $groomPhone, $msg);
            }
        }

    }
}
