<?php

namespace App\Listeners;

use App\Events\PhotoSelected;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Mail;
class RemindSalesPhotoSelection
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  PhotoSelected  $event
     * @return void
     */
    public function handle(PhotoSelected $event)
    {
        if($event->ogstatus == 'created' || $event->ogstatus == 'viewed'){
            if($event->ps->status == 'selected'){
                $code = strtolower($event->ps->company->code);
                $email = 'sales'.$code.'@pariswedding.nyc';
                //$email = 'n7best@gmail.com';

                if($email && !empty($email)){
                    Mail::send('emails.photoselect', ['gallery' => $event->ps->gallery], function ($m) use ($code, $email) {
                        $m->from('admin@pariswedding.nyc', 'ParisWedding');

                        $m->to($email, 'Sales')->subject('['.$code.'] New Photo Selected.');
                    });
                }
            }
        }
    }
}
