<?php

namespace App\Listeners;

use App\Events\TaskStatusChanged;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Twilio;
use Mail;

class TaskReminderNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  TaskStatusChanged  $event
     * @return void
     */
    public function handle(TaskStatusChanged $event)
    {
        $task = $event->task;
        $statusBefore = $event->statusBeforeChange;
        $order = $task->order;
        $customer = $order->customer;

        if($statusBefore == 'submited' && $task->status == 'approved' && $customer->getMeta('notification', 'true') == 'true') {
            $this->sendSMS($customer, 'ParisWedding: You order is updated and require you action, please check your order.');
            $this->sendEmail($customer, 'You order is updated and require you action, please check your order.', $task);
        }

    }

    public function sendEmail($customer, $msg, $task){
        $email = $customer->getMeta('email', false);

        if($email && !empty($email)){
            Mail::send('emails.taskreminder', ['task' => $task], function ($m) use ($customer, $email) {
                $m->from('admin@pariswedding.nyc', 'ParisWedding');

                $m->to($email, $customer->display_name)->subject('Your Reminder!');
            });
        }
    }

    public function sendSMS($customer, $msg){
        $bridePhone = $customer->getMeta('bridePhone', false);
        $groomPhone = $customer->getMeta('groomPhone', false);

        if($bridePhone || $groomPhone){
            if(!empty($bridePhone)){
                Twilio::message( $bridePhone, $msg);
            }elseif(!empty($groomPhone)){
                Twilio::message( $groomPhone, $msg);
            }
        }

    }
}
