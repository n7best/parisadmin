<?php

namespace App\Listeners;

use App\Events\NewRaffleCreated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\Customers\Customer;
use App\Models\Orders\Coupon;
use Twilio;
use Mail;
use GuzzleHttp\Client;
use Carbon\Carbon;

class CreateAndSendCouponCode
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  NewRaffleCreated  $event
     * @return void
     */
    public function handle(NewRaffleCreated $event)
    {
        $customer = Customer::where('providers','=',$event->uuid)->first();
        $notified = false;

        if($customer){
            $prize = $event->prize['coupon'];
            $coupon = new Coupon();
            $coupon->type = $prize['type'];
            $coupon->value = $prize['value'];
            $coupon->requirements = $prize['requirements'];
            $coupon->user_id = $customer->id;
            $coupon->code = Coupon::genCode();
            $coupon->save();
            $msg = '[ParisWedding] Congrats, You won '.$event->prize['name'].'! Coupon Code: '.$coupon->code.'.';

            /*if($customer->getMeta('email', false)) {
                $notified = true;
                $this->sendEmail($customer, $msg);
            }*/

            if(!$notified && $customer->getMeta('groomPhone', false)) {
                $this->sendSMS($customer, $msg);
                //$this->updateBot($customer, $event->prize['name']);
            }
        }
    }

    public function updateBot($customer, $prize){
        $client = new Client();

        $msg = '[抽奖] '.Carbon::now()."\n".'客户:'.$customer->groomname."\n".'电话:'.$customer->getMeta('groomPhone', false)."\n奖品:".$prize;

        $response = $client->post('http://dl.pariswedding.nyc:8888/log', [
            'json' => ['msg' => $msg]
        ]);
    }

    public function sendEmail($customer, $msg){
        $email = $customer->getMeta('email', false);

        if($email && !empty($email)){
            Mail::raw($msg, function ($m) use ($customer, $email) {
                $m->from('admin@pariswedding.nyc', 'ParisWedding');
                $m->to($email, $customer->display_name)->subject('Your Raffle Wining!');
            });
        }
    }

    public function sendSMS($customer, $msg){
        $bridePhone = $customer->getMeta('bridePhone', false);
        $groomPhone = $customer->getMeta('groomPhone', false);

        if($bridePhone || $groomPhone){
            try {
                if(!empty($bridePhone)){
                    Twilio::message( $bridePhone, $msg);
                }elseif(!empty($groomPhone)){
                    Twilio::message( $groomPhone, $msg);
                }
            } catch (Exception $e) {
                //ignore
            }

        }

    }
}
