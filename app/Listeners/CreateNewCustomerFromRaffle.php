<?php

namespace App\Listeners;

use App\Events\NewRaffleCreated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\Customers\Customer;

class CreateNewCustomerFromRaffle
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  NewRaffleCreated  $event
     * @return void
     */
    public function handle(NewRaffleCreated $event)
    {
        $info = $event->userinfo;

        if(Customer::customerExists($event->uuid)){
            $customer = Customer::where('providers','=', $event->uuid)->first();
            $customer->groomname = $info['name'];
            $customer->providers = $event->uuid;
            $customer->update();
        }else{
            $customer = new Customer();
            $customer->groomname = $info['name'];
            $customer->providers = $event->uuid;
            $customer->save();
        }

        if(!empty($info['phone'])) $customer->updateMeta('groomPhone', $info['phone']);
        if(!empty($info['email'])) $customer->updateMeta('email', $info['email']);
    }
}
