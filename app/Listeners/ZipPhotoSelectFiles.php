<?php

namespace App\Listeners;

use App\Events\PhotoSelected;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Aws\Sqs\SqsClient;
use Aws\Credentials\Credentials;
use App\Models\Media\Zip;
use App\Models\Media\File;
use Vinkla\Hashids\Facades\Hashids;
class ZipPhotoSelectFiles
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  PhotoSelected  $event
     * @return void
     */
    public function handle(PhotoSelected $event)
    {
        if($event->ogstatus == 'created' || $event->ogstatus == 'viewed'){
            if($event->ps->status == 'selected'){
                $code = strtolower($event->ps->company->code);
                $email = 'sales'.$code.'@pariswedding.nyc';

                $ps = $event->ps;

                $zip = new Zip();
                $zip->from = get_class($ps);
                $zip->from_id = $ps->id;
                $zip->status = 'created';
                $zip->save();

                $zip->hash = Hashids::encode($zip->id);
                $zip->update();


                $credentials = new Credentials(config('services.amazon.serverPublicKey'), config('services.amazon.serverPrivateKey'));
                // Instantiate the SQS client with your AWS credentials
                $client = SqsClient::factory(array(
                    'credentials' => $credentials,
                    'region'  => 'us-east-1',
                    'version' => '2012-11-05'
                ));

                $result = $client->createQueue(array('QueueName' => 'zips'));
                $queueUrl = $result->get('QueueUrl');

                $data = [
                    'hash' => $zip->hash,
                    'zippable' => $ps->zippable(),
                    'apiUrl'  => route('zips.update', $zip->hash),
                    'user_id' => $ps->user_id
                ];

                $client->sendMessage(array(
                    'QueueUrl'    => $queueUrl,
                    'MessageBody' => json_encode($data),
                ));
            }
        }
    }
}
