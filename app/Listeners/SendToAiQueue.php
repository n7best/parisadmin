<?php

namespace App\Listeners;

use App\Events\NewPhotoCreate;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Aws\Sqs\SqsClient;
use Aws\Credentials\Credentials;

class SendToAiQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  NewPhotoCreate  $event
     * @return void
     */
    public function handle(NewPhotoCreate $event)
    {
        $photo = $event->photo;
        $credentials = new Credentials('AKIAJOVYQDQQVC3HTIAA', '2tBUl82DJcP5rHZk+grHmBeo47RKfb4A4JXIDp/l');
        // Instantiate the SQS client with your AWS credentials
        $client = SqsClient::factory(array(
            'credentials' => $credentials,
            'region'  => 'us-east-1',
            'version' => '2012-11-05'
        ));

        $result = $client->createQueue(array('QueueName' => 'ai-photo-queue'));
        $queueUrl = $result->get('QueueUrl');

        $data = [
            'pid' => $photo->id,
            'url' => $photo->forAI(),
            'apiUrl'  => route('photodetail.update', $photo->id),
        ];

        $client->sendMessage(array(
            'QueueUrl'    => $queueUrl,
            'MessageBody' => json_encode($data)
        ));
    }
}
