<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Google;
use PulkitJalan\Google\Client;
use App\Models\Tasks\Task;
use App\Models\Media\File;
use DB;
class checkGoogleDrive extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'checks:google';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    protected $client;
    protected $drive;
    protected $rootFid = '0B-LEReuskO1Aa0tfeVFfdjZadEE';

    protected $folders = [];
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->client = new Client(config('google'));
        $this->drive = $this->client->make('drive');
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $orderList = $this->listFolder($this->rootFid);
        $folders = [];


        $unfinishFiles = File::whereNull('gdkey')->get();

        foreach ($unfinishFiles as $file) {
            $task = $file->task;
            $rid = $task->order_rid;
            $tid = $file->task_id;
            $filename = basename($file->s3key);

            $this->info('Locating '.$filename.' rid: '.$rid.' tid: '.$tid);
            $orderFolder = $this->inFolder($rid,$orderList);

            if($orderFolder){
                $this->info('Folder: '.$orderFolder['title'].' id: '.$orderFolder['id']);
                $taskList = $this->listFolder($orderFolder['id']);
                $taskFolder = $this->inFolder($tid,$taskList);
                if($taskFolder){
                    $this->info('Folder: '.$taskFolder['title'].' id: '.$taskFolder['id']);
                    $photoList = $this->listFolder($taskFolder['id']);
                    $photo = $this->inFolder($filename,$photoList);
                    if($photo){
                        $this->info('Photo: '.$photo['title'].' id: '.$photo['id'].' --- file id: '.$file->id);
                        $file->gdkey = $photo['id'];
                        $file->save();
                    }
                }
            }
        }
    }

    public function inFolder($name,$list){
        foreach ($list as $listfile) {
            if($listfile['title'] == $name) return $listfile;
        }
        return false;
    }

    public function listFolder($fid){

        if(isset($this->folders[$fid])){
            return $this->folders[$fid];
        }else{
            $this->folders[$fid] = $this->printFilesInFolder($fid);
            return $this->folders[$fid];
        }
    }

    public function printFilesInFolder($fid) {
      $pageToken = NULL;
      $files = [];

      do {
        try {
          $parameters = array();
          if ($pageToken) {
            $parameters['pageToken'] = $pageToken;
          }
          $children = $this->drive->children->listChildren($fid, $parameters);
          $this->info('Folder: '.$fid);
          foreach ($children->getItems() as $child) {
            $file = $this->drive->files->get($child->getId());
            $files[] = [
                'id' => $file->id,
                'title' => $file->title,
                'type' => $file->mimeType
            ];
            $this->info('File: '.$file->title);
          }
          $pageToken = $children->getNextPageToken();
        } catch (Exception $e) {
          print "An error occurred: " . $e->getMessage();
          $pageToken = NULL;
        }
      } while ($pageToken);

      return $files;
    }
}
