<?php

namespace App\Providers;

use Illuminate\Contracts\Events\Dispatcher as DispatcherContract;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        /**
         * Frontend Events
         */
        'App\Events\InvoiceStatusChanged' => [
            'App\Listeners\InvoiceReminderNotification',
        ],

        'App\Events\TaskStatusChanged' => [
            'App\Listeners\TaskReminderNotification',
        ],

        'App\Events\NewOrderCreated' => [
            'App\Listeners\SendWelcomeNotification',
        ],

        'App\Events\NewRaffleCreated' => [
            'App\Listeners\CreateNewCustomerFromRaffle',
            'App\Listeners\CreateAndSendCouponCode',
        ],

        'App\Events\PhotoSelected' => [
            'App\Listeners\ZipPhotoSelectFiles',
            'App\Listeners\RemindSalesPhotoSelection',
        ],

        'App\Events\NewPhotoCreate' => [
            'App\Listeners\SendToAiQueue',
        ],

        'App\Events\NewIntelReceived' => [
            'App\Listeners\TrainPredictionModel',
        ],
        /**
         * Authentication Events
         */
        \App\Events\Frontend\Auth\UserLoggedIn::class  => [
            \App\Listeners\Frontend\Auth\UserLoggedInListener::class,
        ],
        \App\Events\Frontend\Auth\UserLoggedOut::class => [
            \App\Listeners\Frontend\Auth\UserLoggedOutListener::class,
        ],
        \App\Events\Frontend\Auth\UserRegistered::class => [
            \App\Listeners\Frontend\Auth\UserRegisteredListener::class,
        ],
    ];

    /**
     * Register any other events for your application.
     *
     * @param  \Illuminate\Contracts\Events\Dispatcher  $events
     * @return void
     */
    public function boot(DispatcherContract $events)
    {
        parent::boot($events);

        //
    }
}
