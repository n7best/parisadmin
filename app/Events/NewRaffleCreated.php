<?php

namespace App\Events;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class NewRaffleCreated extends Event
{
    use SerializesModels;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($prize, $uuid, $userinfo = []/*, $wc_user = []*/)
    {
        $this->prize = $prize;
        $this->uuid = $uuid;
        $this->userinfo = $userinfo;
        //$this->wc_user = $wc_user;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
