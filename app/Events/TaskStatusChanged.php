<?php

namespace App\Events;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use App\Models\Tasks\Task;

class TaskStatusChanged extends Event
{
    use SerializesModels;

    public $task;
    public $statusBeforeChange;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Task $task, $statusBeforeChange)
    {
        $this->task = $task;
        $this->statusBeforeChange = $statusBeforeChange;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
