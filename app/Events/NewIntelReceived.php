<?php

namespace App\Events;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class NewIntelReceived extends Event
{
    use SerializesModels;

    public static $TYPE_PHOTO = 0;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($type, $details)
    {
        $this->type = $type;
        $this->details = $details;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
