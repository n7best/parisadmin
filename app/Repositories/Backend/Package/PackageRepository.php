<?php

namespace App\Repositories\Backend\Package;

use App\Models\Package\Package;
use InfyOm\Generator\Common\BaseRepository;

class PackageRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Package::class;
    }
}
