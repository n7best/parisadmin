<?php

namespace App\Repositories\Backend\Coupon;

use App\Models\Orders\Coupon;
use InfyOm\Generator\Common\BaseRepository;

class CouponRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Coupon::class;
    }
}
