<?php

namespace App\Repositories\Backend\Companies;

use App\Models\Companies\Company;
use InfyOm\Generator\Common\BaseRepository;

class CompanyRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        "name",
		"phones",
		"address",
		"email"
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Company::class;
    }
}
