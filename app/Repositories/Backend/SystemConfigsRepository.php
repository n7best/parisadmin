<?php

namespace App\Repositories\Backend;

use App\Models\SystemConfigs;
use InfyOm\Generator\Common\BaseRepository;

class SystemConfigsRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return SystemConfigs::class;
    }
}
