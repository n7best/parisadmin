<?php

namespace App\Repositories\Frontend\Customers;

use App\Models\Customers\Customer;
use InfyOm\Generator\Common\BaseRepository;

class CustomersRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Customer::class;
    }
}
