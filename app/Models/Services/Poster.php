<?php

namespace App\Models\Services;


use Illuminate\Database\Eloquent\Model;
use App\Models\Services\Interfaces\ServiceInterface;

class Poster extends Model implements ServiceInterface{
  private $name = 'Poster';
  private $image, $size;

  public function __construct(){

  }

  public function getName() {
    return $this->name;
  }

  public function getPrice(){
    return 1000;
  }

  public function getMetas(){
    return [
      [
        'name' => 'Size',
        'type' => 'select',
        'options' => [
          'Signature',
          '16x20',
          '20x30',
          '30x40'
        ]
      ]
    ];
  }

  public function setMetas($data){

  }
}