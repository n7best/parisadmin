<?php

namespace App\Models\Services\Interfaces;

interface ServiceInterface{
  public function getName();
  public function getPrice();
  public function getMetas();
  public function setMetas($data);
}