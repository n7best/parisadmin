<?php

namespace App\Models\Services;


use Illuminate\Database\Eloquent\Model;


class Service_Meta extends Model{

  /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table;

  /**
   * The attributes that are not mass assignable.
   *
   * @var array
   */
  protected $guarded = ['id'];

  protected $hidden = ['id', 'created_at', 'updated_at', 'services_id'];

  /**
   *
   */
  public function __construct()
  {
      $this->table = 'services_metas';
  }

  public function service()
  {
      return $this->belongsTo('App\Models\Services\Service');
  }

  public function getOptionsAttribute($value)
  {
      return explode(',', $value);
  }

  public function getPriceFactorAttribute($value)
  {
      return explode(',', $value);
  }
}