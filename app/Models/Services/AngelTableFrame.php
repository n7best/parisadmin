<?php

namespace App\Models\Services;


use Illuminate\Database\Eloquent\Model;
use App\Models\Services\Interfaces\ServiceInterface;

class AngelTableFrame extends Model implements ServiceInterface{
  private $name = 'Angel Table Frame';
  private $image, $size;

  public function __construct(){

  }

  public function getName() {
    return $this->name;
  }

  public function getPrice(){
    return 1000;
  }

  public function getMetas(){
    return [
      [
        'name' => 'Size',
        'type' => 'select',
        'options' => [
          '8x10',
          '8x12'
        ]
      ]
    ];
  }

  public function setMetas($data){

  }
}