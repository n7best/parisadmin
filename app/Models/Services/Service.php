<?php

namespace App\Models\Services;


use Illuminate\Database\Eloquent\Model;
use App\Models\Services\Traits\Attribute\ServicesAttribute;


class Service extends Model{

  use ServicesAttribute;
  /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table;

  /**
   * The attributes that are not mass assignable.
   *
   * @var array
   */
  protected $guarded = ['id'];

  /**
   *
   */
  public function __construct()
  {
      $this->table = 'services';
  }

  public function metas()
  {
    return $this->hasMany('App\Models\Services\Service_Meta','services_id');
  }

  public static function options($id)
  {
    $service = Service::find($id);
    return $service->metas;
  }
}