<?php

namespace App\Models\Services;


use Illuminate\Database\Eloquent\Model;
use App\Models\Services\Interfaces\ServiceInterface;

class VipFrame extends Model implements ServiceInterface{
  private $name = 'Vip Frame';
  private $image, $size;

  public function __construct(){

  }

  public function getName() {
    return $this->name;
  }

  public function getPrice(){
    return 1000;
  }

  public function getMetas(){
    return [
      [
        'name' => 'Size',
        'type' => 'select',
        'options' => [
          '24x36'
        ]
      ]
    ];
  }

  public function setMetas($data){

  }
}