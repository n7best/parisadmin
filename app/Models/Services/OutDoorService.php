<?php

namespace App\Models\Services;


use Illuminate\Database\Eloquent\Model;
use App\Models\Services\Interfaces\ServiceInterface;

class OutDoorService extends Model implements ServiceInterface{
  private $name = 'Out Door Photography';
  private $image, $day, $ltouch, $dtouch;

  public function __construct(){

  }

  public function getName() {
    return $this->name;
  }

  public function getPrice(){
    return 1000;
  }

  public function getMetas(){
    return [
      [
        'name' => 'Day',
        'type' => 'integer'
      ],
      [
        'name' => 'Light Touch Up',
        'type' => 'integer'
      ],
      [
        'name' => 'Detail Touch Up',
        'type' => 'integer'
      ]
    ];
  }

  public function setMetas($data){

  }
}