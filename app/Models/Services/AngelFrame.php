<?php

namespace App\Models\Services;


use Illuminate\Database\Eloquent\Model;
use App\Models\Services\Interfaces\ServiceInterface;

class AngelFrame extends Model implements ServiceInterface{
  private $name = 'Angel Frame';
  private $image, $size;

  public function __construct(){

  }

  public function getName() {
    return $this->name;
  }

  public function getPrice(){
    return 1000;
  }

  public function getMetas(){
    return [
      [
        'name' => 'Size',
        'type' => 'select',
        'options' => [
          '16x24',
          '20x30',
          '24x36'
        ]
      ]
    ];
  }

  public function setMetas($data){

  }
}