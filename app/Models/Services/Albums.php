<?php

namespace App\Models\Services;


use Illuminate\Database\Eloquent\Model;
use App\Models\Services\Interfaces\ServiceInterface;

class Albums extends Model implements ServiceInterface{
  private $name = 'Albums';
  private $image, $size;

  public function __construct(){

  }

  public function getName() {
    return $this->name;
  }

  public function getPrice(){
    return 1000;
  }

  public function getMetas(){
    return [
      [
        'name' => 'Size',
        'type' => 'select',
        'options' => [
          'Notebook',
          '6x6 Crystal',
          '8x10 Crystal',
          '11x14 Crystal',
          '5x7 Magazine',
          '8x12 Magazine',
          '250p WD Story',
          '500p WD Story'
        ]
      ]
    ];
  }

  public function setMetas($data){

  }
}