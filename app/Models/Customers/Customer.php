<?php

namespace App\Models\Customers;


use Illuminate\Database\Eloquent\Model;
use Phoenix\EloquentMeta\MetaTrait;

class Customer extends Model{
  use MetaTrait;
  /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table;

  protected $dates = ['deleted_at'];

    public $fillable = [
        "name",
        "bridename",
        "groomname",
        "providers",
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        "id" => "integer",
        "name" => "string",
        "bridename" => "string",
        "groomname" => "string",
        "providers" => "string"
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

      /**
   *
   */

    public function orders(){
      return $this->hasMany('App\Models\Orders\Order','customer_id');
    }

    public function origin(){
      return $this->orders->first()->rid;
    }

    public function __construct()
    {
        $this->table = 'customers';
    }

    public function getDisplayNameAttribute(){
        if(!empty($this->name)) return $this->name;
        if(!empty($this->bridename) && empty($this->groomname)) return $this->bridename;
        if(empty($this->bridename) && !empty($this->groomname)) return $this->groomname;
        if(!empty($this->bridename) && !empty($this->groomname)) return $this->bridename.' & '.$this->groomname;
    }

    public function getPhoneAttribute(){
      if($this->getMeta('groomPhone', false)) return $this->getMeta('groomPhone');
      if($this->getMeta('bridePhone', false)) return $this->getMeta('bridePhone');
      return '';
    }

    public function getEmailAttribute(){
      if($this->getMeta('email', false)) return $this->getMeta('email');
      return '';
    }

    public static function genVerificationCode(){
        $number = mt_rand(100000, 999999);

        if(Customer::customerExists($number)){
            return Customer::genVerificationCode();
        }

        return $number;
    }

    public static function customerExists($number){
        return Customer::where('providers', '=', $number)->exists();
    }
}