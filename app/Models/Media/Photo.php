<?php

namespace App\Models\Media;

use Illuminate\Database\Eloquent\Model;

use Imgix\UrlBuilder;

class Photo extends Model
{
    protected $table = 'photos';

    //relationships

    public function file() {
      return $this->belongsTo('App\Models\Media\File');
    }

    public function makeup() {
      return $this->belongsTo('App\Models\Access\User\User', 'makeup_id');
    }

    public function photographer() {
      return $this->belongsTo('App\Models\Access\User\User', 'photographer_id');
    }

    public function galleries() {
      return $this->belongsToMany('App\Models\Media\Gallery', 'gallery_photo');
    }

    

    //photo functions
    public function url() {
      return s3url($this->file->s3key);
    }

    public function thumbnail() {
      $builder = $this->imgix();

      $params = array("w" => 50, 'h' => 50, 'fit' => 'crop', 'auto'=> 'compress');
      return  $builder->createURL($this->file->s3key, $params);
    }

    public function imgix(){
      $builder = new UrlBuilder("img.pariswedding.nyc");
      $builder->setSignKey(config('services.imgix.token'));
      return $builder;
    }

    public function forAI(){
      $builder = $this->imgix();
      $params = [
        'auto'  => 'compress',
        'w' => '1024',
        'fit'   => 'max',
      ];
      return $builder->createURL($this->file->s3key, $params);
    }

    public function withMark($companycode = 'pa', $makeup = '', $photographer = ''){
      $builder = $this->imgix();
      $marksrc = 'https://s3.amazonaws.com/paris-photo/logo/'.strtolower($companycode).'overlaybig.png';
      $txt = empty($makeup) ? '' : 'Makeup by '.$makeup;
      $txt = empty($photographer) ? $txt : $txt.'    '.$photographer;

      $params = [
        'auto'  => 'compress',
        'blend' => $marksrc,
        'w' => '2048',
        'fit'   => 'max',
        //'txtsize' => '25',
        //'txtpad'  => '25',
        //'txtfit' => 'max',
        'bm'  => 'overlay',
        //'txtfont' => 'Charter-Black',
        //'txtclr'  => 'fff',
        //'txtalign' => 'bc',
        //'txt' => $txt,
        'pad' => '50',
        'bg'  => 'fff'
      ];

      if(!empty($makeup) || !empty($photographer)){
        $params['txtsize'] = '50';
        $params['txtpad'] = '50';
        $params['txtfit'] = 'max';
        $params['txtfont'] = 'Charter-Black';
        $params['txtclr'] = 'fff';
        $params['txtalign'] = 'bc';
        $params['txt'] = $txt;
      }

      return $builder->createURL($this->file->s3key, $params);
    }

    public function withLogo(){
      $builder = $this->imgix();

      $params = array("markw" => 0.30, "markh" => 0.30, 'mark' => '/logo/logo.png', 'markfit' => 'clip', 'markalpha'=>'40', 'markalign'=> 'right,bottom', 'markpad'=> '125', 'auto'=> 'compress');
      return  $builder->createURL($this->file->s3key, $params);
    }
}


/*https://sandbox-uploads.imgix.net/u/1475720156-e56f5fa4348ecb2fdc1bc1c3634709f8?pad=25&txt64=TWFrZXVwIGJ5IERlbWkgICAgSkMgUGhvdG9ncmFwaHk&txtalign=bc&txtclr=fff&txtfont64=Q2hhcnRlci1CbGFjaw&txtpad=25&blend64=aHR0cHM6Ly9zYW5kYm94LXVwbG9hZHMuaW1naXgubmV0L3UvMTQ3NTcyMTUyNi05MjQzZjQxMTVjZmMwNmFlMmY0Mjg0NThlOGE3ODJiOQ&bm=overlay&txtfit=max&txtsize=25*/