<?php

namespace App\Models\Media;

use Illuminate\Database\Eloquent\Model;
use Phoenix\EloquentMeta\MetaTrait;
use App\Models\Media\Zip;
use App\Models\Media\Zippable;

use Imgix\UrlBuilder;

class Gallery extends Model implements Zippable
{
    use MetaTrait;

    protected $table = 'galleries';

    //relationships

    public function customer(){
      return $this->belongsTo('App\Models\Customers\Customer');
    }

    public function user(){
        return $this->belongsTo('App\Models\Access\User\User');
    }

    public function photos() {
      return $this->belongsToMany('App\Models\Media\Photo', 'gallery_photo');
    }

    public function photoselects() {
      return $this->hasMany('App\Models\Media\PhotoSelect');
    }

    public function zippable() {
      return [
        'folder' => $this->id,
        'files' => []
      ];
    }

    public function zips() {
      return Zip::where('from', '=', 'App\Models\Media\Gallery')->where('from_id', '=', $this->id)->get();
    }

    public function ziplist(){
      $zips = $this->zips();

      if(count($zips) == 0) return [];

      $output = [];
      foreach ($zips as $zip) {
        $output[] = [
          'hash' => $zip->hash,
          'status' => $zip->status
        ];
      }

      return $output;
    }

}
