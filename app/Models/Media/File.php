<?php

namespace App\Models\Media;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use Imgix\UrlBuilder;

class File extends Model
{
    use SoftDeletes;
    protected $table = 'files';
    protected $dates = ['deleted_at'];

    public function task() {
      return $this->belongsTo('App\Models\Tasks\Task');
    }

    public function user() {
      return $this->belongsTo('App\Models\Access\User\User');
    }

    public function url() {
      return s3url($this->s3key);
    }

    public function thumbnail() {
      $builder = $this->imgix();

      $params = array("w" => 50, 'h' => 50, 'fit' => 'crop', 'auto'=> 'compress');
      return  $builder->createURL($this->s3key, $params);
    }

    public function imgix(){
      $builder = new UrlBuilder("img.pariswedding.nyc");
      $builder->setSignKey(config('services.imgix.token'));
      return $builder;
    }

    public function withLogo(){
      $builder = $this->imgix();

      $params = array("markw" => 0.30, "markh" => 0.30, 'mark' => '/logo/logo.png', 'markfit' => 'clip', 'markalpha'=>'40', 'markalign'=> 'right,bottom', 'markpad'=> '125', 'auto'=> 'compress');
      return  $builder->createURL($this->s3key, $params);
    }
}
