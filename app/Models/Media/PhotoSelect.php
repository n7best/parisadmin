<?php

namespace App\Models\Media;

use Illuminate\Database\Eloquent\Model;
use Phoenix\EloquentMeta\MetaTrait;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Media\Photo;
use App\Models\Media\Zip;
use App\Models\Media\Zippable;

class PhotoSelect extends Model
{
    use MetaTrait, SoftDeletes;
    protected $table = 'photoselect';
    protected $dates = ['deleted_at'];

    //relationships

    public function company(){
      return $this->belongsTo('App\Models\Companies\Company');
    }

    public function gallery(){
        return $this->belongsTo('App\Models\Media\Gallery');
    }

    public function zips() {
      return Zip::where('from', '=', 'App\Models\Media\PhotoSelect')->where('from_id', '=', $this->id)->get();
    }

    public function zippable() {
      $selectedkeys = [];
      foreach (explode(',', $this->selected) as $photoid) {
          $photo = Photo::find($photoid);
          if($photo){
            $selected[] = [
              'id' => $photo->id,
              'thumb' => $photo->thumbnail(),
              'src'       => $photo->url()
            ];

            $selectedkeys[] = $photo->file->s3key;
          }
      }

      return [
        'folder' => $this->gallery_id,
        'files' => $selectedkeys
      ];
    }
}
