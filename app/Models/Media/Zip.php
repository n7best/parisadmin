<?php

namespace App\Models\Media;

use Illuminate\Database\Eloquent\Model;
use Phoenix\EloquentMeta\MetaTrait;
use Imgix\UrlBuilder;

use Aws\S3\S3Client;
use Aws\Credentials\Credentials;

class Zip extends Model
{
    use MetaTrait;
    protected $table = 'zips';

    //relationships

    private $clientPrivateKey;
    private $serverPublicKey;
    private $serverPrivateKey;
    private $expectedBucketName;
    private $expectedHostName; // v4-only
    private $expectedMaxSize;
    private $expectedBucketRegion;
    private $expectedBucketVersion;

    public function __construct(){
         $this->clientPrivateKey = config('services.amazon.clientPrivateKey');
         $this->serverPublicKey = config('services.amazon.serverPublicKey');
         $this->serverPrivateKey = config('services.amazon.serverPrivateKey');
         $this->expectedBucketName = config('services.amazon.expectedBucketName');
         $this->expectedHostName = config('services.amazon.expectedHostName');
         $this->expectedMaxSize = config('services.amazon.expectedMaxSize');
         $this->expectedBucketRegion = config('services.amazon.expectedBucketRegion');
         $this->expectedBucketVersion = config('services.amazon.expectedBucketVersion');
    }

    public function file() {
      return $this->belongsTo('App\Models\Media\File');
    }

    public function source() {
      return $this->belongsTo($this->from, $this->from_id);
    }

    //photo functions
    public function url() {
      if(empty($this->file_id)) return '';

      return $this->getTempLink($this->expectedBucketName, $this->file->s3key);
    }

    private function getS3Client() {
        $credentials = new Credentials($this->serverPublicKey,$this->serverPrivateKey);
        return new S3Client([
            'region' => $this->expectedBucketRegion,
            'version' => $this->expectedBucketVersion,
            'credentials' => $credentials
        ]);
    }

    private function getTempLink($bucket, $key) {
        $client = $this->getS3Client();
        $url = "{$bucket}/{$key}";
        $cmd = $client->getCommand('GetObject',[
           'Bucket' => $bucket,
           'Key' => $key
        ]);
        $request = $client->createPresignedRequest($cmd, '+15 minutes');
        return (string) $request->getUri();
    }
}
