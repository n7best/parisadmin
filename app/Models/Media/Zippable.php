<?php

namespace App\Models\Media;


interface Zippable
{
    public function zippable();
}
