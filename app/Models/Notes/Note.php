<?php

namespace App\Models\Notes;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Note extends Model
{
    use SoftDeletes;
    protected $table = 'note';
    protected $dates = ['deleted_at'];
}
