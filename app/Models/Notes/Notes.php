<?php

namespace App\Models\Notes;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Notes\Note;

class Notes extends Model
{
    use SoftDeletes;
    protected $table = 'notes';
    protected $dates = ['deleted_at'];

    public function note(){
      return $this->hasMany('App\Models\Notes\Note','notes_id');
    }

    public function write($name, $message, $internal = false){
      $note = new Note();
      $note->notes_id = $this->id;
      $note->name = $name;
      $note->message = $message;
      $note->internal = $internal;
      $note->save();

      return $note->id;
    }
}
