<?php

namespace App\Models\Tasks;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Access\User\User;
use Phoenix\EloquentMeta\MetaTrait;
use Carbon\Carbon;

class Task extends Model implements \MaddHatter\LaravelFullcalendar\Event
{
    use SoftDeletes, MetaTrait;
    protected $table = 'tasks';
    protected $dates = ['deleted_at','start', 'end'];

    public function service() {
      return $this->belongsTo('App\Models\Services\Service');
    }

    public function options() {
      return $this->service->metas;
    }

    public function customer() {
      return $this->belongsTo('App\Models\Customers\Customer');
    }

    public function order() {
      return $this->belongsTo('App\Models\Orders\Order');
    }

    public function notes_id() {
      return $this->order->notes_id;
    }

    public function files() {
      return $this->hasMany('App\Models\Media\File');
    }

    public function getCompanyCodeAttribute(){
      return $this->order->company->code;
    }

    public function getAssignNameAttribute(){
      $uids = explode(',',$this->assign_users);
      if(count($uids) > 1){
        $names = [];
        foreach ($uids as $uid) {
          $names[$uid] = User::find($uid)->name;
        }
        return $names;
      }elseif(count($uids) == 1 && !empty($uids[0])){
        $user = User::find($uids[0]);
        return [$uids[0] => $user->name];
      }else{
        return [];
      }
    }

    public function getResourceIdAttribute(){
      return $this->service->resourceId;
    }

    public function getDisplayNameAttribute(){
      return $this->service->display_name;
    }

    public function getAvaibleOptionsAttribute(){
      return json_encode($this->avaibleOptions());
    }

    public function getOrderRidAttribute(){
      return $this->order->rid;
    }

    public function allowUpload(){
      $avaibleOptions = $this->avaibleOptions();
      foreach ($avaibleOptions as $option) {
        if($option['type'] == 'submited'){
          return true;
        }
      }
      return false;
    }

    public function allowSchedule(){
      $avaibleOptions = $this->avaibleOptions();
      foreach ($avaibleOptions as $option) {
        if($option['type'] == 'schedule' || $option['type'] == 'reschedule'){
          return true;
        }
      }
      return false;
    }

    public function avaibleOptions(){
      $all = taskStatus($this->service->name);
      $allStatus = [];

      foreach ($all as $status) {
        $requireRoles = explode(',',$status['role']);
        if(access()->hasRoles($requireRoles, false) || access()->hasRole('Administrator')){
          $allStatus[] = ['type'=>$status['type']];
        }
      }

      return $allStatus;
    }


    public function gallery() {
      if($this->allowUpload()){
        $output= [];
        $files = $this->files;
        foreach ($files as $file) {
          $output[] = [
            'title' => $file->filename,
            'href' => $file->url(),
            'thumbnail' => $file->thumbnail(),
            'withlogo' => $file->withLogo()
          ];
        }
        return json_encode($output);
      }
      return json_encode([]);
    }

    public function galleryMobile() {
      $output= [];
      $files = $this->files;
      foreach ($files as $file) {
        $output[] = [
          'id' => $file->id,
          'thumbnail' => $file->thumbnail(),
          'url' => $file->withLogo()
        ];
      }
      return json_encode($output);
    }

    public function galleryDesktop() {
      $output= [];
      $files = $this->files;
      $picked = $this->getMeta('selected', false);
      foreach ($files as $file) {
        $output[] = [
          'id' => $file->id,
          'thumbnail' => $file->thumbnail(),
          'src' => $file->url(),
          'selected' => $picked ? in_array($file->id, $picked) : false
        ];

      }
      return $output;
    }

    public function getColor() {
      switch ($this->type) {
        case 'pre-photoshot':
          $pretype = $this->getMeta('op-Type');
          if($pretype){
            if($pretype == 'Outdoor'){
              return '#4986E7';
            }elseif($pretype == 'Studio'){
              return '#16A765';
            }else{
              return '#9FE1E7';
            }
          }else{
            return '#9FE1E7';
          }
          return '#7FDBFF';
          break;

        case 'wd-photoshot':
          return '#0074D9';
          break;

        default:
          return '#111111';
          break;
      }
    }

    //event addons


    /**
     * Get the event's id number
     *
     * @return int
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Get the event's title
     *
     * @return string
     */
    public function getTitle()
    {
        $title = $this->getMeta('title');
        if($title) return $title;
        return $this->order->rid.' '.$this->getDisplayNameAttribute();
    }

    /**
     * Is it an all day event?
     *
     * @return bool
     */
    public function isAllDay()
    {
        if($this->getMeta('allday', null) === true || $this->getMeta('allday', null) === 'true'){
          return true;
        }else if($this->getMeta('allday', null) === false || $this->getMeta('allday', null) === 'false'){
          return false;
        }
        if(!empty($this->schedule_end)) return false;
        return true;
    }

    /**
     * Get the start time
     *
     * @return DateTime
     */
    public function getStart()
    {
        return Carbon::parse($this->schedule_at);
    }

    /**
     * Get the end time
     *
     * @return DateTime
     */
    public function getEnd()
    {
        if(!empty($this->schedule_end)) return Carbon::parse($this->schedule_end);
        return Carbon::parse($this->schedule_at);
    }

    /**
     * Optional FullCalendar.io settings for this event
     *
     * @return array
     */
    public function getEventOptions()
    {
        return [
            'color' => $this->getColor(),
            'url' => '/invoice/'.$this->order->id.'?tid='.$this->id,
            'resourceId' => $this->resource_id,
            'id' => $this->id,
            'type' => 'task',
            'company' => $this->company_code
        ];
    }
}
