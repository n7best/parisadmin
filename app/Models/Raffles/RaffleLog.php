<?php

namespace App\Models\Raffles;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RaffleLog extends Model
{
    use SoftDeletes;

    public $table = "raffle_log";

    protected $dates = ['deleted_at'];

}
