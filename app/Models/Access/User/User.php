<?php

namespace App\Models\Access\User;

use App\Models\Access\User\Traits\UserAccess;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Models\Access\User\Traits\Attribute\UserAttribute;
use App\Models\Access\User\Traits\Relationship\UserRelationship;
use App\Models\Tasks\Task;
use Carbon\Carbon;

/**
 * Class User
 * @package App\Models\Access\User
 */
class User extends Authenticatable
{

    use SoftDeletes, UserAccess, UserAttribute, UserRelationship;

    /**
     * The attributes that are not mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    /**
     * @var array
     */
    protected $dates = ['deleted_at'];

    public function tasks(){
        $uid = $this->id;
        $rawtasks = Task::whereRaw("find_in_set('$uid',assign_users) <> 0")->where('status','=','schedule')->get();

        return $rawtasks;
    }

    public function isAvailable($timeStart, $timeEnd, $exclude = false){
        $tasks = $this->tasks();
        foreach ($tasks as $task) {
            if($exclude && $task->id == $exclude) continue;
            if($task->isAllDay()){
                if($task->getStart()->between($timeStart,$timeEnd)) return false;
            }else{
                if($task->getStart()->between($timeStart,$timeEnd) || $task->getEnd()->between($timeStart,$timeEnd)) return false;
            }
        }

        return true;
    }
}
