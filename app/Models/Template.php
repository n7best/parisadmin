<?php

namespace App\Models;

use Eloquent as Model;
use App\BladeSupport\BladeRenderer;

class Template extends Model
{

    public $table = "templates";
    protected $dates = ['deleted_at'];

    public $fillable = [
        "slog",
        "html"
    ];


    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
    ];

    public static function invoice(){
        return Template::where('slog','=','invoice')->first()->html;
    }

    public static function render($key, $data = []) {
        if(!Template::where('slog','=', $key)->exists()){
            return '';
        }
        $htmlraw = html_entity_decode(Template::where('slog','=', $key)->first()->html);
        $html = new BladeRenderer($htmlraw, $data);
        return $html->render();
    }
}
