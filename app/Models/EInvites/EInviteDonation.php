<?php

namespace App\Models\EInvites;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class EInviteDonation extends Model
{
    use SoftDeletes;
    protected $table = 'einvitedonations';
    protected $dates = ['deleted_at'];
}
