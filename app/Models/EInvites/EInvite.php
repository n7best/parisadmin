<?php

namespace App\Models\EInvites;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class EInvite extends Model
{
    use SoftDeletes;
    protected $table = 'einvites';
    protected $dates = ['deleted_at'];

    public function donations() {
      return $this->hasMany('App\Models\EInvites\EInviteDonation','einvite_id');
    }

    public function logs() {
      return $this->hasMany('App\Models\EInvites\EInviteLog','einvite_id');
    }
}
