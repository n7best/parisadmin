<?php

namespace App\Models\EInvites;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class EInviteLog extends Model
{
    use SoftDeletes;
    protected $table = 'einvitelogs';
    protected $dates = ['deleted_at'];
}
