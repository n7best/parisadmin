<?php

namespace App\Models\Orders;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Access\User\User;
class Order extends Model
{
    use SoftDeletes;
    protected $table = 'orders';
    protected $dates = ['deleted_at'];


    public function tasks(){
      return $this->hasMany('App\Models\Tasks\Task','order_id');
    }

    public function package(){
      return $this->belongsTo('App\Models\Package\Package');
    }

    public function customer(){
      return $this->belongsTo('App\Models\Customers\Customer');
    }

    public function notes(){
      return $this->belongsTo('App\Models\Notes\Notes');
    }

    public function company(){
      return $this->belongsTo('App\Models\Companies\Company');
    }

    public function payment(){
      return $this->hasMany('App\Models\Orders\Payment','order_id');
    }

    public function getSalesNameAttribute(){
      $uids = explode(',',$this->sales);
      if(count($uids) > 1){
        $names = [];
        foreach ($uids as $uid) {
          $names[$uid] = User::find($uid)->name;
        }
        return $names;
      }else{
        $user = User::find($uids[0]);
        return [$uids[0] => $user->name];
      }
    }

    public function getAvaibleOptionsAttribute(){
      $all = config('status.order');
      $allStatus = [];

      foreach ($all as $status) {
        $requireRoles = explode(',',$status['role']);
        if(access()->hasRoles($requireRoles, false) || access()->hasRole('Administrator')){
          $allStatus[] = ['type'=>$status['type']];
        }
      }
      return json_encode($allStatus);
    }
}
