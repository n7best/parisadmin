<?php

namespace App\Models\Orders;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Access\User\User;


class Coupon extends Model
{
    use SoftDeletes;
    protected $table = 'coupons';
    protected $dates = ['deleted_at'];

    public $fillable = [
        "type",
        "value",
        "requirements",
        "code"
    ];

    public function order(){
      return $this->belongsTo('App\Models\Orders\Order');
    }

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    public static function genCode(){
        $number = mt_rand(100000, 999999);

        if(Coupon::couponExists($number)){
            return Coupon::genCode();
        }

        return $number;
    }

    public static function couponExists($number){
        return Coupon::where('code', '=', $number)->exists();
    }

    public static function verify($allcoupons, $packages, $services){
        $total = 0;
        $details = [];
        $onlyCount = 0;
        $valid = true;
        $errMsg = 'Invalid Coupon';

        foreach ($allcoupons as $couponcode) {
          if($valid){
            $coupon = Coupon::where('code','=', $couponcode)->first();

            //check if already exists
            $requirements = explode(',', $coupon->requirements);

            foreach ($requirements as $req) {
              switch ($req) {
                case 'public':

                  break;
                case 'package':
                  $packages = $packages && $packages !== 'false' ? $packages : false;
                  if(!$packages){
                    $valid = false;
                    $errMsg = 'Not Valid Package for Coupon';
                  }
                  break;
                case 'only':
                  $onlyCount++;
                  if($onlyCount > 1){
                    $valid = false;
                    $errMsg = 'This Coupon can only use by itself';
                  }
                  break;
                default:
                  $valid = false;
                  break;
              }
            }

            if(!$valid) {
              return [
                  'fail' => true,
                  'msg' => $errMsg
              ];
            }

            switch ($coupon->type) {
              case 'cash':
                $total = $total + $coupon->value;
                $details[] = $coupon->code;
                break;

              case 'percent':
                foreach ($packages as $package) {
                  $offValue = ceil($package['price'] * ($coupon->value / 100));
                  $total = $total + $offValue;
                }
                $details[] = $coupon->code;
                break;
              default:
                $valid = false;
                break;
            }
          }else{
            break;
          }
        }

        return [
          'fail' => false,
          'discounts' => $total,
          'allcoupons' => $details,
          'message' => 'Applied Succesfful'
        ];
    }
}
