<?php

namespace App\Models\Orders;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Access\User\User;
use App\Traits\EncryptableTrait;

class Payment extends Model
{
    use SoftDeletes, EncryptableTrait;

    protected $table = 'payments';
    protected $dates = ['deleted_at'];
    protected $encryptable = [
        'amount'
    ];

    public function user(){
        return $this->belongsTo('App\Models\Access\User\User');
    }

    public function order(){
        return $this->belongsTo('App\Models\Orders\Order');
    }

    public static function fromOrder($id){
      return Payment::where('order_id','=',$id)->get();
    }

}
