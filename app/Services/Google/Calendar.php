<?php namespace App\Services\Google;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Config;

class Calendar {

    protected $client;

    protected $service;

    function __construct() {
        $client_id = config('services.google.client_id');
        $service_account_name = config('services.google.client_id');
        $key_file_location = base_path() . config('services.google.key_file_location');
        $client_secret = config('services.google.client_secret');
        $key = config('services.google.api_key');//you can use later

        $this->client = new \Google_Client();
        $this->client->setApplicationName(config('services.google.app_name'));
        $this->client->setDeveloperKey($key);

        $this->service = new \Google_Service_Calendar($this->client);

        if (Cache::has('service_token')) {
            $this->client->setAccessToken(Cache::get('service_token'));
        }




        $key = file_get_contents($key_file_location);
        $scopes = array('https://www.googleapis.com/auth/calendar', 'https://www.googleapis.com/auth/calendar.readonly');


        $cred = new \Google_Auth_AssertionCredentials(
            $service_account_name,
            $scopes,
            $key
        );

        $this->client->setAssertionCredentials($cred);
        if ($this->client->getAuth()->isAccessTokenExpired()) {
            $this->client->getAuth()->refreshTokenWithAssertion($cred);
        }
        Cache::forever('service_token', $this->client->getAccessToken());
        
    }

    public function addEvent()
    {
        $service = new \Google_Service_Calendar($this->client);
        $event = new \Google_Service_Calendar_Event();
        $event->setSummary('Appointment');
        $event->setLocation('Somewhere');
        $start = new \Google_Service_Calendar_EventDateTime();
        $start->setDateTime('2016-02-29T13:00:00+05:30');
        $start->setTimeZone('America/Los_Angeles');
        $event->setStart($start);
        $end = new \Google_Service_Calendar_EventDateTime();
        $end->setDateTime('2016-02-29T14:00:00+05:30');
        $start->setTimeZone('America/Los_Angeles');
        $event->setEnd($end);
        $attendee1 = new \Google_Service_Calendar_EventAttendee();
        $attendee1->setEmail('rahul@headerlabs.com');
        // ...
        $attendees = array($attendee1
            //, ...
        );
        $event->attendees = $attendees;
        $createdEvent = $service->events->insert('primary', $event);
        echo $createdEvent->getId();
    }

    public function get($calendarId)
    {
        $results = $this->service->calendars->get($calendarId);

        return $resuls;
    }

    public function events($calendarId){
        $optParams = array(
          'maxResults' => 10,
          'orderBy' => 'startTime',
          'singleEvents' => TRUE,
          'timeMin' => date('c'),
        );
        $results = $this->service->events->listEvents($calendarId);

        if (count($results->getItems()) == 0) {
          print "No upcoming events found.\n";
        } else {
          print "Upcoming events:\n";
          foreach ($results->getItems() as $event) {
            $start = $event->start->dateTime;
            if (empty($start)) {
              $start = $event->start->date;
            }
            printf("%s (%s)\n", $event->getSummary(), $start);
          }
        }
    }

}