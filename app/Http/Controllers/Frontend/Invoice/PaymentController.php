<?php

namespace App\Http\Controllers\Frontend\Invoice;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Activity;
use Carbon\Carbon;
use Flash;
use App\Models\Orders\Order;
use App\Models\Orders\Payment;
use App\Models\Companies\Company;
use App\Models\Customers\Customer;
use DB;
use App\Models\Raffles\RaffleLog;
use App\Models\Notes\Notes;

class PaymentController extends Controller
{
    public function __construct() {
       // \Debugbar::disable();
    }

    public function index(Request $req){

      $inputs = $req->all();

      $companies = Company::all();

      if(!empty($inputs['start']) && !empty($inputs['end'])){
        $start = new Carbon($inputs['start']);
        $end = new Carbon($inputs['end']);
        $end->addHours(23)->addMinutes(59)->addSeconds(59);
      }else{
        $start = Carbon::today();
        $end = Carbon::today();
        $end->addHours(23)->addMinutes(59)->addSeconds(59);
      }

      $payments = Payment::with('order')->whereBetween('created_at', [$start, $end])->get();
      $paymentData = [];
      foreach ($payments as $payment) {
        $paymentData[] = [
          'status' => $payment->status,
          'amount' => intval($payment->amount),
          'rid' => $payment->order->rid,
          'type' => $payment->type,
          'company' => $payment->order->company->code,
        ];
      }

      $orders = Order::whereBetween('created_at', [$start, $end])->get();
      $ordersData = [];
      foreach ($orders as $order) {
        $ordersData[] = [
          'rid' => $order->rid,
          'price' => $order->price,
          'balance' => $order->balance,
          'company' => $order->company->code,
          'created_at' => $order->created_at->toDateTimeString()
        ];
      }

      $customers = Customer::whereBetween('created_at', [$start, $end])
                  ->whereExists(function($query){
                    $query->select(DB::raw(1)) -> from('orders')
                     ->whereRaw('orders.customer_id= customers.id');
                  })
                  ->get();
      $customersData = [];
      foreach ($customers as $customer) {
        $customersData[] = [
          'name' => $customer->display_name,
          'company' => $customer->orders->first()->company->code,
          'created_at' => $customer->created_at->toDateTimeString()
        ];
      }

      $raffeLogs = RaffleLog::whereBetween('created_at', [$start, $end])->get();
      $raffeLogsData = [];
      foreach ($raffeLogs as $raffle) {
        $raffeLogsData[] = [
          'detail' => $raffle->detail,
          'created_at' => $raffle->created_at->toDateTimeString()
        ];
      }

      return view('frontend.payment.index')
              ->withStart($start)
              ->withEnd($end)
              ->withCustomers(collect($customersData))
              ->withCompanies($companies)
              ->withOrders(collect($ordersData))
              ->withRaffles(collect($raffeLogsData))
              ->withPayments(collect($paymentData));
    }

    public function store(Request $request){

      $inputs = $request->all();

      $user = $request->user();

      if(!empty($inputs['type']) && !empty($inputs['amount']) && !empty($inputs['orderid']) && Order::where('id', '=', $inputs['orderid']) ){
        $order = Order::with('tasks','customer','notes')->find($inputs['orderid']);
        $notes = Notes::find($order->notes_id);

        Activity::log([
            'contentId'   => $order->id,
            'contentType' => 'Order',
            'action'      => 'Update',
            'description' => 'Updated order '.$order->display_name.' Payment',
            'details'     => 'Amount: '.$inputs['amount'].' To: '.$order->balance,
        ]);
        $notes->write($user->name, 'Add order <b>'.$order->rid.'</b> Payment <br/> Amount: <b>'.$inputs['amount'].'</b><br/> Original Balance: <b>'.$order->balance.'</b><br/> New Balance: <b>'.((float)$order->balance - (float)$inputs['amount']).'</b>');

        $payment = new Payment();
        $payment->type = $inputs['type'];

        $payment->user_id = $user->id;
        $payment->order_id = $order->id;

        $payment->status = 'created';
        $payment->amount = $inputs['amount'];

        $payment->save();

        $order->balance = $order->balance - $payment->amount;
        $order->update();
      }

      Flash::success('Invoice updated successfully.');

      return back();
    }
}