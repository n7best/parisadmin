<?php

namespace App\Http\Controllers\Frontend\Invoice;

use App\Http\Controllers\Controller;
use App\Http\Requests\Frontend\Invoice\CreateInvoiceRequest;
use App\Http\Requests\Frontend\Invoice\StoreInvoiceRequest;
use App\Models\Customers\Customer;
use Illuminate\Http\Request;
use App\Repositories\Backend\Companies\CompanyRepository;

use Activity;
use Datatables;

use App\Models\Orders\Order;
use App\Models\Tasks\Task;
use App\Models\Notes\Notes;
use App\Models\Access\User\Hrs;
use Carbon\Carbon;

use Debugbar;
use App\Models\Services\Service;
use App\Models\Orders\Coupon;
use App\Events\NewOrderCreated;
use App\Models\Template;
use PDF;
use Flash;

use Firebase\JWT\JWT;
use App\Models\Package\Package;
/**
 * Class ProfileController
 * @package App\Http\Controllers\Frontend
 */
class InvoiceController extends Controller
{
    public function __construct() {
       // \Debugbar::disable();
    }

    public function pdf($id, Request $req){
      $finalHtml = '';
      $inputs = $req->all();
      if(!empty($inputs['invoice'])){
        $order = Order::with('tasks','customer','notes')->find($id);
        $tasks = [];
        $counts = [];
        foreach ($order->tasks as $task) {
          $service = $task->service;
          if(array_key_exists($service->catalog, $counts)){
            //Debugbar::info($task->status);
            if($task->status != 'complete') {
              $counts[$service->catalog] = $counts[$service->catalog] + 1;
            }
          }else{
            $counts[$service->catalog] = 0;
            if($task->status != 'complete') {
              $counts[$service->catalog] = $counts[$service->catalog] + 1;
            }
          }
          $tasks[$service->catalog][] = [
            'detail'=>$task,
            'service'=>$service,
            'options'=>$task->getAllMeta()
          ];
        }

        $data = [
          'company' => $order->company,
          'order' => $order,
          'tasks' => $tasks
        ];

        $html = html_entity_decode(Template::render('invoice', $data));
        $allhtml = view('includes.partials.invoicecss').$html;
        $finalHtml = $finalHtml.$allhtml;
      }

      if(!empty($inputs['terms'])){
        $finalHtml = $finalHtml.html_entity_decode(Template::render('terms'));
      }

      if(!empty($inputs['tips'])){
        $finalHtml = $finalHtml.html_entity_decode(Template::render('tips'));
      }

      if(empty($finalHtml)) return 'Nothing to generate';
      def("DOMPDF_ENABLE_REMOTE", false);
      $pdf = PDF::loadHTML($finalHtml);
      return $pdf->stream('invoice.pdf');
    }
    /**
     * @return mixed
     */
    public function index(Request $req)
    {
        $uid = $req->user()->id;
        $jwtToken = JWT::encode(['uid'=>$uid], config('app.key'));
        $orders = Order::all();

        return view('frontend.invoice.index')
            ->with('jwtToken', $jwtToken)
            ->with('orders', $orders);
    }

    public function tableInvoiceData(){

      $orders = Order::with('package','customer')->get();

      debug($orders);

      return Datatables::of($orders)
      ->addColumn('action', function ($order) {
            return '<a href="/invoice/'.$order->id.'" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> Full Detail</a>';
        })
      ->editColumn('package.display_name', function($orderone){
        if(!$orderone->package) return 'No Package';
        return $orderone->package->display_name;
      })
      ->editColumn('customer.display_name', function($orderone){
        return $orderone->customer->display_name;
      })
      ->make(true);
    }

    public function show($orderid, Request $req)
    {
      $order = Order::with('tasks','customer','notes')->find($orderid);
      $order->notes->load('note');
      $notes = $order->notes->note;
      $notesid = $order->notes->id;
      $customer = $order->customer;
      $payment = $order->payment;

      $nav = $req->has('tid') ? $req->input('tid') : false;
      $navcatalog = false;

      //get all tasks with related service
      $tasks = [];
      $counts = [];
      foreach ($order->tasks as $task) {
        $service = $task->service;
        if(array_key_exists($service->catalog, $counts)){
          //Debugbar::info($task->status);
          if($task->status != 'complete') {
            $counts[$service->catalog] = $counts[$service->catalog] + 1;
          }
        }else{
          $counts[$service->catalog] = 0;
          if($task->status != 'complete') {
            $counts[$service->catalog] = $counts[$service->catalog] + 1;
          }
        }
        $tasks[$service->catalog][] = [
          'nav' => $task->id == (int)$nav ? true : false,
          'detail'=>$task,
          'service'=>$service,
          'options'=>$task->getAllMeta()
        ];

        if($task->id == (int)$nav) $navcatalog =$service->catalog;
      }

      header("Access-Control-Allow-Origin: *");

      return view('frontend.invoice.show')
          ->with('nav', $nav)
          ->with('navcatalog', $navcatalog)
          ->with('order', $order)
          ->with('notes', $notes)
          ->with('notesid', $notesid)
          ->with('customer', $customer)
          ->with('tasks', $tasks)
          ->with('taskCounts', $counts)
          ->with('payment', $payment);

    }

    public function create(CreateInvoiceRequest $req, CompanyRepository $companyRepo)
    {
      $cid = $req->input('c');
      $company = $companyRepo->findWithoutFail($cid);

      $coupons = Coupon::whereRaw("find_in_set('public',requirements) <> 0")->get();

      return view('frontend.invoice.create')
          ->withUser(access()->user())
          ->with('company',$company)
          ->with('coupons', $coupons);
    }

    public function verifyCoupon(Request $req)
    {
      $inputs = $req->all();

      if(!empty($inputs['code']) && isset($inputs['applied']) && !empty($inputs['services']) && !empty($inputs['packages']) && Coupon::where('code','=',$inputs['code'])->exists()) {

          $allcoupons = $inputs['applied'] && $inputs['applied'] !== 'false' ? $inputs['applied'] : [];

          //check if already exists
          if(in_array($inputs['code'], $allcoupons)){
            return response()->json([
                'fail' => true,
                'msg' => 'Code Already Applied'
            ], 401);
          }else{
            $allcoupons[] = $inputs['code'];
          }

          $verification = Coupon::verify($allcoupons, $inputs['packages'], $inputs['services']);

          if($verification['fail']){
            return response()->json([
                'fail' => true,
                'msg' => $verification['msg']
            ], 401);
          }

          return $verification;
      }

      return response()->json([
          'fail' => true,
          'msg' => 'Invalid Code'
      ], 401);
    }

    public function update($id, Request $req){
      //dd($req->all());
      $inputs = $req->all();
      $user = $req->user();

      $order = Order::find($id);

      $notes = Notes::find($order->notes_id);

      if($req->has('customer')){
        $ogCustomer = $order->customer;
        $customer = $inputs['customer'];

        if(!empty($customer['groomPhone']) && $customer['groomPhone'] !== $ogCustomer->getMeta('groomPhone', '')){
          Activity::log([
              'contentId'   => $ogCustomer->id,
              'contentType' => 'Customer',
              'action'      => 'Update',
              'description' => 'Updated customer '.$ogCustomer->id.' Groom Phone',
              'details'     => 'From: '.$ogCustomer->getMeta('groomPhone', '').' To: '.$customer['groomPhone'],
          ]);
          $notes->write($user->name, 'Updated Customer <b>'.$ogCustomer->id.'</b> Groom Phone <br/> From: <b>'.$ogCustomer->getMeta('groomPhone', '').'</b><br/> To: <b>'.$customer['groomPhone'].'</b>');

          $ogCustomer->updateMeta('groomPhone', $customer['groomPhone']);
        }

        if(!empty($customer['bridePhone']) && $customer['bridePhone'] !== $ogCustomer->getMeta('bridePhone', '')){
          Activity::log([
              'contentId'   => $ogCustomer->id,
              'contentType' => 'Customer',
              'action'      => 'Update',
              'description' => 'Updated customer '.$ogCustomer->id.' Bride Phone',
              'details'     => 'From: '.$ogCustomer->getMeta('bridePhone', '').' To: '.$customer['bridePhone'],
          ]);
          $notes->write($user->name, 'Updated Customer <b>'.$ogCustomer->id.'</b> Bride Phone <br/> From: <b>'.$ogCustomer->getMeta('bridePhone', '').'</b><br/> To: <b>'.$customer['bridePhone'].'</b>');

          $ogCustomer->updateMeta('bridePhone', $customer['bridePhone']);
        }

        if(!empty($customer['email']) && $customer['email'] !== $ogCustomer->getMeta('email', '')){
          Activity::log([
              'contentId'   => $ogCustomer->id,
              'contentType' => 'Customer',
              'action'      => 'Update',
              'description' => 'Updated customer '.$ogCustomer->id.' Email',
              'details'     => 'From: '.$ogCustomer->getMeta('email', '').' To: '.$customer['email'],
          ]);
          $notes->write($user->name, 'Updated Customer <b>'.$ogCustomer->id.'</b> Email <br/> From: <b>'.$ogCustomer->getMeta('email', '').'</b><br/> To: <b>'.$customer['email'].'</b>');

          $ogCustomer->updateMeta('email', $customer['email']);
        }
      }

      if(!empty($inputs['status']) && $inputs['status'] !== $order->status && $user->allow('edit-invoice')){

        Activity::log([
            'contentId'   => $order->id,
            'contentType' => 'Order',
            'action'      => 'Update',
            'description' => 'Updated order '.$order->rid.' Status',
            'details'     => 'From: '.$order->status.' To: '.$inputs['status'],
        ]);
        $notes->write($user->name, 'Updated order <b>'.$order->rid.'</b> Status <br/> From: <b>'.$order->status.'</b><br/> To: <b>'.$inputs['status'].'</b>');

        $order->status = $inputs['status'];
        $order->update();

      }

      if(!empty($inputs['price']) && $inputs['price'] !== $order->price && $user->allow('edit-invoice-price')){

        Activity::log([
            'contentId'   => $order->id,
            'contentType' => 'Order',
            'action'      => 'Update',
            'description' => 'Updated order '.$order->rid.' price',
            'details'     => 'From: '.$order->price.' To: '.$inputs['price'],
        ]);
        $notes->write($user->name, 'Updated order <b>'.$order->rid.'</b> price <br/> From: <b>'.$order->price.'</b><br/> To: <b>'.$inputs['price'].'</b>');

        $order->price = $inputs['price'];
        $order->update();

      }

      if(!empty($inputs['sales_name']) && is_array($inputs['sales_name']) &&  implode(',', $inputs['sales_name']) !== $order->sales_name && $user->allow('edit-invoice')){

        $newnames = '';
        $oldnames = '';
        foreach ($inputs['sales_name'] as $newnameid) {
          $newnames = $newnames.uName($newnameid).' ';
        }
        foreach ($order->sales_name as $oldname) {
          $oldnames = $oldnames.$oldname.' ';
        }

        Activity::log([
            'contentId'   => $order->id,
            'contentType' => 'Order',
            'action'      => 'Update',
            'description' => 'Updated order '.$order->display_name.' Assignee',
            'details'     => 'From: '.$oldnames.' To: '.$newnames,
        ]);
        $notes->write($user->name, 'Updated order <b>'.$order->rid.'</b> sales_name <br/> From: <b>'.$oldnames.'</b><br/> To: <b>'.$newnames.'</b>');

        $order->sales = implode(',', $inputs['sales_name']);
        $order->update();

      }

      Flash::success('Invoice updated successfully.');

      return redirect(route('invoice.show', $id));
    }

    public function store(StoreInvoiceRequest $req)
    {

      $inputs = $req->all();

      //dd($inputs);

      $customer = $inputs['customer'];
      $services = $inputs['services'];
      $packages = !empty($inputs['packages']) ? $inputs['packages'] : false;
      $prices = $inputs['price'];
      $invoice = $inputs['order'];
      $coupons = empty($inputs['coupons']) ? false : $inputs['coupons'];

      if($coupons){
        $verification = Coupon::verify($coupons, $inputs['packages'], $services);

        if($verification['fail']){
          return response()->json([
              'fail' => true,
              'errors' => [$verification['msg']]
          ], 401);
        }
        $discounts = $verification['discounts'];
      }else{
        $discounts = 0;
      }


      if(!empty($customer['id']) && $customer['id'] !== 'false'){
        $newCustomer = Customer::find($customer['id']);
        if(!empty($customer['invoiceBrideName'])) $newCustomer->bridename = $customer['invoiceBrideName'];
        if(!empty($customer['invoiceGroomName'])) $newCustomer->groomname = $customer['invoiceGroomName'];
        $newCustomer->update();
      }else{
        $newCustomer = new Customer();
        if(!empty($customer['invoiceBrideName'])) $newCustomer->bridename = $customer['invoiceBrideName'];
        if(!empty($customer['invoiceGroomName'])) $newCustomer->groomname = $customer['invoiceGroomName'];
        $newCustomer->save();

        Activity::log([
            'contentId'   => $newCustomer->id,
            'contentType' => 'Customer',
            'action'      => 'Create',
            'description' => 'Created a Customer',
            'details'     => 'Username: '.$req->user()->name,
        ]);
      }

      if(empty($newCustomer->providers)){
        $newCustomer->providers = Customer::genVerificationCode();
        $newCustomer->update();
      }

      if(!empty($customer['invoiceWeddingDate'])) $newCustomer->addMeta('weddingDate',$customer['invoiceWeddingDate']);
      if(!empty($customer['invoiceStudioDate'])) $newCustomer->addMeta('studioDate',$customer['invoiceStudioDate']);
      if(!empty($customer['invoiceOutdoorDate'])) $newCustomer->addMeta('outdoorDate',$customer['invoiceOutdoorDate']);
      if(!empty($customer['invoiceCustomerAddress'])) $newCustomer->addMeta('address',$customer['invoiceCustomerAddress']);
      if(!empty($customer['invoiceBridePhone'])) $newCustomer->addMeta('bridePhone',$customer['invoiceBridePhone']);
      if(!empty($customer['invoiceGroomPhone'])) $newCustomer->addMeta('groomPhone',$customer['invoiceGroomPhone']);
      if(!empty($customer['invoiceCustomerEmail'])) $newCustomer->addMeta('email',$customer['invoiceCustomerEmail']);
      if(!empty($customer['notification'])) $newCustomer->addMeta('notification',$customer['notification']);


      $package_id = null;
      if($packages){
        foreach ($packages as $package) {
          $dbp = Package::where('name', '=', $package['pid'])->first();
          if($dbp){
            $package_id = $dbp->id;
            break;
          }
        }
      }
      //create note
      $notes = new Notes();
      $notes->save();

      //create order
      $order = new Order();
      $order->status = 'created';
      $order->company_id = $invoice['company'];
      $order->customer_id = $newCustomer->id;
      $order->price = $prices['total'];
      $order->balance = $prices['total'];
      $order->tax = $prices['tax'];
      $order->price_services = $prices['services'];
      $order->sales = $invoice['sales'];
      $order->notes_id = $notes->id;
      $order->package_id = $package_id;


      if($discounts > 0) $order->discounts = $discounts;

      if(!empty($prices['adjustment'])) $order->adjustment = $prices['adjustment'];

      $order->save();

      if($req->user()->allow('edit-rid') && !empty($customer['invoiceCustomRID'])){
        $order->rid = $invoice['company_code'].$customer['invoiceCustomRID'];
      }else{
        $order->rid = $invoice['company_code'].$invoice['sales_init'].$order->id;
      }

      $order->update();
      //create tasks

      foreach ($services as $service) {
        $task = new Task();
        $task->status = 'created';
        $task->order_id = $order->id;
        $task->customer_id = $newCustomer->id;
        $task->type = $service['itemid'];
        $task->service_id = $service['sid'];
        $detail = '';

        /*foreach ($service['options'] as $option) {
          $detail = $detail.$option['name'].':'.$option['value'].',';
        }
*/
        $task->details = $detail;

        $task->save();

        foreach ($service['options'] as $sopt) {
          $task->addMeta('op-'.$sopt['name'], $sopt['value']);
        }

        /*$allOptions = Service::options($service['sid']);

        //dd($allOptions);
        //$service['options']
        foreach ( $allOptions as $option) {
          //Debugbar::info($option);
          $type = $option->name;


          $value = null;

          foreach ($service['options'] as $sopt) {
            if($sopt['name'] == $type) $value = $sopt['value'];
          }
          //Debugbar::info($type, $value);
          //do it in the view
          $task->addMeta('op-'.$type, $value);
        }*/
      }

      //notes after succesful
      $notes->write($req->user()->name, 'Create customer '.$newCustomer->id.': '.$newCustomer->display_name);
      $notes->write($req->user()->name, 'Create this order: '.$order->rid);

      //return false;

      event(new NewOrderCreated($order));
      return $order;
    }
}