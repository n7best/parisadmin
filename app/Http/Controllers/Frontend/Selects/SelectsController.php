<?php

namespace App\Http\Controllers\Frontend\Selects;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Notes\Notes;
use App\Models\Notes\Note;
use App\Models\Access\User\User;
use App\Models\Customers\Customer;

class SelectsController extends Controller
{
    public function index(Request $req){
      $inputs = $req->all();
      $type = $inputs['d'];
      $term = $inputs['term'];
      switch ($type) {
        case 'user':
          return $this->user($term);
          break;
        case 'customers':
          return $this->customers($term);
        default:
          # code...
          break;
      }
    }

    private function user($q){
      $output = [];
      $list = User::where('name', 'like', '%'.$q.'%')->lists('name', 'id');
      foreach ($list as $key => $value) {
        $output[] = ['text'=>$value, 'id'=>$key];
      }
      return $output;
    }

    private function customers($q){
      $output = [];
      $list = Customer::where('groomname', 'like', '%'.$q.'%')
              ->orWhere('bridename', 'like', '%'.$q.'%')
              ->get();
      foreach ($list as $customer) {
        $userdata = [
          'text'=>$customer->display_name, 
          'id'=>$customer->id,
          'metas'=>$customer->getAllMeta(),
          'groomname'=> $customer->groomname,
          'bridename' => $customer->bridename
        ];
        $output[] = $userdata;
      }
      return $output;
    }
}
