<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Customers\Customer;
use Illuminate\Http\Request;
use Firebase\JWT\JWT;
use App\Models\Media\Zip;
use App\Models\Media\File;
use Validator;
use Vinkla\Hashids\Facades\Hashids;

use Aws\Sqs\SqsClient;
use Aws\Credentials\Credentials;


class ZipController extends Controller{

  public function show($hash, Request $req){
    $id = Hashids::decode($hash);
    $zip = Zip::find($id);

    if(count($zip) == 0){
      return response()->json([
          'fail' => true,
          'msg' => 'Invalid arguments! no zip'
      ], 401);
    }

    $zip = $zip->first();

    return json_encode([
      'hash' => $zip->hash,
      'status' => $zip->status,
      'url' => $zip->url()
    ]);
  }

  public function store(Request $req){
    if(!$req->has('type') || !$req->has('type_id')){
      return response()->json([
          'fail' => true,
          'msg' => 'Invalid arguments! no type'
      ], 401);
    }

    if(Zip::where('from', '=', $req->input('type'))->where('from_id', '=', $req->input('type_id'))->where('status', '<>', 'error')->exists()){
      return response()->json([
          'fail' => true,
          'msg' => 'Zip already exists'
      ], 401);
    }

    if(!$req->has('folder') && !$req->has('files')){
      return response()->json([
          'fail' => true,
          'msg' => 'Invalid arguments! no inputs'
      ], 401);
    }

    $zip = new Zip();
    $zip->from = $req->input('type');
    $zip->from_id = $req->input('type_id');
    $zip->status = 'created';
    $zip->save();

    $zip->hash = Hashids::encode($zip->id);
    $zip->update();


    $credentials = new Credentials(config('services.amazon.serverPublicKey'), config('services.amazon.serverPrivateKey'));
    // Instantiate the SQS client with your AWS credentials
    $client = SqsClient::factory(array(
        'credentials' => $credentials,
        'region'  => 'us-east-1',
        'version' => '2012-11-05'
    ));

    $result = $client->createQueue(array('QueueName' => 'zips'));
    $queueUrl = $result->get('QueueUrl');

    $data = [
        'hash' => $zip->hash,
        'zippable' => call_user_func([$zip->from, 'find'], $zip->from_id)->zippable(),
        'apiUrl'  => route('zips.update', $zip->hash),
        'user_id' => $req->user()->id
    ];

    $client->sendMessage(array(
        'QueueUrl'    => $queueUrl,
        'MessageBody' => json_encode($data),
    ));

    return response()->json([
      'hash' => $zip->hash,
      'status' => $zip->status,
    ]);
  }

  public function update($hash, Request $req){
    $id = Hashids::decode($hash);
    $zip = Zip::find($id);

    if(count($zip) == 0){
      return response()->json([
          'fail' => true,
          'msg' => 'Invalid arguments! no zip'
      ], 401);
    }

    $zip = $zip->first();
    if($req->has('status')){
      $zip->status = $req->input('status');

      if($req->input('status') == 'complete'){
        $key = $req->input('file');
        if(File::where('s3key','=',$key)->exists()){
          $file = File::where('s3key','=',$key)->first();
        }else{
          $file = new File();

          $file->task_id = null;
          $file->user_id = $req->input('user_id');
          $file->type = 'file';
          $file->filename = $key;
          $file->s3key = $key;
          $file->save();
        }

        $zip->file_id = $file->id;
      }
    }

    $zip->update();

    return response()->json([
      'hash' => $zip->hash,
      'status' => $zip->status,
    ]);
  }
}