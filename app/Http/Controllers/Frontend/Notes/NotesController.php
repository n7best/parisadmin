<?php

namespace App\Http\Controllers\Frontend\Notes;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Notes\Notes;
use App\Models\Notes\Note;
use App\Http\Requests\Frontend\Notes\CreateNoteRequest;

class NotesController extends Controller
{
    public function post(CreateNoteRequest $req){
      $inputs = $req->all();
      $nid = $inputs['ni'];
      $message = $inputs['message'];
      $internal = !empty($inputs['internal']) && $inputs['internal'] == '1' ? true : false;
      $name = $req->user()->name;

      $note = new Note();
      $note->notes_id = $nid;
      $note->name = $name;
      $note->message = $message;
      $note->internal = $internal;
      $note->save();
      return back();
    }
}
