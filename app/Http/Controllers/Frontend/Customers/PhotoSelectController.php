<?php

namespace App\Http\Controllers\Frontend\Customers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Companies\Company;
use App\Models\Media\Gallery;
use App\Models\Media\PhotoSelect;
use Firebase\JWT\JWT;
use App\Events\PhotoSelected;
use App\Events\NewIntelReceived;
use App\Models\Media\Photo;

class PhotoSelectController extends Controller
{

    public function phpDetailApi($id, Request $req){
      $photo = Photo::find($id);
      if(!$photo){
        return response()->json([
            'fail' => true,
            'msg' => 'Invalid arguments! no photo'
        ], 401);
      }

      $detail = json_encode($req->input('detail'));

      $photo->details = $detail;
      $photo->update();

      event(new NewIntelReceived(NewIntelReceived::TYPE_PHOTO, $detail));

      return response()->json([
          'fail' => false,
          'msg' => 'good update'
      ]);
    }

    public function apiDlKeys($id, Request $req){
       \Debugbar::disable();
      $ps = Photoselect::find($id);

      if(!$ps){
        return response()->json([
            'fail' => true,
            'msg' => 'Invalid arguments! no ps'
        ], 401);
      }

      $selectedkeys = [];
      foreach (explode(',', $ps->selected) as $photoid) {
        $photo = Photo::find($photoid);
        if($photo){
          $selected[] = [
            'id' => $photo->id,
            'thumb' => $photo->thumbnail(),
            'src'       => $photo->url()
          ];

          $selectedkeys[] = $photo->file->s3key;
        }
      }

      return json_encode($selectedkeys);
    }

    public function show($id, Request $req){
      \Debugbar::disable();
      try{
        $info = JWT::decode($id, config('app.key'), array('HS256'));
      }catch(\Exception $e){
        return abort(404);
      }

      $ps = Photoselect::find($info->psid);

      if(!$ps){
        return abort(404);
      }

      $photos = [];
      foreach ($ps->gallery->photos as $photo) {
        $photos[] = [
          'id'  => $photo->id,
          'thumb' => $photo->thumbnail(),
          'src'       => $photo->withMark($ps->company->code, $photo->makeup ? $photo->makeup->name : '', $photo->photographer ? $photo->photographer->name : ''),
          'key' => $photo->file->s3key
        ];
      }

      $selected = false;
      if($ps->status == 'selected'){
        $selected = explode(',' , $ps->selected);
      }

      //dd($selected->count());

      return view('frontend.photoselect.show')
            ->with('selected', $selected)
            ->with('id', $id)
            ->with('ps', $ps)
            ->with('photos', $photos);
    }

    public function update($id, Request $req){
      \Debugbar::disable();
      try{
        $info = JWT::decode($id, config('app.key'), array('HS256'));
      }catch(\Exception $e){
        return response()->json([
            'fail' => true,
            'msg' => 'Invalid arguments! no token'
        ], 401);
      }

      $ps = Photoselect::find($info->psid);
      if(!$ps){
          return response()->json([
              'fail' => true,
              'msg' => 'Invalid arguments! no PS'
          ], 401);
      }

      if($ps->status != 'created' && $ps->status != 'viewed'){
        return response()->json([
            'fail' => true,
            'msg' => 'Not Able to select the photo due to previous selection'
        ], 401);
      }

      if(!$req->has('selected')){
        return response()->json([
            'fail' => true,
            'msg' => 'Invalid arguments! no selected'
        ], 401);
      }

      if(count($req->input('selected')) > $ps->max_allow){
        return response()->json([
            'fail' => true,
            'msg' => 'Invalid arguments! Exceed max',
            'selected' => $req->input('selected')
        ], 401);
      }

      $select = collect($req->input('selected'));
      $ogStatus = $ps->status;

      $ps->selected = $select->implode('id', ',');
      $ps->status = 'selected';

      if($req->has('comment')){
        $ps->remark = $req->input('comment');
      }

      $ps->update();

      event(new PhotoSelected($ps, $ogStatus));

      //event ps update

      return $ps;

    }

    public function store(Request $req){
      if(!$req->has('store') || !$req->has('gid')  || !$req->has('ms')){
        return response()->json([
            'fail' => true,
            'msg' => 'Invalid arguments!'
        ], 401);
      }

      $company = Company::find($req->input('store'));
      if(!$company){
        return response()->json([
            'fail' => true,
            'msg' => 'Invalid arguments! No Company'
        ], 401);
      }

      $gallery = Gallery::find($req->input('gid'));

      if(!$gallery){
        return response()->json([
            'fail' => true,
            'msg' => 'Invalid arguments! No Gallery'
        ], 401);
      }

      $photoselect = new PhotoSelect();

      $photoselect->user_id = $req->user()->id;
      $photoselect->gallery_id = $gallery->id;
      $photoselect->company_id = $company->id;
      $photoselect->status = 'created';
      $photoselect->max_allow = $req->input('ms');

      $photoselect->save();

      if($req->has('wm')){
        $photoselect->updateMeta('wm', 'true');
      }

      if($req->has('text')){
        $photoselect->updateMeta('text', 'true');
      }

      return $gallery->photoselects;
    }
}
