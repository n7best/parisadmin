<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Customers\Customer;
use Illuminate\Http\Request;
use App\Models\Raffles\RaffleLog;
use App\Models\Tasks\Task;

use Activity;
use App\Events\TaskStatusChanged;
use App\Models\Notes\Notes;
use Carbon\Carbon;
use App\Events\NewRaffleCreated;
use Twilio;
use \Services;
/**
 * Class FrontendController
 * @package App\Http\Controllers
 */
class MobileController extends Controller
{
    public $userService = null;

    public function __construct() {
        \Debugbar::disable();
        //$wechat = app('wechat');
        //$this->userService = $wechat->user;
    }
    /**
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $user = session('wechat.oauth_user'); // 拿到授权用户资料

        if(Customer::where('providers', '=', $user->id)->exists()){
            $customer = Customer::where('providers', '=', $user->id)->first();
            $customer->updateMeta('wc_nickname', $user->name);
            $customer->updateMeta('wc_avatar', $user->avatar);

            return view('frontend.mobile')->with('user', $customer)->with('uid', $user->id);
        }else{
            return view('frontend.mobile.verify')->with('uid', $user->id);
        }
    }

    public function mobileinvite()
    {
        $data = [
            'groom' => 'Wai',
            'bride' => 'Fiona'
        ];

        return view('frontend.wxinvite.style1')->withData(json_encode($data));
    }

    public function verify(Request $req)
    {

        $inputs = $req->all();
        if(!empty($inputs['vcode']) && !empty($inputs['uid']) && Customer::customerExists($inputs['vcode'])){
            $user = $this->userService->get($inputs['uid']);
            //dd($user);

            $customer = Customer::where('providers', '=', $inputs['vcode'])->first();
            $customer->providers = $inputs['uid'];
            $customer->update();

            $this->userService->remark($inputs['uid'], $customer->origin().' '.$customer->display_name);

            if(!empty($user['nickname'])) $customer->updateMeta('wc_nickname', $user['nickname']);
            if(!empty($user['avatar'])) $customer->updateMeta('wc_avatar', $user['avatar']);
            return [
                'result' => true,
                'data' => [
                    'name' => $customer->display_name
                ]
            ];
        }else{
            return response()->json([
                'fail' => true,
                'msg' => 'Invalid Code'
            ], 401);
        }
    }

    public function mobile_user_photoselected(Request $req){
        $user = session('wechat.oauth_user');
        $inputs = $req->all();
        if(!empty($inputs['uid']) && !empty($inputs['tid']) && !empty($inputs['selected']) && Customer::where('providers', '=', $inputs['uid'])->exists()){
            $customer = Customer::where('providers', '=', $inputs['uid'])->first();
            $task = Task::find($inputs['tid']);

            if($task->customer_id != $customer->id) {
                return response()->json([
                    'fail' => true,
                    'msg' => 'Unauthorized'
                ], 401);
            }

            $task->updateMeta('selected', $inputs['selected']);

            //change task status
            $notes_id = $task->notes_id();
            $notes = Notes::find($notes_id);

            $notes->write($customer->display_name, 'Updated Task <b>'.$task->display_name.'</b> Photo Selected: <br/> <b>'.implode(',', $inputs['selected']).'</b>');

            $task->status = 'pending';
            $task->update();

            $notes->write('System', 'Updated Task <b>'.$task->display_name.'</b> Status <br/> From: <b> Approved </b><br/> To: <b> Pending </b>');
            //log the pick
            return [
                'fail' => false,
                'msg' => 'Selected Succesfful!'
            ];
        }else{
            return response()->json([
                'fail' => true,
                'msg' => 'Select Fail!'
            ], 401);
        }
    }

    public function mobile_user_update(Request $req){
        $user = session('wechat.oauth_user');
        $inputs = $req->all();
        if(!empty($inputs['uid']) && Customer::where('providers', '=', $inputs['uid'])->exists()){
            $customer = Customer::where('providers', '=', $inputs['uid'])->first();
            //$customer->update();

            if(!empty($inputs['phone']) && $inputs['phone'] != $customer->getMeta('groomPhone')) $customer->updateMeta('groomPhone', $inputs['phone']);
            if(!empty($inputs['email']) && $inputs['email'] != $customer->getMeta('email')) $customer->updateMeta('email', $inputs['email']);
            if(!empty($inputs['notification']) && $inputs['notification'] != $customer->getMeta('notification')) $customer->updateMeta('notification', $inputs['notification']);

            return [
                'phone' => $customer->phone,
                'email' => $customer->getMeta('email'),
                'notification' => $customer->getMeta('notification')
            ];
        }else{
            return response()->json([
                'fail' => true,
                'msg' => 'Update Fail!'
            ], 401);
        }
    }

    private function getPrizes(){
        return collect([
            [
                'name' => '$100 Coupon',
                'odds' => 900,
                'img'  => '/img/raffle/pala100.png',
                'desc' => 'Paris Wedding &amp; Laffection Wedding $100 In-Store Coupon<br>巴黎婚纱，法国婚纱100美金礼劵<br>本礼品仅限于店内直接抽奖兑换<br><br><br>',
                'coupon' => [
                    'type' => 'cash',
                    'requirements' => 'package',
                    'value' => 100
                ]
            ],
            [
                'name' => '$200 Coupon',
                'odds' => 999,
                'img'  => '/img/raffle/pala200.png',
                'desc' => 'Paris Wedding &amp; Laffection Wedding $200 In-Store&nbsp;Coupon<br>巴黎婚纱，法国婚纱200美金礼劵<br>本礼品仅限于店内直接抽奖兑换<br>',
                'coupon' => [
                    'type' => 'cash',
                    'requirements' => 'package',
                    'value' => 200
                ]
            ],
            [
                'name' => '$150 Coupon',
                'odds' => 9999,
                'img'  => '/img/raffle/pala300.png',
                'desc' => 'Paris Wedding &amp; Laffection Wedding $150 In-Store&nbsp;Coupon<br>巴黎婚纱，法国婚纱150美金礼劵<br>本礼品仅限于店内直接抽奖兑换<br>',
                'coupon' => [
                    'type' => 'cash',
                    'requirements' => 'package',
                    'value' => 150
                ]
            ],
            [
                'name' => 'X-Banner',
                'odds' => 50,
                'img'  => '/img/raffle/xbanner.png',
                'desc' => 'Paris Wedding &amp; Laffection Wedding X-Banner In-Store&nbsp;Coupon<br>巴黎婚纱，法国婚纱X-Banner<br>本礼品仅限于店内直接抽奖兑换<br>',
                'coupon' => [
                    'type' => 'item',
                    'requirements' => 'package',
                    'value' => 'xbanner'
                ]
            ],
            [
                'name' => '精修底片10张',
                'odds' => 999,
                'img'  => '/img/raffle/lucky.png',
                'desc' => '精修底片10张<br>本礼品仅限于店内直接抽奖兑换<br>',
                'coupon' => [
                    'type' => 'item',
                    'requirements' => 'package',
                    'value' => 400
                ]
            ],
            [
                'name' => '摆台1个',
                'odds' => 999,
                'img'  => '/img/raffle/lucky.png',
                'desc' => '摆台1个<br>本礼品仅限于店内直接抽奖兑换<br>',
                'coupon' => [
                    'type' => 'item',
                    'requirements' => 'package',
                    'value' => 400
                ]
            ],
            [
                'name' => 'EastBuffet $200 Coupon',
                'odds' => 999,
                'img'  => '/img/raffle/eastbuffet.png',
                'desc' => 'EastBuffet $200&nbsp;Coupon<br>东王朝200美金礼劵<br>本礼品仅限于店内直接抽奖兑换<br>',
                'coupon' => [
                    'type' => 'item',
                    'requirements' => 'package',
                    'value' => 'eb200coupon'
                ]
            ],
            [
                'name' => '电子请帖',
                'odds' => 999,
                'img'  => '/img/raffle/invite.png',
                'desc' => 'Paris Wedding &amp; Laffection Wedding Digital Invitation Card<br>巴黎婚纱，法国婚纱电子请帖',
                'coupon' => [
                    'type' => 'item',
                    'requirements' => 'package',
                    'value' => 'digitalInvitation'
                ]
            ]
        ]);
    }

    public function raffle(Request $req){
        $user = session('mobileuser');
        //dd($user);
        if(empty($user)){
            $user = new \stdClass;
            $user->id = 'none';
        }

        $inputs = $req->all();
        $played = RaffleLog::where('providers', '=', $user->id)->exists();

        return view('frontend.mobile.raffle')
            ->withPlayed($played)
            ->withPrizes($this->getPrizes()->toJson())
            ->with('uid', $user->id)
            ->with('user', collect($user));
    }

    public function verifyNumber($number){
        // Your Account Sid and Auth Token from twilio.com/user/account
        //dd(config('twilio'));
        $sid = config('twilio.twilio.connections.twilio.sid');
        $token = config('twilio.twilio.connections.twilio.token');
        $client = new \Lookups_Services_Twilio($sid, $token);
        try {
            $number = $client->phone_numbers->get(
              "+1".$number,
              array("CountryCode" => "US", "Type" => "carrier")
            );

            $type = $number->carrier->type;

            return $type;
        } catch (\Services_Twilio_RestException $e) {
            return false;
        }
    }

    public function raffledraw(Request $req){
        //$user = session('wechat.oauth_user');
        $inputs = $req->all();
        $prizes = $this->getPrizes();

        if(!empty($inputs['uid']) && !empty($inputs['contacts'])){
            if(empty($inputs['contacts']['name'])) {
                return response()->json([
                    'fail' => true,
                    'msg' => 'Please fill the require information.'
                ], 401);
            }

            if(empty($inputs['contacts']['phone']) && empty($inputs['contacts']['email'])) {
                return response()->json([
                    'fail' => true,
                    'msg' => 'Please fill the require information.'
                ], 401);
            }

            if(RaffleLog::where('providers', '=', $inputs['contacts']['phone'])->exists()){
                return response()->json([
                    'fail' => true,
                    'msg' => 'You already played.'
                ], 401);
            }

            if(!$this->verifyNumber($inputs['contacts']['phone'])){
                return response()->json([
                    'fail' => true,
                    'msg' => 'Invalid Phone Number.'
                ], 401);
            }

            $winprize = false;
            $totalodds = $prizes->sum('odds');
            $winnum = mt_rand(0, $totalodds);

            $total = 0;

            if($winnum == 0) $winprize = $prizes->first();
            foreach ($prizes as $prize) {
                if($winnum > $total && $winnum < $total + $prize['odds']) $winprize = $prize;
                $total = $total + $prize['odds'];
            }

            if(!$winprize) {
                return response()->json([
                    'fail' => true,
                    'msg' => 'System Error.',
                    'winum' => $winnum,
                    'total' => $totalodds
                ], 401);
            }

            //record
            $log = new RaffleLog();
            $log->providers = $inputs['contacts']['phone'];
            $log->event_id = 1;
            $log->detail = $inputs['contacts']['name'].' won '.$winprize['name'];
            $log->save();

            /*if(empty($inputs['wc_user']) || empty($inputs['wc_user']['avatar']) || empty($inputs['wc_user']['nickname'])){
                $user = $this->userService->get($inputs['uid']);
            }else{
                $user = $inputs['wc_user'];
            }*/

            event(new NewRaffleCreated($winprize, $inputs['contacts']['phone'], $inputs['contacts']/*, $user*/));

            return [
                'name' => $winprize['name'],
                'wonodd' => $winnum
            ];
        }else{
            return response()->json([
                'fail' => true,
                'msg' => 'Please fill the require information.'
            ], 401);
        }
    }
}
