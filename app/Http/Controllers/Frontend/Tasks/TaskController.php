<?php

namespace App\Http\Controllers\Frontend\Tasks;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Access\User\User;
use App\Models\Tasks\Task;
use App\Models\Notes\Notes;
use Activity;
use Carbon\Carbon;
use Firebase\JWT\JWT;
use App\Events\TaskStatusChanged;

use \Eluceo\iCal\Component\Calendar as iCal;
use \Eluceo\iCal\Component\Event as iEvent;

class TaskController extends Controller
{

    public function index(Request $req){

      $uid = $req->user()->id;
      $rawtasks = $req->user()->tasks();

      $jwtToken = JWT::encode(['uid'=>$uid], config('app.key'));

      //get all tasks with related service
      $tasks = [];
      foreach ($rawtasks as $task) {
        $service = $task->service;
        $tasks[$service->catalog][] = [
          'detail'=>$task,
          'service'=>$service,
          'options'=>$task->getAllMeta()
        ];
      }
      return view('frontend.task.index')
        ->withTasks($tasks)
        ->with('jwtToken', $jwtToken);
    }

    public function apiStore(Request $req){
      if($req->has('token') && $req->has('data') && $req->has('tid')){
        $infos = JWT::decode($req->input('token') ,config('app.key'), array('HS256'));
        $user = User::find($infos->uid);
        $task = Task::find($req->input('tid'));

        if($user && $task){
          $order = $task->order;
          $data = $req->input('data');

          $notes_id = $task->notes_id();
          $notes = Notes::find($notes_id);

          $ogTitle = $task->getMeta('title');
          $ogAllday = $task->getMeta('allday', null);

          if(!empty($data['title']) && $data['title'] != $ogTitle){
            Activity::log([
                'contentId'   => $task->id,
                'contentType' => 'Task',
                'action'      => 'Update',
                'description' => 'Updated Task '.$task->display_name.' Title',
                'details'     => 'From: '.$ogTitle.' To: '.$data['title'],
            ]);

            $notes->write($user->name, 'Updated Task <b>'.$task->display_name.'</b> <br/> From: <b>'.$ogTitle.'</b><br/>To: <b>'.$data['title'].'</b>');

            $task->updateMeta('title', $data['title']);
          }
          debug($data['allday'], $ogAllday, $data['allday'] !== $ogAllday, $ogAllday == null);
          if(isset($data['allday']) && ($data['allday'] !== $ogAllday || $ogAllday == null)){
            Activity::log([
                'contentId'   => $task->id,
                'contentType' => 'Task',
                'action'      => 'Update',
                'description' => 'Updated Task '.$task->display_name.' Allday',
                'details'     => 'From: '.$ogAllday.' To: '.$data['allday'],
            ]);

            $notes->write($user->name, 'Updated Task <b>'.$task->display_name.' Allday</b> <br/> From: <b>'.$ogAllday.'</b><br/>To: <b>'.$data['allday'].'</b>');

            $task->updateMeta('allday', $data['allday']);
          }
          try {
              if(!empty($data['start']) && $data['start'] != $task->schedule_at && Carbon::parse($data['start']) !== false) {
                Activity::log([
                    'contentId'   => $task->id,
                    'contentType' => 'Task',
                    'action'      => 'Update',
                    'description' => 'Updated Task '.$task->display_name.' Start Datetime',
                    'details'     => 'From: '.$task->schedule_at.' To: '.Carbon::parse($data['start'])->toDateTimeString(),
                ]);

                $notes->write($user->name, 'Updated Task <b>'.$task->display_name.' Start Datetime</b> <br/> From: <b>'.$task->schedule_at.'</b><br/>To: <b>'.Carbon::parse($data['start'])->toDateTimeString().'</b>');

                $task->schedule_at = $data['start'];
                $task->update();
              }

              if(!empty($data['end']) && $data['end'] != $task->schedule_end && Carbon::parse($data['end']) !== false){
                Activity::log([
                    'contentId'   => $task->id,
                    'contentType' => 'Task',
                    'action'      => 'Update',
                    'description' => 'Updated Task '.$task->display_name.' End Datetime',
                    'details'     => 'From: '.$task->schedule_end.' To: '.Carbon::parse($data['end'])->toDateTimeString(),
                ]);

                $notes->write($user->name, 'Updated Task <b>'.$task->display_name.' End Datetime</b> <br/> From: <b>'.$task->schedule_end.'</b><br/>To: <b>'.Carbon::parse($data['end'])->toDateTimeString().'</b>');

                $task->schedule_end = $data['end'];
                $task->update();
              }
          }
          catch (\Exception $err) {
              return response()->json([
                  'fail' => true,
                  'msg' => 'Invalid Date'
              ], 422);
          }

          return [
            'fail' => false,
            'data' => [
              'title' => $task->getTitle(), //event title
              'allDay' => $task->isAllDay(), //full day event?
              'start' => $task->getStart()->toIso8601String(), //start time, must be a DateTime object or valid DateTime format (http://bit.ly/1z7QWbg)
              'end' => $task->getEnd()->toIso8601String(), //end time, must be a DateTime object or valid DateTime format (http://bit.ly/1z7QWbg),
              'id' => $task->id, //optional event ID
              'options' => $task->getEventOptions()
            ],
            'hrevent' => [
              'title' => $task->company_code.' '.implode(',', $task->assign_name), //event title
              'allDay' => $task->isAllDay(), //full day event?
              'start' => $task->getStart()->toIso8601String(), //start time (you can also use Carbon instead of DateTime)
              'end' => $task->getEnd()->toIso8601String(), //end time (you can also use Carbon instead of DateTime)
              'id' => $task->id.'hr', //optionally, you can specify an event ID,
              'options' => [
                'resourceId' => $task->resource_id,
                'type' => 'taskhr',
                'company' => $task->company_code
              ]
            ]
          ];

        }else{
          return response()->json([
              'fail' => true,
              'msg' => 'Unauthorized'
          ], 401);
        }
      }else{
        return response()->json([
            'fail' => true,
            'msg' => 'Invalid Code'
        ], 401);
      }
    }

    public function apiCal(Request $req){
      \Debugbar::disable();

      $tasks = Task::where('status','=','schedule')->orWhere('status', '=', 'reschedule')->get();

      $vCalendar = new iCal('pariswedding.nyc');

      foreach ($tasks as $task) {
        $vEvent = new iEvent();
        $vEvent
            ->setDtStart($task->getStart())
            ->setDtEnd($task->getEnd())
            ->setSummary($task->getTitle());

        if($task->isAllDay()){
          $vEvent->setNoTime(true);
        }else{
          $vEvent->setNoTime(false);
        }

        $vCalendar->addComponent($vEvent);
      }

      header('Content-Type: text/calendar; charset=utf-8');
      header('Content-Disposition: attachment; filename="cal.ics"');

      return $vCalendar->render();

    }

    public function apiShow(Request $req){
      $inputs = $req->all();
      $tid = $inputs['task_id'];
      $tid = str_replace('hr', '', $tid);
      $task = Task::find($tid);
      $customer = $task->customer;
      return [
        'title' => $task->getTitle(),
        'id' => $task->id,
        'status' => $task->status,
        'rid' => $task->order_rid,
        'name' => $task->display_name,
        'assignee' => implode(',', $task->assign_name),
        'options' => $task->getAllMeta(),
        'details' => $task->details,
        'customer' => $customer->display_name,
        'customerDetails' => $customer->getAllMeta()
      ];
    }

    public function checkUserTime(Request $req){
      $inputs = $req->all();
      $timeStart = Carbon::parse($inputs['time_start']);
      $timeEnd = Carbon::parse($inputs['time_end']);
      $tid = $inputs['task_id'];

      $user = $req->user();
      if($user->isAvailable($timeStart,$timeEnd,$tid)){
        return [
          'status' => 'success',
          'available' => true
        ];
      }else{
        return [
          'status' => 'error',
          'available' => false
        ];
      }
    }

    public function post(Request $req){
      $inputs = $req->all();
      $tid = $inputs['tid'];

      $task = Task::find($tid);
      $order = $task->order;
      $notes_id = $task->notes_id();
      $notes = Notes::find($notes_id);

      $assign_name = empty($inputs['assign_name']) ? false : implode(',', $inputs['assign_name']);
      $details = empty($inputs['details']) ? false : $inputs['details'];
      $status = empty($inputs['status']) ? false : $inputs['status'];
      $schedule_at = empty($inputs['schedule']) ? false :$inputs['schedule'];
      $schedule_end = empty($inputs['scheduleEnd']) ? false :$inputs['scheduleEnd'];
      $allday = isset($inputs['allday']) ? true : false;
      //$allday = true;
      $options = empty($inputs['options']) ? false : $inputs['options'];
      $ogAllday = $task->getMeta('allday', null);

      if($assign_name && $assign_name != $task->assign_users){
        $newnames = '';
        $oldnames = '';
        foreach ($inputs['assign_name'] as $newnameid) {
          $newnames = $newnames.uName($newnameid).' ';
        }
        foreach ($task->assign_name as $oldname) {
          $oldnames = $oldnames.$oldname.' ';
        }
        Activity::log([
            'contentId'   => $task->id,
            'contentType' => 'Task',
            'action'      => 'Update',
            'description' => 'Updated Task '.$task->display_name.' Assignee',
            'details'     => 'From: '.$oldnames.' To: '.$newnames,
        ]);
        $notes->write($req->user()->name, 'Updated Task <b>'.$task->display_name.'</b> Assignee <br/> From: <b>'.$oldnames.'</b><br/> To: <b>'.$newnames.'</b>');
        $task->assign_users = $assign_name;
        $task->update();
      }

      if($details && $details != $task->details){
        Activity::log([
            'contentId'   => $task->id,
            'contentType' => 'Task',
            'action'      => 'Update',
            'description' => 'Updated Task '.$task->display_name.' Details',
            'details'     => 'From: '.$task->details.' To: '.$details,
        ]);
        $notes->write($req->user()->name, 'Updated Task <b>'.$task->display_name.'</b> Details <br/> From: <b>'.$task->details.'</b><br/>To: <b>'.$details.'</b>');
        $task->details = $details;
        $task->update();
      }

      if($status && $status != $task->status){
        $statusBefore = $task->status;
        Activity::log([
            'contentId'   => $task->id,
            'contentType' => 'Task',
            'action'      => 'Update',
            'description' => 'Updated a Task '.$task->display_name.' Status',
            'details'     => 'From: '.$task->status.' To: '.$status,
        ]);
        $notes->write($req->user()->name, 'Updated Task <b>'.$task->display_name.'</b> Status <br/> From: <b>'.$task->status.'</b><br/>To: <b>'.$status.'</b>');
        $task->status = $status;
        $task->update();
        event(new TaskStatusChanged($task, $statusBefore));
      }

      debug($allday, $ogAllday);
      if(($allday !== $ogAllday) || $ogAllday == null){
        $allday = $allday ? 'true' : 'false';
        debug('here', $allday);
        Activity::log([
            'contentId'   => $task->id,
            'contentType' => 'Task',
            'action'      => 'Update',
            'description' => 'Updated Task '.$task->display_name.' Allday',
            'details'     => 'From: '.$ogAllday.' To: '.$allday,
        ]);

        $notes->write($req->user()->name, 'Updated Task <b>'.$task->display_name.' Allday</b> <br/> From: <b>'.$ogAllday.'</b><br/>To: <b>'.$allday.'</b>');

        $task->updateMeta('allday', $allday);
      }

      if($schedule_at && $schedule_at != $task->schedule_at && Carbon::parse($schedule_at) !== false) {
        Activity::log([
            'contentId'   => $task->id,
            'contentType' => 'Task',
            'action'      => 'Update',
            'description' => 'Updated Task '.$task->display_name.' Start Datetime',
            'details'     => 'From: '.$task->schedule_at.' To: '.Carbon::parse($schedule_at)->toDateTimeString(),
        ]);

        $notes->write($req->user()->name, 'Updated Task <b>'.$task->display_name.' Start Datetime</b> <br/> From: <b>'.$task->schedule_at.'</b><br/>To: <b>'.Carbon::parse($schedule_at)->toDateTimeString().'</b>');

        $task->schedule_at = $schedule_at;
        $task->update();
      }

      if($schedule_end && $schedule_end != $task->schedule_end && Carbon::parse($schedule_end) !== false){
        Activity::log([
            'contentId'   => $task->id,
            'contentType' => 'Task',
            'action'      => 'Update',
            'description' => 'Updated Task '.$task->display_name.' End Datetime',
            'details'     => 'From: '.$task->schedule_end.' To: '.Carbon::parse($schedule_end)->toDateTimeString(),
        ]);

        $notes->write($req->user()->name, 'Updated Task <b>'.$task->display_name.' End Datetime</b> <br/> From: <b>'.$task->schedule_end.'</b><br/>To: <b>'.Carbon::parse($schedule_end)->toDateTimeString().'</b>');

        $task->schedule_end = $schedule_end;
        $task->update();
      }


      if($options){
        $ogOptions = $task->getAllMeta();
        foreach ($options as $type => $value) {
          if(!empty($value) && (empty($ogOptions['op-'.$type]) || $value != $ogOptions['op-'.$type]) ){
            $ogValue = empty($ogOptions['op-'.$type]) ? 'Empty' : $ogOptions['op-'.$type];
            Activity::log([
                'contentId'   => $task->id,
                'contentType' => 'Task',
                'action'      => 'Update',
                'description' => 'Updated a Task '.$task->display_name.' Option '.$type,
                'details'     => 'From: ['.$ogValue.'] To: ['.$value.']',
            ]);

            $notes->write($req->user()->name, 'Updated a Task <b>'.$task->display_name.'</b> Option <b>'.$type.'</b> From: [ <b>'.$ogValue.'</b> ] To: [ <b>'.$value.'</b>]');

            $task->updateMeta('op-'.$type, $value);
          }
        }
      }

      $order->touch();
      return back();
    }

}
