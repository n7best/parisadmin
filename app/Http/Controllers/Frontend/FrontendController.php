<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Tasks\Task;
use App\Models\Access\User\Hrs;
use Illuminate\Http\Request;
use Firebase\JWT\JWT;
/**
 * Class FrontendController
 * @package App\Http\Controllers
 */
class FrontendController extends Controller
{
    /**
     * @return \Illuminate\View\View
     */
    public function index(Request $req)
    {
        $events = [];
        $taskhrs = [];

        $tasks = Task::where('status','=','schedule')->orWhere('status', '=', 'reschedule')->whereNotNull('schedule_at')->get();
        $hrs = Hrs::where('status','=', 'approve')->get();

        $calendar = \Calendar::addEvents($hrs);

        foreach ($tasks as $task) {
          $taske = \Calendar::event(
              $task->company_code.' '.implode(',', $task->assign_name), //event title
              $task->isAllDay(), //full day event?
              $task->getStart(), //start time (you can also use Carbon instead of DateTime)
              $task->getEnd(), //end time (you can also use Carbon instead of DateTime)
              $task->id.'hr', //optionally, you can specify an event ID,
              [
                'resourceId' => $task->resource_id,
                'type' => 'taskhr',
                'company' => $task->company_code
              ]
          );

          $taskhrs[] = $taske;

        }

        $calendar->addEvents($taskhrs);

        $calendar->addEvents($tasks);


        $calendar->setOptions([
          'header' => [
            'left' => 'prev,next today',
            'center' => 'title',
            'right' => 'month,timelineSevenDay,agendaDay'
          ],
          'views' => [
            'timelineSevenDay' => [
              'type' => 'timelineMonth',
              'duration' => [
                'days' => 7
              ],
              'slotDuration' => '24:00',
              'groupByResource' => true,
              'resourceLabelText' => 'Departments',
              'resourceAreaWidth' => '15%'
            ]
          ],
          'schedulerLicenseKey' => 'CC-Attribution-NonCommercial-NoDerivatives',
          'defaultView' => 'timelineSevenDay',
          'resources' => [
            [
              'id' => 'hr',
              'title' => 'HR'
            ],
            [
              'id' => 'wd',
              'title' => 'Wedding Day'
            ],
            [
              'id' => 'pw',
              'title' => 'Pre Wedding'
            ],
            [
              'id' => 'ot',
              'title' => 'Other'
            ],
            [
              'id' => 'dr',
              'title' => 'Dress'
            ],
            [
              'id' => 'pp',
              'title' => 'Post Prodctuion'
            ]
          ]
        ]);

        $calendar->setCallbacks([
          'dayClick' => '
            function(date, jsEvent, view, resourceObj) {
              console.log(\'dayclick\', date, jsEvent, resourceObj)
              // Clicked on the entire day

              console.log(\'dtart get find\', date)
              $(\'#calendar-'.$calendar->getId().'\').fullCalendar(\'changeView\', \'agendaDay\'/* or \'basicDay\' */);
              $(\'#calendar-'.$calendar->getId().'\').fullCalendar(\'gotoDate\', date);
            }
          ',
          'eventRender' => '
            function(event, element, view) {
              var ntoday = new Date().getTime();
              var eventEnd = moment( event.end ).valueOf();
              var eventStart = moment( event.start ).valueOf();
              if (!event.end){
                  if (eventStart < ntoday){
                      element.addClass("past-event");
                      element.children().addClass("past-event");
                  }
              } else {
                  if (eventEnd < ntoday){
                      element.addClass("past-event");
                      element.children().addClass("past-event");
                  }
              }

              function updateDateTime(start, end, allday=false){
                console.log("time", start, end, allday)
                if(!end) end = start;
                if(allday){
                    $(\'#eventAllday\').prop("checked", true)

                    $(\'#detailDateTime .time.start\').hide()
                    $(\'#detailDateTime .time.end\').hide()

                    $(\'#detailDateTime .date.start\').datepicker(\'update\', start.toDate());
                    $(\'#detailDateTime .date.end\').datepicker(\'update\', end.toDate());
                    $(\'#detailDateTime\').datepair(\'refresh\');

                }else{
                    $(\'#eventAllday\').prop("checked", false)

                    $(\'#detailDateTime .time.start\').show()
                    $(\'#detailDateTime .time.end\').show()

                    $(\'#detailDateTime .date.start\').datepicker(\'update\', start.toDate());
                    $(\'#detailDateTime .date.end\').datepicker(\'update\', end.toDate());
                    $(\'#detailDateTime .time.start\').timepicker(\'setTime\', start.toDate());
                    $(\'#detailDateTime .time.end\').timepicker(\'setTime\', end.toDate());
                    $(\'#detailDateTime\').datepair(\'refresh\');
                }
              }
              console.log(event)

              if(event.type == \'task\'){
                element.click(function() {

                    function tplRow(label, message){
                        return `
                        <div class="form-group" style="margin-bottom: 0px;">
                          <label class="col-sm-3 control-label">${label}</label>
                          <div class="col-sm-8=9">
                            <h5><span class="label label-default">${message}</span></h5>
                          </div>
                        </div>
                        `;
                    }

                    $(\'#detailBody\').hide();
                    $(\'#detailSpinner\').show();
                    element.attr(\'href\', \'javascript:void(0);\');

                    $.ajax({
                      url: "'.route('api.tasks').'?task_id=" + event.id
                    })
                    .then(function(data){

                      var message = tplRow("Status", data.status) + tplRow("Assignee", data.assignee)

                      if(data.options){
                        Object.keys(data.options).forEach(function(key){
                            if(key == \'title\') return;
                            var _opt = data.options[key];
                            var _optTitle = key.replace(/op-/g, \'\')

                            message = message + tplRow(_optTitle, _opt)
                        })
                      }
                      if(data.details){
                        message = message + tplRow("Details", data.details)
                      }
                      if(data.customer) {
                        let customerContent = \'\';
                        if(data.customerDetails) {
                          Object.keys(data.customerDetails).forEach(key=>{
                              let _detail = data.customerDetails[key];
                              customerContent = customerContent + `<p>${key}: <b> ${_detail} </b></p>`
                          })
                        }
                        message = message + `
                          <div class="media" style="border-top: 1px #eee solid; padding-top: 15px;">
                            <div class="media-left">
                              <a href="#">
                                <img class="media-object" src="https://api.adorable.io/avatars/50/${data.customer}.png" alt="...">
                              </a>
                            </div>
                            <div class="media-body">
                              <h4 class="media-heading">${data.customer}</h4>
                              ${customerContent}
                            </div>
                          </div>
                        `
                      }
                      $(\'#detailBody\').html(message);
                      $(\'#detailSpinner\').hide();
                      $(\'#detailBody\').show();
                    }, function(xhr, status, error) {
                      console.log(error);
                      alert(error);
                    })

                    $(\'#eventTitle\').val(event.title);
                    $(\'#modalBody\').html(event.description);
                    $(\'#eventUrl\').attr(\'href\',event.url);
                    updateDateTime(event.start, event.end, event.allDay);
                    $(\'#fullCalModal\').modal();

                    $(\'#eventAllday\').change(function(){
                        updateDateTime(event.start, event.end, $(this).prop(\'checked\'));
                    })

                    $(\'#eventSave\').off(\'click\')
                    $(\'#eventSave\').click(function(){
                        var btn = $(this);
                        btn.button(\'loading\');

                        let allday = $(\'#eventAllday\').prop(\'checked\');
                        let startdatetime = moment(`${$(\'#detailDateTime .date.start\').val()} ${ allday ? "" : $(\'#detailDateTime .time.start\').val()}`,\'MM/DD/YYYY h:mm a\');
                        let enddatetime = moment(`${$(\'#detailDateTime .date.end\').val()} ${ allday ? "" : $(\'#detailDateTime .time.end\').val()}`,\'MM/DD/YYYY h:mm a\');

                        console.log(startdatetime, enddatetime);

                        $.ajax({
                          method: \'POST\',
                          url: "'.route('api.updatetask').'",
                          data: {
                            token: __token,
                            data: {
                                title: $(\'#eventTitle\').val(),
                                allday: allday,
                                start: startdatetime.format(),
                                end: enddatetime.format()
                            },
                            tid: event.id
                          }
                        })
                        .then(function(data){
                          btn.button(\'reset\');
                          if(!data.fail){
                            var newevent = $.extend({},event, data.data, data.data.options);
                            console.log(newevent)
                            newevent.start = moment(newevent.start)
                            newevent.end = moment(newevent.end)

                            updateDateTime(newevent.start, newevent.end, newevent.allDay);
                            $(\'#calendar-'.$calendar->getId().'\').fullCalendar(\'updateEvent\', newevent);

                            if(data.hrevent){
                                var hrevent = $.extend({},$(\'#calendar-'.$calendar->getId().'\').fullCalendar(\'clientEvents\', newevent.id + "hr")[0], data.hrevent, data.hrevent.options);
                                console.log("hrevent", hrevent);
                                $(\'#calendar-'.$calendar->getId().'\').fullCalendar(\'updateEvent\', hrevent);
                            }
                          }
                        }, function(xhr, status, error) {
                          btn.button(\'reset\');
                          console.log(error);
                          alert(error);
                        })
                    })
                });
              }

              if(event.type != \'hr\'){
                element.qtip({
                  style: {
                    classes: \'qtip-bootstrap\'
                  },
                  content: {
                    text: function(e, api) {
                      $.ajax({
                        url: "'.route('api.tasks').'?task_id=" + event.id
                      })
                      .then(function(data){
                        api.set(\'content.title\', data.title);

                        var message = "<p>Status: <b>" + data.status + "</b></p>" +
                                      "<p>Assignee: <b>" + data.assignee+ "</b></p>"

                        if(data.options){
                          Object.keys(data.options).forEach(function(key){
                              if(key == \'title\') return;
                              var _opt = data.options[key];
                              var _optTitle = key.replace(/op-/g, \'\')

                              message = message + `<p>${_optTitle}: <b> ${_opt} </b></p>`
                          })
                        }
                        if(data.details){
                          message = message + `<p>Details: <b> ${data.details} </b></p>`
                        }
                        
                        if(data.customer) {
                          let customerContent = \'\';
                          if(data.customerDetails) {
                            Object.keys(data.customerDetails).forEach(key=>{
                                let _detail = data.customerDetails[key];
                                customerContent = customerContent + `<p>${key}: <b> ${_detail} </b></p>`
                            })
                          }
                          message = message + `
                            <div class="media" style="border-top: 1px #eee solid; padding-top: 3px;">
                              <div class="media-left">
                                <a href="#">
                                  <img class="media-object" src="https://api.adorable.io/avatars/50/${data.customer}.png" alt="...">
                                </a>
                              </div>
                              <div class="media-body">
                                <h4 class="media-heading">${data.customer}</h4>
                                ${customerContent}
                              </div>
                            </div>
                          `
                        }
                        api.set(\'content.text\', message);
                      }, function(xhr, status, error) {
                        api.set(\'content.text\', status + \': \' + error);
                      })

                      return \'Loading...\';
                    }
                  }
                })
              }
            }
          '
        ]);

        $uid = $req->user()->id;
        $jwtToken = JWT::encode(['uid'=>$uid], config('app.key'));

        return view('frontend.index')
            ->with('jwtToken', $jwtToken)
            ->with('calendar', $calendar);
    }

    /**
     * @return \Illuminate\View\View
     */
    public function macros()
    {
        return view('frontend.macros');
    }

    /**
     * @return \Illuminate\View\View
     */
    public function mobile()
    {
        \Debugbar::disable();
        return view('frontend.mobile');
    }
}
