<?php

namespace App\Http\Controllers\Frontend\EInvites;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\EInvites\EInvite;
use Firebase\JWT\JWT;
use Flash;
use App\Models\EInvites\EInviteLog;
use App\Models\EInvites\EInviteDonation;
use Activity;

class EInviteController extends Controller
{
    public function __construct() {
        \Debugbar::disable();
        $wechat = app('wechat');
        $this->wcjs = $wechat->js;
        $this->userService = $wechat->user;
    }

    public function apiDonations($id, Request $req) {
      if($id){
        $einvite = EInvite::with('donations')->find($id);
        return $einvite->donations->sortByDesc('amount')->values()->all();
      }else{
        return [];
      }
    }

    public function storeDonation(Request $req) {
      if($req->has('type') && $req->has('token') && $req->has('name') && $req->has('amount') ){
        if($req->input('type') == 'stripe'){
          if(!$req->has('stoken')){
            return response()->json([
                'fail' => true,
                'msg' => 'No stripe token'
            ], 401);
          }

          $infos = JWT::decode($req->input('token') ,config('app.key'), array('HS256'));

          $einvite = EInvite::find($infos->eid);
          $options = json_decode(json_decode($einvite->options));

          //dd(json_decode($options));

          if($options->payment->stripeKey == 'pk_live_15wtsI34uawgeRbkPavxCRwa'){
            \Stripe\Stripe::setApiKey("sk_live_Qoa0cM98ZyQIW16qJeHgQ371");
          }else{
            \Stripe\Stripe::setApiKey("sk_test_WYcGpzcjjAca8oETZUBOsetT");
          }

          /*$token = \Stripe\Token::create(array(
            "card" => array(
              "number" => "4242424242424242",
              "exp_month" => 1,
              "exp_year" => 2017,
              "cvc" => "314"
            )
          ));*/


          //dd();

          $result = \Stripe\Charge::create(array(
            "amount" => $req->input('amount'),
            "currency" => "usd",
            "source" => $req->input('stoken'), // obtained with Stripe.js $token, //
            "description" => "Donations to Wedding"
          ));

          if($result->status !== 'succeeded') {
            response()->json([
                'fail' => true,
                'msg' => 'Payment Fail'
            ], 401);
          }

          Activity::log([
              'contentId'   => 0,
              'contentType' => 'Payment Donation',
              'action'      => 'Create Payment',
              'description' => 'Token Log',
              'details'     => json_encode($result),
          ]);

          $donation = new EInviteDonation();
          $donation->einvite_id = $infos->eid;
          $donation->type = $req->input('type');
          $donation->amount = ceil($req->input('amount') / 100);
          $donation->name = $req->input('name');
          $donation->save();

          $alldonations = EInviteDonation::where('einvite_id', '=', $infos->eid)->get();

          return [
            'fail' => false,
            'data' => $alldonations
          ];
        }else{
          $infos = JWT::decode($req->input('token') ,config('app.key'), array('HS256'));

          $donation = new EInviteDonation();
          $donation->einvite_id = $infos->eid;
          $donation->type = $req->input('type');
          $donation->amount = $req->input('amount');
          $donation->name = $req->input('name');
          $donation->save();

          $alldonations = EInviteDonation::where('einvite_id', '=', $infos->eid)->get();

          return [
            'fail' => false,
            'data' => $alldonations
          ];
        }

      }else{
        return response()->json([
            'fail' => true,
            'msg' => 'Invalid Code'
        ], 401);
      }
    }

    public function apiLogs($id, Request $req) {
      if($id){
        $einvite = EInvite::with('logs')->find($id);
        return $einvite->logs;
      }else{
        return [];
      }

    }

    public function storeLog(Request $req) {

      if($req->has('type') && $req->has('token') && $req->has('message') && $req->has('time') ){
        $infos = JWT::decode($req->input('token') ,config('app.key'), array('HS256'));

        $newlog = new EInviteLog();
        $newlog->einvite_id = $infos->eid;
        $newlog->time = $req->input('time');
        $newlog->message = $req->input('message');
        $newlog->type = $req->input('type');
        $newlog->uid = $infos->uid;
        $newlog->save();

        $alllogs = EInviteLog::where('einvite_id', '=', $infos->eid)->get();
        return [
          'fail' => false,
          'data' => $alllogs
        ];
      }else{
        return response()->json([
            'fail' => true,
            'msg' => 'Invalid Code'
        ], 401);
      }

    }

    public function index(){
      $invites = EInvite::all();
      return view('frontend.einvites.index')->with('einvites', $invites);
    }

    public function create(Request $req){
      $uid = $req->user()->id;
      $jwtToken = JWT::encode(['uid'=>$uid], config('app.key'));

      return view('frontend.einvites.create')
              ->with('jwtToken', $jwtToken);
    }

    public function update($id, Request $req){
      $uid = $req->user()->id;
      $options = $req->get('options');
      $einvite = EInvite::find($id);
      $einvite->options = json_encode($options);
      $einvite->update();

      Flash::success('Einvite update successfully.');

      return redirect()->route('einvite.index');
    }

    public function edit($id, Request $req){
      $einvite = EInvite::with('donations', 'logs')->find($id);
      $uid = $req->user()->id;
      $jwtToken = JWT::encode(['uid'=>$uid, 'eid'=> $einvite->id], config('app.key'));
      return view('frontend.einvites.edit')
              ->with('einvite', $einvite)
              ->with('jwtToken', $jwtToken);
    }

    public function store(Request $req){
      $options = $req->get('options');
      $invite = new EInvite();
      $invite->options = json_encode($options);
      $invite->save();

      Flash::success('Einvite created successfully.');

      return redirect()->route('einvite.index');
    }

    public function display($id, Request $req){
      $user = session('wechat.oauth_user');
      if(!$user->id) return 'System Error, Please Refresh';

      if(!empty($user['nickname'])){
        $userName = $user['nickname'];
      }else{
        $userName = 'Guest';
      }

      $einvite = EInvite::with('logs','donations')->find($id);

      $checkedIn = "false";
      foreach ($einvite->logs as $log) {
        if($log->uid == $user->id) $checkedIn = "true";
      }
      $jwtToken = JWT::encode(['eid'=> $einvite->id, 'uid' => $user->id ], config('app.key'));
      return view('frontend.wxinvite.style1')
        ->with('userName', $userName)
        ->with('jwtToken', $jwtToken)
        ->with('checkedIn', $checkedIn)
        ->with('wcjs', $this->wcjs)
        ->with('einvite', $einvite);
    }
}
