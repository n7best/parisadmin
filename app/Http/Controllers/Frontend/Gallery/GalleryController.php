<?php

namespace App\Http\Controllers\Frontend\Gallery;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Media\Gallery;
use App\Models\Customers\Customer;
use Flash;
use Firebase\JWT\JWT;
use App\Models\Companies\Company;
use App\Models\Media\Photo;
use Carbon\Carbon;


class GalleryController extends Controller
{

    public function transfer(Request $req){

      $jwtToken = JWT::encode(['uid'=>$req->user()->id, ], config('app.key'));
      $companies = Company::all();

      return view('frontend.gallery.transfer')
              ->with('companies', $companies)
              ->with('jwtToken', $jwtToken);
    }

    public function create(Request $req){
      return view('frontend.gallery.create');
    }

    public function apiPhotoSelect($id, Request $req){
      $gallery = Gallery::find($id);
      $output = [];

      foreach ($gallery->photoselects as $ps) {

        if($ps->status == 'selected' && !empty($ps->selected)){
          $selected = [];
          //$selectedkeys = [];
          foreach (explode(',', $ps->selected) as $photoid) {
            $photo = Photo::find($photoid);
            if($photo){
              $selected[] = [
                'id' => $photo->id,
                'thumb' => $photo->thumbnail(),
                'src'       => $photo->url(),
                'key' => $photo->file->s3key
              ];

              //$selectedkeys[] = $photo->file->s3key;
            }
          }
        }else{
          $selected = [];
          //$selectedkeys = [];
        }
        $output[] = [
          'id' => $ps->id,
          'status' => $ps->status,
          'selected' => $selected,
          'zips' => $ps->zips(),
          //'selectedkeys' => $selectedkeys,
          'remark'  => $ps->remark,
          'link'  => JWT::encode(['psid' => $ps->id], config('app.key'))
        ];
      }
      return json_encode($output);
    }

    public function apishow($id, Request $req){
      $output = [];
      foreach (Gallery::find($id)->photos as $photo) {
        $output[] = [
          'thumb' => $photo->thumbnail(),
          'src'       => $photo->url()
        ];
        //$photo->withMark('la', $photo->makeup ? $photo->makeup->name : '', $photo->photographer ? $photo->photographer->name : '')
      }

      return json_encode($output);
    }

    public function edit($id, Request $req){
      \Debugbar::getJavascriptRenderer()->setBindAjaxHandlerToXHR(false);
      $gallery = Gallery::find($id);
      $uid = $req->user()->id;
      $jwtToken = JWT::encode(['uid'=>$uid, 'gid'=> $gallery->id], config('app.key'));
      $companies = Company::all();

      return view('frontend.gallery.edit')
              ->with('companies', $companies)
              ->withGallery($gallery)
              ->with('jwtToken', $jwtToken);
    }

    public function update($id, Request $req){

      if(!$req->has('name')){
        return back()->withFlashDanger('Please fill require information');
      }

      if($req->has('customer') && !Customer::find($req->input('customer'))->exists()){
        return back()->withFlashDanger('Not Customer Found');
      }

      $gallery = Gallery::find($id);
      $gallery->name = $req->input('name');
      if($req->has('customer')){
        $gallery->customer_id = $req->input('customer');
      }
      $gallery->update();

      if($req->has('orderid')){
        $gallery->updateMeta('orderid', $req->input('orderid'));
      }

      Flash::success('Gallery update successfully.');

      return redirect(route('gallery.edit', $gallery->id));

    }

    public function store(Request $req){
      if($req->ajax()){

        $name = 'Transfer by '.$req->user()->name.' '.Carbon::now()->toDateTimeString();
        $gallery = new Gallery();
        $gallery->name = $name;
        $gallery->user_id = $req->user()->id;
        $gallery->save();

        return $gallery;
      }

      if(!$req->has('name')){
        return back()->withFlashDanger('Please fill require information');
      }

      if($req->has('customer') && !Customer::find($req->input('customer'))->exists()){
        return back()->withFlashDanger('Not Customer Found');
      }

      $gallery = new Gallery();
      $gallery->name = $req->input('name');
      $gallery->user_id = $req->user()->id;

      if($req->has('customer')){
        $gallery->customer_id = $req->input('customer');
      }

      $gallery->save();

      if($req->has('orderid')){
        $gallery->updateMeta('orderid', $req->input('orderid'));
      }

      Flash::success('Gallery create successfully.');

      return redirect(route('gallery.index'));
    }

    public function index(Request $req){
      $galleries = Gallery::all();

      return view('frontend.gallery.index')
              ->with('galleries', $galleries);
    }
}
