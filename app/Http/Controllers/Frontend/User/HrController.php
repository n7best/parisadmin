<?php

namespace App\Http\Controllers\Frontend\User;

use App\Http\Controllers\Controller;
use App\Repositories\Frontend\User\UserContract;
use Illuminate\Http\Request;
use App\Models\Access\User\Hrs;
use Carbon\Carbon;
use Flash;
/**
 * Class ProfileController
 * @package App\Http\Controllers\Frontend
 */
class HrController extends Controller
{

    public function create(Request $req)
    {
        return view('frontend.hr.create');
    }

    public function store(Request $req)
    {
        $inputs = $req->all();
        $user = $req->user();

        $hr = new Hrs();
        $hr->user_id = $user->id;
        $hr->status = 'Pending';
        $hr->type = $inputs['type'];
        $hr->start_at = Carbon::parse($inputs['hrStart'])->toDateTimeString();
        $hr->end_at = Carbon::parse($inputs['hrEnd'])->toDateTimeString();
        $hr->remark = $inputs['reason'];

        $hr->save();
        return redirect()->route('hr.index')->withFlashSuccess('Request Successfully Created');
    }

    public function index(Request $req)
    {
        if(access()->allow('approve-hr')){
            $hrs = Hrs::all();
        }else{
            $hrs = Hrs::where('user_id','=',$req->user()->id)->get();
        }
        return view('frontend.hr.index')
                ->with('hrs', $hrs);
    }
    /**
     * @return mixed
     */
    public function edit($hrid, Request $req)
    {
        $user = $req->user();
        $hr = Hrs::find($hrid);

        if($hr->user_id == $user->id || $user->allow('approve-hr')){
            return view('frontend.hr.edit')
                ->withHr($hr);
        }
        return redirect()->route('hr.index')->withFlashDanger('Not Enough Permission');
    }

    /**
     * @param  UserContract         $user
     * @param  UpdateProfileRequest $request
     * @return mixed
     */
    public function update($hrid, Request $req)
    {
        $inputs = $req->all();
        $user = $req->user();
        $hrs = Hrs::find($hrid);

        if(Carbon::parse($inputs['hrStart'])->toDateTimeString() !== $hrs->start_at) {
            $hrs->start_at = Carbon::parse($inputs['hrStart'])->toDateTimeString();
        }

        if(Carbon::parse($inputs['hrEnd'])->toDateTimeString() !== $hrs->end_at) {
            $hrs->end_at = Carbon::parse($inputs['hrEnd'])->toDateTimeString();
        }

        if($inputs['reason'] !== $hrs->remark) {
            $hrs->remark = $inputs['reason'];
        }

        if($inputs['type'] !== $hrs->type) {
            $hrs->type = $inputs['type'];
        }

        if(!empty($inputs['status']) && $inputs['status'] !== $hrs->status) {
            $hrs->status = $inputs['status'];
        }

        $hrs->update();

        return redirect()->route('hr.index')->withFlashSuccess('Update Successfull.');
    }

    public function destroy($hrid, Request $req)
    {
        $inputs = $req->all();
        $user = $req->user();
        $hrs = Hrs::find($hrid);

        if($hrs->user_id == $user->id || $user->allow('approve-hr')){
            $hrs->delete();
            return redirect()->route('hr.index')->withFlashSuccess('Deleted Successfull.');
        }

        return redirect()->route('hr.index')->withFlashDanger('Not Enough Permission');
    }
}