<?php

namespace App\Http\Controllers\Backend\Coupon;

use App\Http\Requests;
use App\Http\Requests\Backend\Coupon\CreateCouponRequest;
use App\Http\Requests\Backend\Coupon\UpdateCouponRequest;
use App\Http\Requests\Backend\Coupon\DeleteCouponRequest;
use App\Repositories\Backend\Coupon\CouponRepository;
use Illuminate\Http\Request;
use Flash;
use InfyOm\Generator\Controller\AppBaseController;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use Activity;

class CouponController extends AppBaseController
{
    /** @var  couponRepository */
    private $couponRepository;

    function __construct(CouponRepository $couponRepo)
    {
        $this->couponRepository = $couponRepo;
    }

    /**
     * Display a listing of the coupon.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->couponRepository->pushCriteria(new RequestCriteria($request));
        $coupons = $this->couponRepository->all();

        return view('backend.coupons.index')
            ->with('coupons', $coupons);
    }

    public function apiIndex(Request $request)
    {
        $this->couponRepository->pushCriteria(new RequestCriteria($request));
        $coupons = $this->couponRepository->all();

        $output = [];

        foreach ($coupons as $coupon) {
            $services = [];
            $serviceIds = explode(",", $coupon->services);
            foreach ($serviceIds as $serviceId) {
                $services[] = $this->services->whereLoose('id', $serviceId)->first()->name;
            }

            $output[] = [
                'name' => $coupon->name,
                'display_name' => $coupon->display_name,
                'price' => $coupon->price,
                'services' => $services,
            ];

        }
        return $output;
    }

    /**
     * Show the form for creating a new coupon.
     *
     * @return Response
     */
    public function create()
    {
        return view('backend.coupons.create');
    }

    /**
     * Store a newly created coupon in storage.
     *
     * @param CreatecouponRequest $request
     *
     * @return Response
     */
    public function store(CreateCouponRequest $request)
    {
        $input = $request->all();

        $coupon = $this->couponRepository->create($input);

        Activity::log([
          'contentId'   => $coupon->id,
          'contentType' => 'coupon',
          'action'      => 'Create',
          'description' => 'created a coupon:'.$coupon->id,
          'details'     => 'Username: '.$request->user()->name,
        ]);

        Flash::success('coupon saved successfully.');

        return redirect(route('admin.coupons.index'));
    }

    /**
     * Display the specified coupon.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $coupon = $this->couponRepository->findWithoutFail($id);

        if (empty($coupon)) {
            Flash::error('coupon not found');

            return redirect(route('admin.coupons.index'));
        }

        return view('backend.coupons.show')->with('coupon', $coupon);
    }

    /**
     * Show the form for editing the specified coupon.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $coupon = $this->couponRepository->findWithoutFail($id);

        if (empty($coupon)) {
            Flash::error('coupon not found');

            return redirect(route('admin.coupons.index'));
        }

        return view('backend.coupons.edit')
            ->with('coupon', $coupon);
    }

    /**
     * Update the specified coupon in storage.
     *
     * @param  int              $id
     * @param UpdatecouponRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateCouponRequest $request)
    {
        $coupon = $this->couponRepository->findWithoutFail($id);

        if (empty($coupon)) {
            Flash::error('coupon not found');

            return redirect(route('admin.coupons.index'));
        }

        $coupon = $this->couponRepository->update($request->all(), $id);


        Activity::log([
          'contentId'   => $coupon->id,
          'contentType' => 'coupon',
          'action'      => 'Edit',
          'description' => 'Editd a coupon:'.$coupon->id,
          'details'     => 'Username: '.$request->user()->name,
        ]);

        Flash::success('coupon updated successfully.');

        return redirect(route('admin.coupons.index'));
    }

    /**
     * Remove the specified coupon from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id, DeleteCouponRequest $request)
    {
        $coupon = $this->couponRepository->findWithoutFail($id);
        $cid = $coupon->id;
        if (empty($coupon)) {
            Flash::error('coupon not found');

            return redirect(route('admin.coupons.index'));
        }

        $this->couponRepository->delete($id);


        Activity::log([
          'contentId'   => $id,
          'contentType' => 'coupon',
          'action'      => 'Delete',
          'description' => 'Deleted a coupon: '.$cid,
          'details'     => 'Username: '.$request->user()->name,
        ]);

        Flash::success('coupon deleted successfully.');

        return redirect(route('admin.coupons.index'));
    }
}
