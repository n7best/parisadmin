<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Datatables;
use Regulus\ActivityLog\Models\Activity;
use App\Models\Template;
use Flash;
class TemplatesController extends Controller
{
    public function edit($id){
        $template = Template::find($id);

        return view('backend.systemConfigs.templateEdit')
            ->with('template', $template);
    }

    public function update($id, Request $request){
        $template = Template::find($id);
        if (empty($template)) {
            Flash::error('SystemConfigs not found');
            return redirect(route('admin.systemConfigs.index'));
        }

        $template = $template->update($request->all());
        Flash::success('SystemConfigs updated successfully.');

        return redirect(route('admin.systemConfigs.index'));

    }
}
