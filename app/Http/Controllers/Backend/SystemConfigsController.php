<?php

namespace App\Http\Controllers\Backend;

use App\Http\Requests;
use App\Http\Requests\Backend\SystemConfigs\CreateSystemConfigsRequest;
use App\Http\Requests\Backend\SystemConfigs\UpdateSystemConfigsRequest;
use App\Http\Requests\Backend\SystemConfigs\DeleteSystemConfigsRequest;
use App\Repositories\Backend\SystemConfigsRepository;
use Illuminate\Http\Request;
use Flash;
use InfyOm\Generator\Controller\AppBaseController;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use Activity;
use App\Models\Template;

class SystemConfigsController extends AppBaseController
{
    /** @var  SystemConfigsRepository */
    private $systemConfigsRepository;

    function __construct(SystemConfigsRepository $systemConfigsRepo)
    {
        $this->systemConfigsRepository = $systemConfigsRepo;
    }

    /**
     * Display a listing of the SystemConfigs.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->systemConfigsRepository->pushCriteria(new RequestCriteria($request));
        $systemConfigs = $this->systemConfigsRepository->all();
        $templates = Template::all();

        return view('backend.systemConfigs.index')
            ->with('systemConfigs', $systemConfigs)
            ->with('templates', $templates);
    }

    /**
     * Show the form for creating a new SystemConfigs.
     *
     * @return Response
     */
    public function create()
    {
        return view('backend.systemConfigs.create');
    }

    /**
     * Store a newly created SystemConfigs in storage.
     *
     * @param CreateSystemConfigsRequest $request
     *
     * @return Response
     */
    public function store(CreateSystemConfigsRequest $request)
    {
        $input = $request->all();

        $systemConfigs = $this->systemConfigsRepository->create($input);

        Activity::log([
            'contentId'   => $systemConfigs->id,
            'contentType' => 'System Configs',
            'action'      => 'Create',
            'description' => 'Created a System Configs',
            'details'     => 'Username: '.$request->user()->name,
        ]);

        Flash::success('SystemConfigs saved successfully.');

        return redirect(route('admin.systemConfigs.index'));
    }

    /**
     * Display the specified SystemConfigs.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $systemConfigs = $this->systemConfigsRepository->findWithoutFail($id);

        if (empty($systemConfigs)) {
            Flash::error('SystemConfigs not found');

            return redirect(route('admin.systemConfigs.index'));
        }

        return view('backend.systemConfigs.show')->with('systemConfigs', $systemConfigs);
    }

    /**
     * Show the form for editing the specified SystemConfigs.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $systemConfigs = $this->systemConfigsRepository->findWithoutFail($id);

        if (empty($systemConfigs)) {
            Flash::error('SystemConfigs not found');

            return redirect(route('admin.systemConfigs.index'));
        }

        return view('backend.systemConfigs.edit')->with('systemConfigs', $systemConfigs);
    }

    /**
     * Update the specified SystemConfigs in storage.
     *
     * @param  int              $id
     * @param UpdateSystemConfigsRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateSystemConfigsRequest $request)
    {
        $systemConfigs = $this->systemConfigsRepository->findWithoutFail($id);

        if (empty($systemConfigs)) {
            Flash::error('SystemConfigs not found');

            return redirect(route('admin.systemConfigs.index'));
        }

        $systemConfigs = $this->systemConfigsRepository->update($request->all(), $id);

        Activity::log([
            'contentId'   => $systemConfigs->id,
            'contentType' => 'System Configs',
            'action'      => 'Update',
            'description' => 'Updated a System Configs',
            'details'     => 'Username: '.$request->user()->name,
        ]);

        Flash::success('SystemConfigs updated successfully.');

        return redirect(route('admin.systemConfigs.index'));
    }

    /**
     * Remove the specified SystemConfigs from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id, DeleteSystemConfigsRequest $request)
    {
        $systemConfigs = $this->systemConfigsRepository->findWithoutFail($id);

        if (empty($systemConfigs)) {
            Flash::error('SystemConfigs not found');

            return redirect(route('admin.systemConfigs.index'));
        }

        Activity::log([
            'contentId'   => $systemConfigs->id,
            'contentType' => 'System Configs',
            'action'      => 'Delete',
            'description' => 'Deleted a System Configs',
            'details'     => 'Username: '.$request->user()->name.' Type:'.$systemConfigs->type.' Value:'.$systemConfigs->value,
        ]);

        $this->systemConfigsRepository->delete($id);

        Flash::success('SystemConfigs deleted successfully.');

        return redirect(route('admin.systemConfigs.index'));
    }
}
