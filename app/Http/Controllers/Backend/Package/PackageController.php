<?php

namespace App\Http\Controllers\Backend\Package;

use App\Http\Requests;
use App\Http\Requests\Backend\Package\CreatePackageRequest;
use App\Http\Requests\Backend\Package\UpdatePackageRequest;
use App\Http\Requests\Backend\Package\DeletePackageRequest;
use App\Repositories\Backend\Package\PackageRepository;
use App\Models\Services\Service;
use Illuminate\Http\Request;
use Flash;
use InfyOm\Generator\Controller\AppBaseController;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use Activity;

class PackageController extends AppBaseController
{
    /** @var  PackageRepository */
    private $PackageRepository;
    protected $services;

    function __construct(PackageRepository $PackageRepo)
    {
        $this->PackageRepository = $PackageRepo;
        $this->services = Service::with('metas')->get();
    }

    /**
     * Display a listing of the package.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->PackageRepository->pushCriteria(new RequestCriteria($request));
        $packages = $this->PackageRepository->all();

        return view('backend.packages.index')
            ->with('packages', $packages);
    }

    public function apiIndex(Request $request)
    {
        $this->PackageRepository->pushCriteria(new RequestCriteria($request));
        $packages = $this->PackageRepository->all();

        $output = [];

        foreach ($packages as $package) {
            $services = [];
            $serviceProps = explode(",", $package->services);


            foreach ($serviceProps as $serviceProp) {

                $detail = explode(":", $serviceProp);
                $serviceId = $detail[0];
                $serviceQty = !empty($detail[1]) ? $detail[1] : 1;

                $services[] = [
                'name'=>$this->services->whereLoose('id', $serviceId)->first()->name,
                'qty'=>$serviceQty,
                'id'=>$serviceId
                ];
            }

            $output[] = [
                'name' => $package->name,
                'display_name' => $package->display_name,
                'price' => $package->price,
                'options' => json_decode($package->options),
                'services' => $services,
            ];

        }
        return $output;
    }

    /**
     * Show the form for creating a new package.
     *
     * @return Response
     */
    public function create()
    {
        return view('backend.packages.create')
            ->with('services',$this->services);
    }

    /**
     * Store a newly created package in storage.
     *
     * @param CreatePackageRequest $request
     *
     * @return Response
     */
    public function store(CreatePackageRequest $request)
    {
        $input = $request->all();

        $package = $this->PackageRepository->create($input);

        Activity::log([
          'contentId'   => $package->id,
          'contentType' => 'Package',
          'action'      => 'Create',
          'description' => 'Editd a Package:'.$package->name,
          'details'     => 'Username: '.$request->user()->name,
        ]);

        Flash::success('package saved successfully.');

        return redirect(route('admin.packages.index'));
    }

    /**
     * Display the specified package.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $package = $this->PackageRepository->findWithoutFail($id);

        if (empty($package)) {
            Flash::error('package not found');

            return redirect(route('admin.packages.index'));
        }

        return view('backend.packages.show')->with('package', $package);
    }

    /**
     * Show the form for editing the specified package.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $package = $this->PackageRepository->findWithoutFail($id);

        if (empty($package)) {
            Flash::error('package not found');

            return redirect(route('admin.packages.index'));
        }

        return view('backend.packages.edit')
            ->with('package', $package)
            ->with('services', $this->services);
    }

    /**
     * Update the specified package in storage.
     *
     * @param  int              $id
     * @param UpdatePackageRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePackageRequest $request)
    {
        $package = $this->PackageRepository->findWithoutFail($id);

        if (empty($package)) {
            Flash::error('package not found');

            return redirect(route('admin.packages.index'));
        }

        $package = $this->PackageRepository->update($request->all(), $id);


        Activity::log([
          'contentId'   => $package->id,
          'contentType' => 'Package',
          'action'      => 'Edit',
          'description' => 'Editd a Package:'.$package->name,
          'details'     => 'Username: '.$request->user()->name,
        ]);

        Flash::success('package updated successfully.');

        return redirect(route('admin.packages.index'));
    }

    /**
     * Remove the specified package from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id, DeletePackageRequest $request)
    {
        $package = $this->PackageRepository->findWithoutFail($id);
        $name = $package->display_name;
        if (empty($package)) {
            Flash::error('package not found');

            return redirect(route('admin.packages.index'));
        }

        $this->PackageRepository->delete($id);


        Activity::log([
          'contentId'   => $id,
          'contentType' => 'Package',
          'action'      => 'Delete',
          'description' => 'Deleted a Package: '.$name,
          'details'     => 'Username: '.$request->user()->name,
        ]);

        Flash::success('package deleted successfully.');

        return redirect(route('admin.packages.index'));
    }
}
