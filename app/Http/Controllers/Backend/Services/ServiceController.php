<?php

namespace App\Http\Controllers\Backend\Services;

use App\Http\Controllers\Controller;
use App\Models\Services\Service;
use App\Models\Services\Service_Meta;
use App\Http\Requests\Backend\Service\CreateServiceRequest;
use App\Http\Requests\Backend\Service\StoreServiceRequest;
use App\Http\Requests\Backend\Service\DeleteServiceRequest;


class ServiceController extends Controller
{
    protected $services;

    public function __construct(){
      $this->services = Service::all();
    }

    public function getServices()
    {
        $output = [];

        foreach ($this->services as $service) {
            $output[] = [
              'id' => $service->name,
              'name' => $service->display_name,
              'metas' => $service->metas,
              'catalog' => $service->catalog,
              'sid' => $service->id
            ];
        }

        usort($output, function($a, $b) {
            return strcmp($a["catalog"], $b["catalog"]);
        });

        //dd($output);
        return json_encode($output);
    }

    public function index()
    {
      return view('backend.services.index')
            ->withServices($this->services);
    }

    public function create(CreateServiceRequest $req)
    {
      return view('backend.services.create');
    }

    public function store(StoreServiceRequest $req)
    {
      $inputs = $req->all();
      $newService = new Service();
      $newService->name = $inputs['name'];
      $newService->display_name = $inputs['display_name'];
      $newService->catalog = $inputs['catalog'];
      $newService->save();

      foreach($inputs['metasType'] as $index => $metaType){
        $meta = new Service_Meta();
        $meta->services_id = $newService->id;
        $meta->name = $inputs['metasName'][$index];
        $meta->type = $metaType;
        $meta->price_factor = $inputs['metasPrice'][$index];
        $meta->options = $inputs['metasOptions'][$index];
        $meta->save();
      }

      return redirect()->route('admin.services.index')->withFlashSuccess('New service sucessfuly created.');
    }

    public function destroy($id,DeleteServiceRequest $req)
    {
      $service = Service::findOrFail($id);
      $service->delete();
      return redirect()->route('admin.services.index')->withFlashSuccess('Service sucessfuly deleted.');
    }
}