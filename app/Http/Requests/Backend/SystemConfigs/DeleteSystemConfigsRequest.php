<?php

namespace App\Http\Requests\Backend\SystemConfigs;

use App\Http\Requests\Request;

/**
 * Class EditPermissionRequest
 * @package App\Http\Requests\Backend\Access\Permission
 */
class DeleteSystemConfigsRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return access()->allow('delete-system-configs');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }
}
