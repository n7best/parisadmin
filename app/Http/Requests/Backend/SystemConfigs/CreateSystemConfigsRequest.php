<?php

namespace App\Http\Requests\Backend\SystemConfigs;

use App\Http\Requests\Request;
use App\Models\SystemConfigs;

class CreateSystemConfigsRequest extends Request {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return access()->allow('create-system-configs');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return SystemConfigs::$rules;
    }
}
