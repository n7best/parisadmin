<?php

namespace App\Http\Requests\Backend\Companies;

use App\Http\Requests\Request;
use App\Models\Companies\Company;

class UpdateCompanyRequest extends Request {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return access()->allow('edit-company');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return Company::$rules;
    }
}
