<?php

namespace App\Http\Requests\Backend\Package;

use App\Http\Requests\Request;
use App\Models\Package\Package;

class UpdatePackageRequest extends Request {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return access()->allow('edit-package');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return Package::$rules;
    }
}
