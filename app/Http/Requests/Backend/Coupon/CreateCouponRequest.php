<?php

namespace App\Http\Requests\Backend\Coupon;

use App\Http\Requests\Request;
use App\Models\Orders\Coupon;

class CreateCouponRequest extends Request {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return access()->allow('create-coupon');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return Coupon::$rules;
    }
}
