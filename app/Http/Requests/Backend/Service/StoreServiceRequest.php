<?php

namespace App\Http\Requests\Backend\Service;

use App\Http\Requests\Request;

/**
 * Class StorePermissionRequest
 * @package App\Http\Requests\Backend\Access\Permission
 */
class StoreServiceRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return access()->allow('create-service');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'         => 'required',
            'display_name' => 'required',
            'metasType.*'  => 'in:text,integer,select',
            'metasName.*'  => 'string'
        ];
    }
}
