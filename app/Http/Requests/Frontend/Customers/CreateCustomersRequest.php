<?php

namespace App\Http\Requests\Frontend\Customers;

use App\Http\Requests\Request;
use App\Models\Customers\Customer;

class CreateCustomersRequest extends Request {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return access()->allow('create-customer');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return Customer::$rules;
    }
}
