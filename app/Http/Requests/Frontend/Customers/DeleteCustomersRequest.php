<?php

namespace App\Http\Requests\Frontend\Customers;

use App\Http\Requests\Request;
use App\Models\Companies\Company;

class DeleteCustomersRequest extends Request {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return access()->allow('delete-customers');;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [];
    }
}
