<?php

namespace App\Http\Requests\Frontend\Invoice;

use App\Http\Requests\Request;

/**
 * Class CreatePermissionRequest
 * @package App\Http\Requests\Backend\Access\Permission
 */
class CreateInvoiceRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return access()->allow('create-invoice');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }
}
