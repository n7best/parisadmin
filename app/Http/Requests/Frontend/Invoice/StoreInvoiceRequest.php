<?php

namespace App\Http\Requests\Frontend\Invoice;

use App\Http\Requests\Request;

/**
 * Class StorePermissionRequest
 * @package App\Http\Requests\Backend\Access\Permission
 */
class StoreInvoiceRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return (access()->allow('create-invoice') && access()->allow('create-customer') && access()->allow('edit-customer'));
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //customer info
            'customer.invoiceCustomerID' => 'exists:customers,id',
            'customer.invoiceBrideName' => 'string',
            'customer.invoiceGroomName' => 'string|required_without_all:customer.invoiceBrideName,invoiceCustomerID',
            'customer.invoiceWeddingDate' => 'date',
            'customer.invoiceStudioDate' => 'date',
            'customer.invoiceOutdoorDate' => 'date',
            'customer.invoiceBridePhone' => 'string',
            'customer.invoiceGroomPhone' => 'string|required_without_all:customer.invoiceBridePhone,invoiceCustomerID',
            'customer.invoiceCustomerEmail' => 'email'
        ];
    }
}
