<?php

namespace App\Http\Requests\Frontend\Notes;

use App\Http\Requests\Request;

/**
 * Class CreatePermissionRequest
 * @package App\Http\Requests\Backend\Access\Permission
 */
class CreateNoteRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return access()->allow('post-note');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'ni'=>'required|integer',
            'message'=>'required',
            'internal'=>'in:1,2'
        ];
    }
}
