<?php

/**
 * Frontend Controllers
 */

Route::get('macros', 'FrontendController@macros')->name('frontend.macros');

Route::group(['middleware' => ['web', 'wechat.inapp', 'wechat.oauth']], function () {
  Route::get('mobile', 'MobileController@index')->name('frontend.mobile');
  

  Route::group(['namespace' => 'EInvites'], function() {
      Route::get("edisplay/{id}", "EInviteController@display");
  });
});

Route::get('raffle', 'MobileController@raffle')->name('frontend.raffle');

Route::get('mobileinvite', 'MobileController@mobileinvite')->name('frontend.mobileinvite');

Route::group(['namespace' => 'Customers'], function() {
    Route::get("photoselect/{id}", "PhotoSelectController@show");
    //Route::post("psupdate/{id}", "PhotoSelectController@update")->name('psupdateapi');
});


/**
 * These frontend controllers require the user to be logged in
 */
Route::group(['middleware' => 'auth'], function () {
    Route::get('/', 'FrontendController@index')->name('frontend.index');
    Route::group(['namespace' => 'User'], function() {
        Route::get('dashboard', 'DashboardController@index')->name('frontend.user.dashboard');
        Route::get('profile/edit', 'ProfileController@edit')->name('frontend.user.profile.edit');
        Route::patch('profile/update', 'ProfileController@update')->name('frontend.user.profile.update');

        Route::resource("hr", "HrController");
        Route::get('hr/delete/{id}', [
            'as' => 'hr.delete',
            'uses' => 'HrController@destroy',
        ]);

    });

    Route::group(['namespace' => 'Invoice'], function() {
        Route::resource('invoice', 'InvoiceController');
        Route::any('invoiceTableData', 'InvoiceController@tableInvoiceData')->name('frontend.invoice.tabledata');

        Route::resource('payment', 'PaymentController', ['only' => [
          'update', 'store', 'index'
        ]]);
    });

    Route::post('zips/store',['as'=>'zips.store','uses'=>'ZipController@store']);

    Route::group(['namespace' => 'Customers'], function() {
      Route::resource("customers", "CustomersController");

      Route::get('customers/delete/{id}', [
          'as' => 'customers.delete',
          'uses' => 'CustomersController@destroy',
      ]);

      Route::post("psstore", "PhotoSelectController@store")->name('psstore');
    });

    Route::group(['namespace' => 'Notes'], function() {
       Route::post('notes', [
           'as' => 'notes.post',
           'uses' => 'NotesController@post',
       ]);
    });

    Route::group(['namespace' => 'EInvites'], function() {
        Route::resource("einvite", "EInviteController");
    });

    Route::group(['namespace' => 'Gallery'], function() {
        Route::resource("gallery", "GalleryController");
        Route::get("transfer", "GalleryController@transfer")->name('transfer');
        Route::get("galleryapi/{id}", "GalleryController@apishow")->name('galleryapishow');
        Route::get("galleryps/{id}", "GalleryController@apiPhotoSelect")->name('galleryapips');
    });

    Route::group(['namespace' => 'Tasks'], function() {
       Route::post('task', [
           'as' => 'task.post',
           'uses' => 'TaskController@post',
       ]);

       Route::get('tasks', [
           'as' => 'task.index',
           'uses' => 'TaskController@index',
       ]);
    });


});