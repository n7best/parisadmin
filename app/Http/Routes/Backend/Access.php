<?php

Route::group([
    'prefix'     => 'access',
    'namespace'  => 'Access',
    'middleware' => 'access.routeNeedsPermission:view-access-management',
], function() {
    /**
     * User Management
     */
    Route::group(['namespace' => 'User'], function() {
        Route::resource('users', 'UserController', ['except' => ['show']]);

        Route::get('users/deactivated', 'UserController@deactivated')->name('admin.access.users.deactivated');
        Route::get('users/deleted', 'UserController@deleted')->name('admin.access.users.deleted');
        Route::get('account/confirm/resend/{user_id}', 'UserController@resendConfirmationEmail')->name('admin.account.confirm.resend');

        /**
         * Specific User
         */
        Route::group(['prefix' => 'user/{id}', 'where' => ['id' => '[0-9]+']], function() {
            Route::get('delete', 'UserController@delete')->name('admin.access.user.delete-permanently');
            Route::get('restore', 'UserController@restore')->name('admin.access.user.restore');
            Route::get('mark/{status}', 'UserController@mark')->name('admin.access.user.mark')->where(['status' => '[0,1]']);
            Route::get('password/change', 'UserController@changePassword')->name('admin.access.user.change-password');
            Route::post('password/change', 'UserController@updatePassword')->name('admin.access.user.change-password');
        });
    });

    /**
     * Role Management
     */
    Route::group(['namespace' => 'Role'], function() {
        Route::resource('roles', 'RoleController', ['except' => ['show']]);
    });

    /**
     * Permission Management
     */
    Route::group(['prefix' => 'roles', 'namespace' => 'Permission'], function() {
        Route::resource('permission-group', 'PermissionGroupController', ['except' => ['index', 'show']]);
        Route::resource('permissions', 'PermissionController', ['except' => ['show']]);

        Route::group(['prefix' => 'groups'], function() {
            Route::post('update-sort', 'PermissionGroupController@updateSort')->name('admin.access.roles.groups.update-sort');
        });
    });
});

/**
 * Services Management
 */
Route::group(['namespace' => 'Services', 'middleware' => 'access.routeNeedsPermission:view-service'], function() {
    Route::resource('services', 'ServiceController', ['except' => ['show']]);
});


Route::group(['namespace' => 'Companies', 'middleware' => 'access.routeNeedsPermission:view-service'], function() {
    Route::resource("companies", "CompanyController");

    Route::get('companies/delete/{id}', [
        'uses' => 'CompanyController@destroy',
    ])->name('admin.companies.delete');
});

Route::group(['middleware' => 'access.routeNeedsPermission:view-activity-log'],function(){
    Route::controller('activity-logs', 'ActivityLogController', [
        'anyData'  => 'activity-logs.data',
        'getIndex' => 'activity-logs',
    ]);
});

Route::group(['middleware' => 'access.routeNeedsPermission:view-system-configs'],function(){
    Route::resource("systemConfigs", "SystemConfigsController");

    Route::get('systemConfigs/delete/{id}', [
        'as' => 'admin.systemConfigs.delete',
        'uses' => 'SystemConfigsController@destroy',
    ]);
});

Route::group(['middleware' => 'access.routeNeedsPermission:view-system-configs'],function(){

    Route::patch('templates/{id}', [
           'as' => 'admin.templates.update',
           'uses' => 'TemplatesController@update',
    ]);

    Route::get('templates/{id}', [
           'as' => 'admin.templates.edit',
           'uses' => 'TemplatesController@edit',
    ]);

    Route::get('templates/delete/{id}', [
        'as' => 'admin.templates.delete',
        'uses' => 'TemplatesController@destroy',
    ]);
});


Route::group(['namespace' => 'Package', 'middleware' => 'access.routeNeedsPermission:view-package'], function() {
    Route::resource("packages", "PackageController");

    Route::get('packages/delete/{id}', [
        'as' => 'admin.packages.delete',
        'uses' => 'PackageController@destroy',
    ]);
});

Route::group(['namespace' => 'Coupon', 'middleware' => 'access.routeNeedsPermission:view-coupon'], function() {
    Route::resource("coupons", "CouponController");

    Route::get('coupon/delete/{id}', [
        'as' => 'admin.coupons.delete',
        'uses' => 'CouponController@destroy',
    ]);
});