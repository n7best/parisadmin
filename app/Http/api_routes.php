<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where all API routes are defined.
|
*/



Route::group(['namespace' => 'Backend\Services', 'middleware' => 'web'], function() {
    Route::get('services', 'ServiceController@getServices')->name('services.all');
});

Route::group(['namespace' => 'Backend\Package', 'middleware' => 'web'], function() {
    Route::get('packages', 'PackageController@apiIndex')->name('packages.all');
});

Route::group(['namespace' => 'Frontend\Selects', 'middleware' => 'web'], function() {
    Route::get('select', 'SelectsController@index')->name('api.selects');
});

Route::group(['namespace' => 'Frontend\Tasks', 'middleware' => 'web'], function() {
    Route::get('checktime', 'TaskController@checkUserTime')->name('api.checktime');
    Route::get('tasks', 'TaskController@apiShow')->name('api.tasks');
    Route::post('updatetask', 'TaskController@apiStore')->name('api.updatetask');
});

Route::group(['namespace' => 'Frontend\EInvites'], function() {
    Route::get('einvitelogs/{id}', 'EInviteController@apiLogs')->name('api.einvitelogs');
    Route::post('storeeinvitelog', 'EInviteController@storeLog')->name('api.storeeinvitelog');
    Route::get('donations/{id}', 'EInviteController@apiDonations')->name('api.donationss');
    Route::post('storedonations', 'EInviteController@storeDonation')->name('api.storedonations');
});

Route::group(['namespace' => 'Frontend\Tasks'], function() {
    Route::get('ical', 'TaskController@apiCal')->name('api.ical');
});

Route::group(['namespace' => 'Frontend'], function() {
    //Route::resource('zips', 'ZipController');
    Route::get('zips/{hash}',['as'=>'zips.show','uses'=>'ZipController@show']);
    Route::post('zips/update/{hash}',['as'=>'zips.update','uses'=>'ZipController@update']);

    Route::post('s3/signature',['as'=>'s3sign','uses'=>'S3UploadCtrl@endpoint']);
    Route::post('mobileverify', 'MobileController@verify')->name('frontend.mobileverify');
    Route::post('mobile_user_update', 'MobileController@mobile_user_update')->name('frontend.mobile_user_update');
    Route::post('mobile_user_photoselected', 'MobileController@mobile_user_photoselected')->name('frontend.mobile_user_photoselected');
    Route::post('raffledraw', 'MobileController@raffledraw')->name('frontend.raffledraw');

    Route::post('coupon/verify', 'Invoice\InvoiceController@verifyCoupon')->name('frontend.verifycoupon');
    Route::get('pdf/{id}', 'Invoice\InvoiceController@pdf')->name('frontend.pdf');

    Route::group(['namespace' => 'Customers'], function() {
        Route::post("psupdate/{id}", "PhotoSelectController@update")->name('psupdateapi');
        Route::get("pskeys/{id}", "PhotoSelectController@apiDlKeys");
        Route::post("photodetail/{id}", "PhotoSelectController@phpDetailApi")->name('photodetail.update');
    });
});