<?php

namespace App\Http\Middleware;

use Closure;
use App\Repositories\Backend\SystemConfigsRepository;

class CheckWeChat
{

    function __construct()
    {
    }
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!$this->is_weixin()){
            return response('Only Support WeChat Browser.', 401);
        }

        return $next($request);
    }

    public function is_weixin(){ 
    if ( strpos($_SERVER['HTTP_USER_AGENT'], 'MicroMessenger') !== false ) {
            return true;
    }
    return false;
}
}
