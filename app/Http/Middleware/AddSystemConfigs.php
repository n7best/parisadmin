<?php

namespace App\Http\Middleware;

use Closure;
use App\Repositories\Backend\SystemConfigsRepository;

class AddSystemConfigs
{
    /** @var  SystemConfigsRepository */
    private $systemConfigsRepository;

    function __construct(SystemConfigsRepository $systemConfigsRepo)
    {
        $this->systemConfigsRepository = $systemConfigsRepo;
    }
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $systemConfigs = $this->systemConfigsRepository->all();

        foreach ($systemConfigs as $config) {
            config(['system.'.$config->type => $config->value]);
        }

        return $next($request);
    }
}
