var elixir = require('laravel-elixir');

elixir(function(mix) {
 mix
     .phpUnit()

    /**
     * Copy needed files from /node directories
     * to /public directory.
     */
     .copy(
       'node_modules/font-awesome/fonts',
       'public/build/fonts/font-awesome'
     )
     .copy(
       'node_modules/bootstrap-sass/assets/fonts/bootstrap',
       'public/build/fonts/bootstrap'
     )
     .copy(
       'node_modules/bootstrap-sass/assets/javascripts/bootstrap.min.js',
       'public/js/vendor/bootstrap'
     )

     .copy(
       'node_modules/moment/min/moment.min.js',
       'public/js/vendor/moment'
     )

     .copy(
       'node_modules/dragula/dist/',
       'public/vendor/dragula'
     )
     
     /**
      * Process frontend SCSS stylesheets
      */
     .sass([
        'frontend/space.scss',
        'frontend/app.scss',
        'plugin/sweetalert/sweetalert.scss'
     ], 'resources/assets/css/frontend/app.css')

     /**
      * Combine pre-processed frontend CSS files
      */
     .styles([
        'plugin/xedit/bootstrap-editable.css',
        'plugin/xedit/bootstrap-switch.min.css',
        'plugin/datepair/bootstrap-datepicker3.min.css',
        'plugin/datepair/jquery.timepicker.css',
        'plugin/fullcalendar/fullcalendar.min.css',
        'plugin/fullcalendar/scheduler.min.css',
        'plugin/datatable/jquery.dataTables.min.css',
        'plugin/datatable/dataTables.bootstrap.min.css',
        'plugin/select2/select2.min.css',
        'plugin/select2/select2-bootstrap.min.css',
        'plugin/fineuploader/fine-uploader-new.min.css',
        'plugin/gallery/blueimp-gallery.min.css',
        'plugin/gallery/bootstrap-image-gallery.min.css',
        'plugin/rangepicker/daterangepicker.css',
        'plugin/magpopup/magnific-popup.css',
        'plugin/qtip/jquery.qtip.min.css',
        'plugin/drawer/bootstrap-drawer.min.css',
        'frontend/app.css'
     ], 'public/css/frontend.css')

     /**
      * Process frontend SCSS stylesheets
      */
     .sass([
        'frontend/mb.scss',
        'plugin/sweetalert/sweetalert.scss'
     ], 'resources/assets/css/frontend/mb.css')

     .styles([
        'plugin/weui/weui.min.css',
        'plugin/weui/jquery-weui.min.css',
        'plugin/magpopup/magnific-popup.css',
        'frontend/mb.css',
     ], 'public/css/mobile.css')

     .scripts([
        'plugin/weui/jquery-weui.min.js',
        'plugin/weui/swiper.min.js',
        'plugin/magpopup/jquery.magnific-popup.min.js',
     ], 'public/js/mobile.js')
     /**
      * Combine frontend scripts
      */
     .scripts([
        'plugin/sweetalert/sweetalert.min.js',
        'plugin/rangepicker/daterangepicker.js',
        'plugin/datepair/bootstrap-datepicker.min.js',
        'plugin/datepair/jquery.timepicker.min.js',
        'plugin/datepair/jquery.datepair.min.js',
        'plugin/fullcalendar/fullcalendar.min.js',
        'plugin/fullcalendar/scheduler.min.js',
        'plugin/xedit/bootstrap-editable.min.js',
        'plugin/xedit/bootstrap-switch.min.js',
        'plugin/datatable/jquery.dataTables.min.js',
        'plugin/datatable/dataTables.bootstrap.min.js',
        'plugin/select2/select2.min.js',
        'plugin/fineuploader/s3.fine-uploader.min.js',
        'plugin/gallery/jquery.blueimp-gallery.min.js',
        'plugin/gallery/bootstrap-image-gallery.min.js',
        'plugin/signpad/signature_pad.min.js',
        'plugin/magpopup/jquery.magnific-popup.js',
        'plugin/jszip/FileSaver.js',
        'plugin/jszip/jszip.js',
        'plugin/jszip/jszip-utils.js',
        'plugin/qtip/jquery.qtip.min.js',
        'plugin/jsoneditor/jsoneditor.min.js',
        'plugin/drawer/drawer.min.js',
        'plugins.js',
        'frontend/app.js'
     ], 'public/js/frontend.js')

     /**
      * Process backend SCSS stylesheets
      */
     .sass([
         'backend/app.scss',
         'backend/plugin/toastr/toastr.scss',
         'plugin/sweetalert/sweetalert.scss'
     ], 'resources/assets/css/backend/app.css')

     /**
      * Combine pre-processed backend CSS files
      */
     .styles([
         'backend/app.css'
     ], 'public/css/backend.css')

     /**
      * Combine backend scripts
      */
     .scripts([
         'plugin/sweetalert/sweetalert.min.js',
         'plugins.js',
         'backend/app.js',
         'backend/plugin/toastr/toastr.min.js',
         'backend/custom.js'
     ], 'public/js/backend.js')

    /**
      * Apply version control
      */
     .version(["public/css/mobile.css", "public/js/mobile.js", "public/css/frontend.css", "public/js/frontend.js", "public/css/backend.css", "public/js/backend.js"]);
});